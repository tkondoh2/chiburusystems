﻿#ifndef MSGBOX_H
#define MSGBOX_H

int msgbox(const char* fmt, ...);

#endif // MSGBOX_H
