﻿#include "dsapilogger.h"
#include "msgbox.h"

#include <QString>
#include <QTextStream>

#include <dsapi.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <osfile.h>
#include <addin.h>

#if defined(NT)
#pragma pack(pop)
#endif

#define LOG_FILE "HttpLog.txt"
#define MSG_HEADER "HTTP Web Server Logger Extension"

char logFileName[MAX_PATH] = "";

EXTERN_C DSAPILOGGERSHARED_EXPORT
unsigned int FilterInit(FilterInitData* pInitData)
{
  char dataDir[MAX_PATH] = "";

  pInitData->appFilterVersion = kInterfaceVersion;
  pInitData->eventFlags = kFilterParsedRequest;
  strcpy(pInitData->filterDesc, "DSAPI Logger");

  OSGetDataDirectory(dataDir);
  sprintf(logFileName, "%s\\%s", dataDir, LOG_FILE);

  msgbox(MSG_HEADER " started.");
  msgbox("HTTP Requests will be logged to %s.", logFileName);
  return kFilterHandledEvent;
}

EXTERN_C DSAPILOGGERSHARED_EXPORT
unsigned int HttpFilterProc(
    FilterContext* pContext
    , unsigned int /*eventType*/
    , void* ptr
    )
{
  static const char* const requestStringTable[] =
  {
    "None"
    , "HEAD"
    , "GET"
    , "POST"
    , "PUT"
    , "DELETE"
    , "TRACE"
    , "CONNECT"
    , "OPTIONS"
    , "UNKNOWN"
    , "BAD"
  };

  FilterParsedRequest* pParsedRequest
      = reinterpret_cast<FilterParsedRequest*>(ptr);

  FILE* fp = fopen(logFileName, "a+b");
  if (fp == nullptr)
  {
    msgbox(MSG_HEADER ": can't open %s\n", logFileName);
    return kFilterNotHandled;
  }

  unsigned int errID = 0;

  FilterRequest request;
  pContext->GetRequest(pContext, &request, &errID);

  msgbox("%s %s %s"
        , requestStringTable[request.method]
        , request.URL
        , request.version
        );

  char* headers;
  int headersLen = pParsedRequest->GetAllHeaders(pContext, &headers, &errID);

  char* contents;
  int contentsLen = pContext->GetRequestContents(pContext, &contents, &errID);

  fwrite(headers, headersLen, 1, fp);
  fwrite("\r\n", 2, 1, fp);

  if (contentsLen != 0)
  {
    fwrite(contents, contentsLen, 1, fp);
    fwrite("\r\n", 2, 1, fp);
    fwrite("\r\n", 2, 1, fp);
  }
  fwrite("\r\n", 2, 1, fp);
  fwrite("\r\n", 2, 1, fp);
  fclose(fp);

  return kFilterHandledEvent;
}

EXTERN_C DSAPILOGGERSHARED_EXPORT
unsigned int TerminateFilter(unsigned int)
{
  msgbox(MSG_HEADER " terminated.");
  return kFilterHandledEvent;
}
