﻿#include "msgbox.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <ostime.h>
#include <misc.h>

#if defined(NT)
#pragma pack(pop)
#endif

#include <stdio.h>

int msgbox(const char* fmt, ...)
{
  va_list argptr;
  int cnt;
  TIMEDATE td;
  WORD n;
  char dateText[MAXALPHATIMEDATE + 1];

  OSCurrentTIMEDATE(&td);
  ConvertTIMEDATEToText(0, 0, &td, dateText, MAXALPHATIMEDATE, &n);
  dateText[n] = '\0';
  printf("%s  ", dateText);

  va_start(argptr, fmt);
  cnt = vprintf(fmt, argptr);
  va_end(argptr);

  printf("\n");

  return n + 2 + cnt + 1;
}
