﻿#ifndef OPTIONVALUES_H
#define OPTIONVALUES_H

#include <QString>

enum Operation
{
  Invalid
  , Replace
  , Remove
  , AppendTo
  , RemoveFrom
};

class OptionValues
{
public:
  OptionValues();

  Operation operation() const { return operation_; }
  void setOperation(Operation operation) { operation_ = operation; }

  QString name() const { return name_; }
  void setName(const QString& name) { name_ = name; }

  QString value() const { return value_; }
  void setValue(const QString& value) { value_ = value; }

private:
  Operation operation_;
  QString name_;
  QString value_;
};

#endif // OPTIONVALUES_H
