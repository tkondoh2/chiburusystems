QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

TARGET = notesinieditor

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH -= $$PWD
INCLUDEPATH += $$PWD/..

DEPENDPATH -= $$PWD
DEPENDPATH += $$PWD/..

SOURCES += main.cpp \
    notesinieditor.cpp \
    options.cpp \
    result.cpp \
    replacevariable.cpp \
    removevariable.cpp \
    appendtolist.cpp \
    removefromlist.cpp \
    operator.cpp \
    optionvalues.cpp

LIBS += -lntlx_core
win32:CONFIG(debug,release|debug): LIBS += -L$$OUT_PWD/../ntlx_core/debug
else:win32:CONFIG(release,release|debug): LIBS += -L$$OUT_PWD/../ntlx_core/release
else:macx: LIBS += -L$$OUT_PWD/../ntlx_core

#
# Notes API用マクロ定義
#
win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
}
else:unix {
    DEFINES += UNIX LINUX W32
    QMAKE_CXXFLAGS += -std=c++0x
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -lnotes

#
# Windows/32
# "INCLUDEPATH+=C:/Users/Public/notesapi/include" "LIBS+=-LC:/Users/Public/notesapi/lib/mswin32"
#

#
# Windows/64
# "INCLUDEPATH+=C:/Users/Public/notesapi/include" "LIBS+=-LC:/Users/Public/notesapi/lib/mswin64"
#

#
# MacOS
# "INCLUDEPATH+=/Users/Shared/notesapi/include" "LIBS+=-L'/Applications/IBM Notes.app/Contents/MacOS'"
#

HEADERS += \
    notesinieditor.h \
    options.h \
    result.h \
    replacevariable.h \
    removevariable.h \
    appendtolist.h \
    removefromlist.h \
    operator.h \
    optionvalues.h
