﻿#include "replacevariable.h"
#include "result.h"

#include <ntlx_core/environment.h>
#include <ntlx_core/text.h>
#include <QObject>

ReplaceVariable::ReplaceVariable(const OptionValues& optionValues)
  : Operator(optionValues)
{
}

Result ReplaceVariable::operator ()()
{
  ntlx::Environment::setValue(
        ntlx::Text::fromQString(values().name())
        , ntlx::Text::fromQString(values().value())
        );
  cout_ << QObject::tr("The value of environment variable \"%1\""
                       " has been replaced with \"%2\".")
           .arg(values().name())
           .arg(values().value())
        << endl
           ;
  return Result();
}
