﻿#include <QCoreApplication>

#include "notesinieditor.h"
#include <ntlx_core/main.h>
#include <QTimer>

int main(int argc, char *argv[])
{
  ntlx::Main notesMain(argc, argv);

  QCoreApplication app(argc, argv);
  app.setOrganizationName("Chiburu Systems");
  app.setApplicationName("NotesIniEditor");
  app.setApplicationVersion("1.0.0");

  // NotesIniEditor本体
  NotesIniEditor editor;
  QTimer::singleShot(0, &editor, SLOT(run()));

  return app.exec();
}
