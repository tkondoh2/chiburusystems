﻿#include "removevariable.h"
#include "result.h"

#include <ntlx_core/environment.h>
#include <ntlx_core/text.h>
#include <QObject>

RemoveVariable::RemoveVariable(const OptionValues& optionValues)
  : Operator(optionValues)
{
}

Result RemoveVariable::operator ()()
{
  ntlx::Environment::setValue(
        ntlx::Text::fromQString(values().name())
        , ntlx::Text()
        );
  cout_ << QObject::tr("Environment variable \"%1\" was deleted.")
           .arg(values().name())
        << endl
           ;
  return Result();
}
