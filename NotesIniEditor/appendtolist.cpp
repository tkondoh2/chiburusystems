﻿#include "appendtolist.h"
#include "result.h"
#include "replacevariable.h"

#include <ntlx_core/environment.h>
#include <ntlx_core/textlist.h>
#include <QObject>

AppendToList::AppendToList(const OptionValues& optionValues)
  : Operator(optionValues)
{
}

Result AppendToList::operator ()()
{
  ntlx::Text name = ntlx::Text::fromQString(values().name());

  ntlx::Text oldValue = ntlx::Environment::value(name);
  ntlx::Text newValue;
  if (oldValue.isEmpty())
    return ReplaceVariable(values())();

  else
  {
    QStringList qList = oldValue.toQString().split(",");
    QString qItem = values().value();
    if (!qList.contains(qItem))
    {
      qList.append(qItem);
      newValue = ntlx::Text::fromQString(qList.join(","));
      ntlx::Environment::setValue(name, newValue);
      cout_ << QObject::tr("Value \"%2\" was added"
                           " to the value list of environment variable \"%1\".")
               .arg(values().name())
               .arg(values().value())
            << endl
               ;
    }
    else
    {
      cout_ << QObject::tr("The value \"%2\" already exists"
                           " in the value list of environment variable \"%1\".")
               .arg(values().name())
               .arg(values().value())
            << endl
               ;
    }
  }
  return Result();
}
