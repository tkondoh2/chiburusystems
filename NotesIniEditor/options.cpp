﻿#include "options.h"
#include "result.h"

#include <QtDebug>

Options::Options()
  : parser_()
  , nameOption_(
      QStringList({"n", "name"})
      , QObject::tr("Specify the variable name.")
      , "name"
      )
  , valueOption_(
      QStringList({"V", "value"})
      , QObject::tr("Specify the variable value.")
      , "value"
      )
  , values_()
{
  parser_.setApplicationDescription(QObject::tr("Notes.INI Editor"));
  parser_.addHelpOption();
  parser_.addVersionOption();
  parser_.addPositionalArgument(
        "operation"
        , QObject::tr("Select from the following operation items and specify,"
                      " 'set','remove','push','pull'.")
        );
  parser_.addOption(nameOption_);
  parser_.addOption(valueOption_);
}

Result Options::parse()
{
  parser_.process(*qApp);

  QStringList args = parser_.positionalArguments();
  if (!args.isEmpty())
  {
    if (args[0] == "set")
      values_.setOperation(Operation::Replace);
    else if (args[0] == "remove")
      values_.setOperation(Operation::Remove);
    else if (args[0] == "push")
      values_.setOperation(Operation::AppendTo);
    else if (args[0] == "pull")
      values_.setOperation(Operation::RemoveFrom);
    else
      return Result(Result::UnknownOperation);

    values_.setName(parser_.value(nameOption_));
    if (values_.name().isEmpty())
      return Result(Result::UndefinedName);

    values_.setValue(parser_.value(valueOption_));
  }
  else
    return Result(Result::NoArguments);
  return Result();
}
