﻿#ifndef APPENDTOLIST_H
#define APPENDTOLIST_H

#include "operator.h"

class AppendToList
    : public Operator
{
public:
  AppendToList(const OptionValues& optionValues);

  virtual Result operator ()() override;
};

#endif // APPENDTOLIST_H
