﻿#ifndef REPLACEVARIABLE_H
#define REPLACEVARIABLE_H

#include "operator.h"

class ReplaceVariable
    : public Operator
{
public:
  ReplaceVariable(const OptionValues& optionValues);

  virtual Result operator ()() override;
};

#endif // REPLACEVARIABLE_H
