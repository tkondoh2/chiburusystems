﻿#ifndef RESULT_H
#define RESULT_H

class QString;

class Result
{
public:
  enum {
    NoError = 0
    , NoArguments = 1
    , UnknownOperation = 2
    , UndefinedName = 3
    , UnknownError = 255
  };

  Result();
  Result(int code);

  int code() const;

  bool hasError() const;
  bool noError() const
  {
    return !hasError();
  }

  QString message() const;

private:
  int code_;
};

#endif // RESULT_H
