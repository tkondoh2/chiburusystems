﻿#include "operator.h"

#include <QtDebug>

Operator::Operator(const OptionValues& optionValues)
  : values_(optionValues)
  , cout_(stdout)
{
  qDebug() << "Variable Name" << values_.name();
  qDebug() << "Variable Value" << values_.value();
}
