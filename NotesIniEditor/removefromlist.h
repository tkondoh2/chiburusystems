﻿#ifndef REMOVEFROMLIST_H
#define REMOVEFROMLIST_H

#include "operator.h"

class RemoveFromList
    : public Operator
{
public:
  RemoveFromList(const OptionValues& optionValues);

  virtual Result operator ()() override;
};

#endif // REMOVEFROMLIST_H
