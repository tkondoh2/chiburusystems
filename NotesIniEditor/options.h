﻿#ifndef OPTIONS_H
#define OPTIONS_H

#include <QCommandLineParser>
#include "optionvalues.h"

class Result;

class Options
{
public:
  Options();

  Result parse();

  const OptionValues& values() const
  {
    return values_;
  }

private:
  QCommandLineParser parser_;
  QCommandLineOption nameOption_;
  QCommandLineOption valueOption_;
  OptionValues values_;
};

#endif // OPTIONS_H
