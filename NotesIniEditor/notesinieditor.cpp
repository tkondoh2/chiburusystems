﻿#include "notesinieditor.h"
#include "result.h"
#include "replacevariable.h"
#include "removevariable.h"
#include "appendtolist.h"
#include "removefromlist.h"
#include "options.h"

#include <QCoreApplication>

NotesIniEditor::NotesIniEditor()
  : QObject()
  , cerr_(stderr)
{
}

void NotesIniEditor::run()
{
  // コマンドラインオプションをパースする。
  Options options;
  Result result = options.parse();
  if (result.noError())
  {
    OptionValues values = options.values();

    // オペレーションごとに動作を分ける。
    switch (values.operation())
    {
    case Operation::Replace:
      result = ReplaceVariable(values)();
      break;

    case Operation::Remove:
      result = RemoveVariable(values)();
      break;

    case Operation::AppendTo:
      result = AppendToList(values)();
      break;

    case Operation::RemoveFrom:
      result = RemoveFromList(values)();
      break;
    }
  }

  if (result.hasError())
    cerr_ << result.message() << endl;

  // 正常終了。
  QCoreApplication::exit(result.code());
}
