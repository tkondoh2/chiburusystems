﻿#ifndef NOTESINIEDITOR_H
#define NOTESINIEDITOR_H

#include <QObject>
#include <QTextStream>

class NotesIniEditor
    : public QObject
{
  Q_OBJECT

public:
  NotesIniEditor();

public slots:
  void run();

private:
  QTextStream cerr_;
};

#endif // NOTESINIEDITOR_H
