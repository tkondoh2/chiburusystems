﻿#ifndef OPERATOR_H
#define OPERATOR_H

#include "optionvalues.h"
#include <QTextStream>

class Result;

class Operator
{
public:
  Operator(const OptionValues& values);
  virtual Result operator ()() = 0;

  const OptionValues& values() const
  {
    return values_;
  }

protected:
  QTextStream cout_;

private:
  OptionValues values_;
};

#endif // OPERATOR_H
