﻿#include "result.h"

#include <QString>
#include <QObject>

Result::Result()
  : code_(0)
{
}

Result::Result(int code)
  : code_(code)
{
}

int Result::code() const
{
  return code_;
}

bool Result::hasError() const
{
  return code_ != 0;
}

QString Result::message() const
{
  switch (code_)
  {
  case NoError:
    return QObject::tr("No Error.");
  case NoArguments:
    return QObject::tr("No Arguments.");
  case UndefinedName:
    return QObject::tr("Undefined Name.");
  case UnknownOperation:
    return QObject::tr("Unknown Operation.");
  }
  return QObject::tr("Unknown Error.");
}
