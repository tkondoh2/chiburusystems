﻿#include "removefromlist.h"
#include "result.h"

#include <ntlx_core/environment.h>
#include <ntlx_core/textlist.h>
#include <QObject>

RemoveFromList::RemoveFromList(const OptionValues& optionValues)
  : Operator(optionValues)
{
}

Result RemoveFromList::operator ()()
{
  ntlx::Text name = ntlx::Text::fromQString(values().name());

  ntlx::Text oldValue = ntlx::Environment::value(name);
  ntlx::Text newValue;
  if (!oldValue.isEmpty())
  {
    QStringList qList = oldValue.toQString().split(",");
    QString qItem = values().value();
    if (qList.contains(qItem))
    {
      qList.removeAll(qItem);
      newValue = ntlx::Text::fromQString(qList.join(","));
      ntlx::Environment::setValue(name, newValue);
      cout_ << QObject::tr("The value \"%2\" was deleted"
                           " from the list of environment variable \"%1\".")
               .arg(values().name())
               .arg(values().value())
            << endl
               ;
    }
    else
      cout_ << QObject::tr("The value \"%2\" is not found"
                           " in the list of environment variable \"%1\".")
               .arg(values().name())
               .arg(values().value())
            << endl
               ;
  }
  else
    cout_ << QObject::tr("Environment variable \"%1\" has no value.")
             .arg(values().name())
          << endl
             ;
  return Result();
}
