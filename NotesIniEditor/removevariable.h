﻿#ifndef REMOVEVARIABLE_H
#define REMOVEVARIABLE_H

#include "operator.h"

class RemoveVariable
    : public Operator
{
public:
  RemoveVariable(const OptionValues& optionValues);

  virtual Result operator ()() override;
};

#endif // REMOVEVARIABLE_H
