﻿#ifndef NTLX_CORE_MAIN_H
#define NTLX_CORE_MAIN_H

#include <ntlx_core/ntlx_core_global.h>
#include <ntlx_core/resultful.h>

NTLX_CORE_BEGIN

/**
 * @brief Notes API初期化用簡易クラス
 */
class NTLX_CORESHARED_EXPORT Main
    : public Resultful
{
public:
  /**
   * @brief コンストラクタ
   * @param argc コマンドライン引数の数
   * @param argv 引数の文字列配列
   */
  Main(int argc, char** argv);

  /**
   * @brief デストラクタ
   */
  virtual ~Main();
};

NTLX_CORE_END

#endif // NTLX_CORE_MAIN_H
