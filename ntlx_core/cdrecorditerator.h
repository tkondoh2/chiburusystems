﻿#ifndef NTLX_CORE_CDRECORDITERATOR_H
#define NTLX_CORE_CDRECORDITERATOR_H

#include <ntlx_core/ntlx_core_global.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class CdRecordIteratorInterface
{
public:
  virtual void operator ()(
      char* RecordPtr
      , WORD RecordType
      , DWORD RecordLength
      ) = 0;
};

template <class ItemClass, class ListClass>
class CdRecordIterator
    : public CdRecordIteratorInterface
{
public:
  CdRecordIterator(ListClass* pList)
    : pList_(pList)
  {
  }

  virtual void operator ()(
      char* RecordPtr
      , WORD RecordType
      , DWORD RecordLength
      ) override
  {
    pList_->append(ItemClass(RecordPtr, RecordType, RecordLength));
  }

private:
  ListClass* pList_;
};

NTLX_CORE_END

#endif // NTLX_CORE_CDRECORDITERATOR_H
