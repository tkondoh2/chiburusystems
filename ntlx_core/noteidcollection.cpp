﻿#include "noteidcollection.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <idtable.h>
#include <nif.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

NoteIdCollection::const_iterator::const_iterator(
    NoteIdCollection* pCollection
    , NOTEID noteId
    , bool isLast
    )
  : pCollection_(pCollection)
  , noteId_(noteId)
  , isLast_(isLast)
{
}

NoteIdCollection::const_iterator::const_iterator(
    const NoteIdCollection::const_iterator& other
    )
  : pCollection_(other.pCollection_)
  , noteId_(other.noteId_)
  , isLast_(other.isLast_)
{
}

NoteIdCollection::const_iterator& NoteIdCollection::const_iterator::operator ++()
{
  isLast_ = !IDScan(pCollection_->handle_, noteId_ == 0, &noteId_);
  return *this;
}

NoteIdCollection::const_iterator NoteIdCollection::const_iterator::operator ++(int)
{
  const_iterator it = *this;
  operator++();
  return it;
}

bool operator ==(
    const NoteIdCollection::const_iterator& lhs
    , const NoteIdCollection::const_iterator& rhs
    )
{
  if (lhs.isLast_ && rhs.isLast_)
    return true;
  return (lhs.pCollection_ == rhs.pCollection_ && lhs.noteId_ == rhs.noteId_);
}

bool operator !=(
    const NoteIdCollection::const_iterator& lhs
    , const NoteIdCollection::const_iterator& rhs
    )
{
  return !operator ==(lhs, rhs);
}

NoteIdCollection::NoteIdCollection()
  : Resultful()
  , handle_(NULLHANDLE)
{
  lastResult_ = IDCreateTable(sizeof(NOTEID), &handle_);
}


NoteIdCollection::NoteIdCollection(const NoteIdCollection& other)
  : Resultful()
  , handle_(NULLHANDLE)
{
  lastResult_ = IDTableCopy(other.handle_, &handle_);
}

NoteIdCollection& NoteIdCollection::operator =(const NoteIdCollection& other)
{
  if (this != &other)
  {
    lastResult_ = IDDestroyTable(handle_);
    handle_ = NULLHANDLE;
    lastResult_ = IDTableCopy(other.handle_, &handle_);
  }
  return *this;
}


NoteIdCollection::~NoteIdCollection()
{
  if (handle_)
    IDDestroyTable(handle_);
}

NoteIdCollection::const_iterator NoteIdCollection::constBegin() const
{
  if (!handle_)
    return constEnd();
  return ++const_iterator(const_cast<NoteIdCollection*>(this), (NOTEID)0);
}

NoteIdCollection::const_iterator NoteIdCollection::constEnd() const
{
  return const_iterator(const_cast<NoteIdCollection*>(this), (NOTEID)0, true);
}

NoteIdCollection::const_iterator NoteIdCollection::insert(NOTEID noteId)
{
  if (noteId & NOTEID_CATEGORY)
    return constEnd();
  Q_ASSERT(handle_);
  lastResult_ = IDInsert(handle_, noteId, nullptr);
  return const_iterator(this, noteId);
}

DWORD NoteIdCollection::size() const
{
  return (handle_ == NULLHANDLE ? 0 : IDEntries(handle_));
}

NTLX_CORE_END
