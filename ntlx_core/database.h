﻿#ifndef NTLX_CORE_DATABASE_H
#define NTLX_CORE_DATABASE_H

#include <ntlx_core/file.h>

NTLX_CORE_BEGIN

class NoteIdCollection;
class FormulaStatement;
//class Formula;
//class DbInfo;
//class ItemTable;
//#ifndef ItemTableList
//using ItemTableList = QList<ItemTable>;
//#endif

class NTLX_CORESHARED_EXPORT Database
    : public File
{
public:

  explicit Database();

  explicit Database(const FilePath& path, bool withOpen = true);

  virtual ~Database();

  ntlx::NoteIdCollection getNoteIdCollection(
      const FormulaStatement& statement
      ) const;

  ntlx::NoteIdCollection getNoteIdCollectionByForm(const Text& formName) const;
};

inline Database::Database()
  : File()
{
}

inline Database::Database(const FilePath& path, bool withOpen)
  : File(path, withOpen)
{
}

inline Database::~Database()
{
}

NTLX_CORE_END

#endif // NTLX_CORE_DATABASE_H
