﻿#ifndef NTLX_CORE_FILEPATH_H
#define NTLX_CORE_FILEPATH_H

#include <ntlx_core/resultful.h>
#include <ntlx_core/text.h>

#include <QString>

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT FilePath
    : public Resultful
{
public:
  FilePath();

  FilePath(
      const Text& path
      , const Text& server = Text()
      , const Text& port = Text()
      );

  FilePath(
      const QString& qPpath
      , const QString& qServer = QString()
      , const QString& qPort = QString()
      );

  FilePath(const FilePath& other);

  FilePath& operator =(const FilePath& other);

  virtual ~FilePath();

  Text path() const { return path_; }

  Text server() const { return server_; }

  Text port() const { return port_; }

  QString qPath() const { return path_.toQString(); }

  QString qServer() const { return server_.toQString(); }

  QString qPort() const { return port_.toQString(); }

  Text netPath() const;

  bool isEmpty() const { return path_.isEmpty(); }

  static ApiResult fromNetPath(const Text& netPath, FilePath* filePath);

private:
  Text path_;
  Text server_;
  Text port_;
};

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::FilePath)

#endif // NTLX_CORE_FILEPATH_H
