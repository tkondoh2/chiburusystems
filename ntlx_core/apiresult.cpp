﻿#include <ntlx_core/apiresult.h>
#include <ntlx_core/text.h>
#include <QString>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmisc.h>
#include <misc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN



Text ApiResult::toText() const
{
  char buffer[MAXSPRINTF + 1];
  WORD length = OSLoadString(NULLHANDLE, error(), buffer, MAXSPRINTF);
  return Text(buffer, length);
}

QString ApiResult::toQString() const
{
  return toText().toQString();
}

NTLX_CORE_END
