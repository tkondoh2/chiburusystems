﻿#include <ntlx_core/main.h>

NTLX_CORE_BEGIN

Main::Main(int argc, char** argv)
  : Resultful()
{
  lastResult_ = ::NotesInitExtended(argc, argv);
}

Main::~Main()
{
  ::NotesTerm();
}

NTLX_CORE_END
