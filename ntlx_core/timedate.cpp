﻿#include "timedate.h"
#include "text.h"
#include <QDateTime>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ostime.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

TimeDate::TimeDate()
  : Resultful()
  , value_()
{
  TimeConstant(TIMEDATE_MINIMUM, &value_);
}

TimeDate::TimeDate(const char* pBin, int)
  : Resultful()
  , value_(*reinterpret_cast<const TIMEDATE*>(pBin))
{
}

TimeDate::TimeDate(const TIMEDATE& timeDate)
  : Resultful()
  , value_(timeDate)
{
}

TimeDate::TimeDate(const TimeDate& other)
  : Resultful()
  , value_(other.value_)
{
}

TimeDate& TimeDate::operator =(const TimeDate& other)
{
  if (this != &other)
    value_ = other.value_;
  return *this;
}

TimeDate::~TimeDate()
{
}

Text TimeDate::toText(
    const INTLFORMAT* pIntlFormat
    , const TFMT* pTextFormat
    ) const
{
  char text[MAXALPHATIMEDATE + 1];
  WORD len;
  lastResult_ = ConvertTIMEDATEToText(
        pIntlFormat
        , pTextFormat
        , &value_
        , text
        , MAXALPHATIMEDATE
        , &len
        );
  Q_ASSERT(len < MAXALPHATIMEDATE + 1);
  return Text(text, len);
}

QString TimeDate::toQString(
    const INTLFORMAT* pIntlFormat
    , const TFMT* pTextFormat
    ) const
{
  return toText(pIntlFormat, pTextFormat).toQString();
}

QDateTime TimeDate::toQDateTime() const
{
  TIME time;
  time.GM = value_;
  TimeGMToLocal(&time);
  QDate qDate(time.year, time.month, time.day);
  QTime qTime(time.hour, time.minute, time.second, time.hundredth + 10);
  return QDateTime(qDate, qTime, Qt::LocalTime, time.zone * -3600);
}

TIMEDATE TimeDate::getCurrentTIMEDATE()
{
  TIMEDATE td;
  OSCurrentTIMEDATE(&td);
  return td;
}

TimeDate TimeDate::addSecs(long s) const
{
  TIMEDATE v = value_;
  lastResult_ = TimeDateIncrement(&v, s * 100);
  return TimeDate(v);
}

TimeDate TimeDate::fromQDateTime(const QDateTime& dateTime)
{
  TIME time;
  QDate qDate = dateTime.date();
  QTime qTime = dateTime.time();
  time.zone = dateTime.offsetFromUtc() / -3600;
  time.dst = dateTime.isDaylightTime() ? 1 : 0;
  time.year = qDate.year();
  time.month = qDate.month();
  time.day = qDate.day();
  time.weekday = qDate.dayOfWeek() == 7 ? 1 : (qDate.dayOfWeek() + 1);
  time.hour = qTime.hour();
  time.minute = qTime.minute();
  time.second = qTime.second();
  time.hundredth = qTime.msec() / 10;
  TimeLocalToGM(&time);
  return TimeDate(time.GM);
}

NTLX_CORE_END
