﻿#ifndef NTLX_CORE_NOTEIDCOLLECTION_H
#define NTLX_CORE_NOTEIDCOLLECTION_H

#include <ntlx_core/resultful.h>
#include <ntlx_core/note.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class SearchedNoteId;

class NTLX_CORESHARED_EXPORT NoteIdCollection
    : public Resultful
{
public:
  class NTLX_CORESHARED_EXPORT const_iterator
  {
  public:
    const_iterator(NoteIdCollection* pCollection, NOTEID noteId, bool isLast = false);

    const_iterator(const const_iterator& other);

    const_iterator& operator =(const const_iterator& other);

    NOTEID operator *() const { return noteId_; }

    const_iterator& operator++();

    const_iterator operator++(int);

    friend NTLX_CORESHARED_EXPORT bool operator ==(const const_iterator& lhs, const const_iterator& rhs);

    friend NTLX_CORESHARED_EXPORT bool operator !=(const const_iterator& lhs, const const_iterator& rhs);

    NoteIdCollection* pCollection_;
    NOTEID noteId_;
    bool isLast_;
  };

  NoteIdCollection();

  NoteIdCollection(const NoteIdCollection& other);

  NoteIdCollection& operator =(const NoteIdCollection& other);

  virtual ~NoteIdCollection();

  const_iterator constBegin() const;

  const_iterator constEnd() const;

  const_iterator insert(NOTEID noteId);

  void append(const SearchedNoteId& snid);

  DWORD size() const;

  bool isEmpty() const;

private:
  DHANDLE handle_;
};

class NTLX_CORESHARED_EXPORT SearchedNoteId
{
public:
  SearchedNoteId(SEARCH_MATCH* pMatch, ITEM_TABLE* /*pTable*/)
    : noteId_(pMatch->ID.NoteID)
  {
  }

  NOTEID nodeId() const
  {
    return noteId_;
  }

private:
  NOTEID noteId_;
};


inline void NoteIdCollection::append(const SearchedNoteId &snid)
{
  insert(snid.nodeId());
}

inline bool NoteIdCollection::isEmpty() const
{
  return (size() == 0);
}

NTLX_CORE_END

#endif // NTLX_CORE_NOTEIDCOLLECTION_H
