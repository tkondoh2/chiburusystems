﻿#ifndef NTLX_CORE_CDRECORD_H
#define NTLX_CORE_CDRECORD_H

#include <ntlx_core/ntlx_core_global.h>
#include <QByteArray>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class Text;

class ReadMemoryFunctor
{
public:
  void operator ()(char**, DWORD){}
};

class NTLX_CORESHARED_EXPORT CdRecord
{
public:
  CdRecord();
  CdRecord(
      char* RecordPtr
      , WORD RecordType
      , DWORD RecordLength
      );
  CdRecord(const CdRecord& other);
  CdRecord& operator =(const CdRecord& other);
  virtual ~CdRecord();

  const char* constData() const;

  WORD signature() const;
  WORD odsLength() const;
  Text signatureName() const;

  template <class T, WORD type, class F = ReadMemoryFunctor>
  T extract(F functor = ReadMemoryFunctor(), WORD iterations = 1) const
  {
    T value;
    char* ptr = const_cast<char*>(bytes_.constData());
    ODSReadMemory(&ptr, type, &value, iterations);
    functor(&ptr, static_cast<DWORD>(value.Header.Length - ODSLength(type)));
    return value;
  }

  static Text signatureName(WORD type);

private:
  QByteArray bytes_;
};

NTLX_CORE_END

#endif // NTLX_CORE_CDRECORD_H
