﻿#ifndef NTLX_CORE_APIRESULT_H
#define NTLX_CORE_APIRESULT_H

#include <ntlx_core/ntlx_core_global.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class Text;

/**
 * @brief 処理結果パラメータを保持するクラス
 */
class NTLX_CORESHARED_EXPORT ApiResult
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  ApiResult();

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  ApiResult(const ApiResult& other);

  /**
   * @brief STATUS値から生成
   * @param status STATUS値
   */
  ApiResult(STATUS status);

  /**
   * @brief 代入演算子
   * @param other 代入元オブジェクト
   * @return 自身への参照
   */
  ApiResult& operator =(const ApiResult& other);

  /**
   * @brief マスク済みエラーコードを返す。
   * @return マスク済みエラーコード
   */
  STATUS error() const;

  /**
   * @brief エラーがなければ真を返す。
   * @return エラーがなければ真
   */
  bool noError() const;

  /**
   * @brief エラーがあれば真を返す。
   * @return エラーがあれば真
   */
  bool hasError() const;

  /**
   * @brief エラーメッセージを返す
   * @return エラーメッセージ(LMBCSオブジェクト)
   */
  Text toText() const;

  /**
   * @brief エラーメッセージを返す
   * @return エラーメッセージ(QStringオブジェクト)
   */
  QString toQString() const;

private:
  /**
   * @brief Notes API処理結果変数
   */
  STATUS status_;
};

inline ApiResult::ApiResult()
  : status_(NOERROR)
{
}

inline ApiResult::ApiResult(const ApiResult& other)
  : status_(other.status_)
{
}

inline ApiResult::ApiResult(STATUS status)
  : status_(status)
{
}

inline ApiResult& ApiResult::operator =(const ApiResult& other)
{
  if (this != &other)
    status_ = other.status_;
  return *this;
}

inline STATUS ApiResult::error() const
{
  return ERR(status_);
}

inline bool ApiResult::noError() const
{
  return error() == NOERROR;
}

inline bool ApiResult::hasError() const
{
  return !noError();
}

NTLX_CORE_END

#endif // NTLX_CORE_APIRESULT_H
