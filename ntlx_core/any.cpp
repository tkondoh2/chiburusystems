﻿#include <ntlx_core/any.h>
#include <ntlx_core/textlist.h>
#include <ntlx_core/numberrange.h>
#include <ntlx_core/timedaterange.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>
#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

Any::Any()
  : type_(TYPE_INVALID_OR_UNKNOWN)
  , value_()
{
}

Any::Any(WORD type, const char* value)
  : type_(type)
  , value_(value)
{
}

Any::Any(WORD type, const char* value, int size)
  : type_(type)
  , value_(value, size)
{
}

Any::Any(
    BLOCKID& /*bidItem*/
    , WORD dataType
    , BLOCKID& bidValue
    , DWORD valueLength
    )
  : type_(dataType)
  , value_()
{
  char* pValue = OSLockBlock(char, bidValue) + sizeof(WORD);
  value_ = QByteArray(pValue, static_cast<int>(valueLength - sizeof(WORD)));
  OSUnlockBlock(bidValue);
}

Any::Any(const Any &other)
  : type_(other.type_)
  , value_(other.value_)
{
}

Any& Any::operator =(const Any& other)
{
  if (this != &other)
  {
    type_ = other.type_;
    value_ = other.value_;
  }
  return *this;
}

Any::~Any()
{
}

bool Any::isValid() const
{
  return (type_ != TYPE_INVALID_OR_UNKNOWN);
}

char Any::operator [](int index) const
{
  if (index < 0 || index >= size()) return '\0';
  return value_[index];
}

Text Any::getText() const
{
  if (type() == TYPE_TEXT)
    return extract<TYPE_TEXT, Text>();
  else if (type() == TYPE_TEXT_LIST)
  {
    TextList list = extract<TYPE_TEXT_LIST, TextList>();
    if (!list.isEmpty())
      return list.at(0);
  }
  return Text();
}

TextList Any::getTextList() const
{
  TextList list;
  if (type() == TYPE_TEXT)
  {
    Text item = extract<TYPE_TEXT, Text>();
    list.append(item);
    return list;
  }
  else if (type() == TYPE_TEXT_LIST)
  {
    return extract<TYPE_TEXT_LIST, TextList>();
  }
  return list;
}

Text Any::toText() const
{
  if (type_ == TYPE_TEXT_LIST)
  {
    const TextList& list = extract<TYPE_TEXT_LIST, TextList>();
    if (list.isEmpty())
      return Text();
    return list.at(0);
  }
  return extract<TYPE_TEXT, Text>();
}

Number Any::toNumber() const
{
  if (type_ == TYPE_NUMBER_RANGE)
  {
    const NumberRange& range = extract<TYPE_NUMBER_RANGE, NumberRange>();
    if (range.singles().isEmpty())
      return Number();
    return range.singles().at(0);
  }
  return extract<TYPE_NUMBER, Number>();
}

TimeDate Any::toTimeDate() const
{
  if (type_ == TYPE_TIME_RANGE)
  {
    const TimeDateRange& range = extract<TYPE_TIME_RANGE, TimeDateRange>();
    if (range.singles().isEmpty())
      return TimeDate();
    return range.singles().at(0);
  }
  return extract<TYPE_TIME, TimeDate>();
}

TextList Any::toTextList() const
{
  return extract<TYPE_TEXT_LIST, TextList>();
}

NumberRange Any::toNumberRange() const
{
  return extract<TYPE_NUMBER_RANGE, NumberRange>();
}

TimeDateRange Any::toTimeDateRange() const
{
  return extract<TYPE_TIME_RANGE, TimeDateRange>();
}

QString Any::typeName(WORD type)
{
  QString className, typeName_;

  switch (type & CLASS_MASK)
  {
  case CLASS_NOCOMPUTE: className = "NOCOMPUTE"; break;
  case CLASS_ERROR: className = "ERROR"; break;
  case CLASS_UNAVAILABLE: className = "UNAVAILABLE"; break;
  case CLASS_NUMBER: className = "NUMBER"; break;
  case CLASS_TIME: className = "TIME"; break;
  case CLASS_TEXT: className = "TEXT"; break;
  case CLASS_FORMULA: className = "FORMULA"; break;
  case CLASS_USERID: className = "USERID"; break;
  }

  switch (type)
  {
  case TYPE_ERROR: typeName_ = "ERROR"; break;
  case TYPE_UNAVAILABLE: typeName_ = "UNAVAILABLE"; break;
  case TYPE_TEXT: typeName_ = "TEXT"; break;
  case TYPE_TEXT_LIST: typeName_ = "TEXT_LIST"; break;
  case TYPE_NUMBER: typeName_ = "NUMBER"; break;
  case TYPE_NUMBER_RANGE: typeName_ = "NUMBER_RANGE"; break;
  case TYPE_TIME: typeName_ = "TIME"; break;
  case TYPE_TIME_RANGE: typeName_ = "TIME_RANGE"; break;
  case TYPE_FORMULA: typeName_ = "FORMULA"; break;
  case TYPE_USERID: typeName_ = "USERID"; break;
  case TYPE_INVALID_OR_UNKNOWN: typeName_ = "INVALID_OR_UNKNOWN"; break;
  case TYPE_COMPOSITE: typeName_ = "COMPOSITE"; break;
  case TYPE_COLLATION: typeName_ = "COLLATION"; break;
  case TYPE_OBJECT: typeName_ = "OBJECT"; break;
  case TYPE_NOTEREF_LIST: typeName_ = "NOTEREF_LIST"; break;
  case TYPE_VIEW_FORMAT: typeName_ = "VIEW_FORMAT"; break;
  case TYPE_ICON: typeName_ = "ICON"; break;
  case TYPE_NOTELINK_LIST: typeName_ = "NOTELINK_LIST"; break;
  case TYPE_SIGNATURE: typeName_ = "SIGNATURE"; break;
  case TYPE_SEAL: typeName_ = "SEAL"; break;
  case TYPE_SEALDATA: typeName_ = "SEALDATA"; break;
  case TYPE_SEAL_LIST: typeName_ = "SEAL_LIST"; break;
  case TYPE_HIGHLIGHTS: typeName_ = "HIGHLIGHTS"; break;
  case TYPE_WORKSHEET_DATA: typeName_ = "WORKSHEET_DATA"; break;
  case TYPE_USERDATA: typeName_ = "USERDATA"; break;
  case TYPE_QUERY: typeName_ = "QUERY"; break;
  case TYPE_ACTION: typeName_ = "ACTION"; break;
  case TYPE_ASSISTANT_INFO: typeName_ = "ASSISTANT_INFO"; break;
  case TYPE_VIEWMAP_DATASET: typeName_ = "VIEWMAP_DATASET"; break;
  case TYPE_VIEWMAP_LAYOUT: typeName_ = "VIEWMAP_LAYOUT"; break;
  case TYPE_LSOBJECT: typeName_ = "LSOBJECT"; break;
  case TYPE_HTML: typeName_ = "HTML"; break;
  case TYPE_SCHED_LIST: typeName_ = "SCHED_LIST"; break;
  case TYPE_CALENDAR_FORMAT: typeName_ = "CALENDAR_FORMAT"; break;
  case TYPE_MIME_PART: typeName_ = "MIME_PART"; break;
  case TYPE_RFC822_TEXT: typeName_ = "RFC822_TEXT"; break;
  case TYPE_SEAL2: typeName_ = "SEAL2"; break;
  }

  return QString("%1/%2").arg(typeName_).arg(className);
}

NTLX_CORE_END
