﻿#include "database.h"
#include "formula.h"
#include "noteidcollection.h"
#include "searchiterator.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfnote.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

ntlx::NoteIdCollection Database::getNoteIdCollection(
    const FormulaStatement& statement
    ) const
{
  Text criteria = statement.replacedText();
  ntlx::NoteIdCollection idCol;
  ntlx::SearchIterator<ntlx::SearchedNoteId, ntlx::NoteIdCollection>
      iterator(&idCol);
  search(&iterator, criteria, ntlx::Text(), 0, NOTE_CLASS_DOCUMENT);
  return idCol;
}

ntlx::NoteIdCollection Database::getNoteIdCollectionByForm(
    const Text& formName
    ) const
{
  ntlx::FormulaStatement stmt("Form=\"{{form}}\"");
  stmt.setParam("{{form}}", formName);
  return getNoteIdCollection(stmt);
}

NTLX_CORE_END
