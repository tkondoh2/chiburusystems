﻿#ifndef NTLX_CORE_GLOBAL_H
#define NTLX_CORE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(NTLX_CORE_LIBRARY)
#  define NTLX_CORESHARED_EXPORT Q_DECL_EXPORT
#else
#  define NTLX_CORESHARED_EXPORT Q_DECL_IMPORT
#endif

#define NTLX_CORE_BEGIN namespace ntlx {
#define NTLX_CORE_END   }

NTLX_CORE_BEGIN

/**
 * @brief 代入元ポインタがNullでも安全に代入できる。
 */
template <typename T>
void assign(T* pValue, const T result)
{
  if (pValue != nullptr)
    *pValue = result;
}

NTLX_CORE_END

#endif // NTLX_CORE_GLOBAL_H
