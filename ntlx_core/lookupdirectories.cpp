﻿#include "lookupdirectories.h"

#include <ntlx_core/any.h>
#include <ntlx_core/text.h>
#include <ntlx_core/numberrange.h>
#include <ntlx_core/timedaterange.h>

#include <memory>

#include <QDateTime>
#include <QJsonDocument>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <lookup.h>
#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

QMap<QString,QVariant> LookupUser::toMap(const QList<LookupUser>& users)
{
  QMap<QString,QVariant> usersMap;
  foreach (LookupUser user, users)
  {
    usersMap[user.name()] = user.items();
  }
  return usersMap;
}

QJsonDocument LookupUser::toJson(const QList<LookupUser>& users)
{
  return QJsonDocument::fromVariant(toMap(users));
}

LookupDirectories::LookupDirectories(
    const QString& nameSpace
    , int bufferSize
    )
  : Resultful()
  , nameSpace_(nameSpace)
  , bufferSize_(bufferSize)
{
}

QList<LookupUser> LookupDirectories::operator ()(
    const QStringList& nameList
    , const QStringList& itemNameList
    ) const
{
  TextArray nameSpaceArray(nameSpace_);
  TextArray nameArray(nameList);
  TextArray itemNameArray(itemNameList);

  QList<LookupUser> users;
  DHANDLE handle = NULLHANDLE;
  lastResult_ = NAMELookup(
        nullptr
        , 0
        , 1
        , *nameSpaceArray
        , nameList.count()
        , *nameArray
        , itemNameList.count()
        , *itemNameArray
        , &handle
        );
  if (lastResult().hasError())
    return users;

  void* pLookup = OSLockObject(handle);
  void* pName = nullptr;
  foreach (QString name, nameList)
  {
    WORD numMatches = 0;
    pName = NAMELocateNextName(pLookup, pName, &numMatches);
    if (pName == nullptr)
      break;

    LookupUser user(name);
    void* pMatch = nullptr;
    for (WORD iMatch = 0; iMatch < numMatches; ++iMatch)
    {
      pMatch = NAMELocateNextMatch(pLookup, pName, pMatch);
      for (WORD i = 0; i < itemNameList.count(); ++i)
      {
        WORD dataType, size;
        void* ptr = NAMELocateItem(pMatch, i, &dataType, &size);
        QVariant data;
        if (ptr != nullptr)
        {
          switch (dataType)
          {
          case TYPE_TEXT:
          {
            std::unique_ptr<char> buffer(new char[bufferSize_]);
            NAMEGetTextItem(pMatch, i, 0, buffer.get(), bufferSize_ - 1);
            data = ntlx::Text(buffer.get()).toQString();
          }
            break;

          case TYPE_TEXT_LIST:
          {
            LIST* pList = (LIST*)(((WORD*)ptr) + 1);
            QStringList list;
            std::unique_ptr<char> buffer(new char[bufferSize_]);
            for (WORD l = 0; l < pList->ListEntries; ++l)
            {
              NAMEGetTextItem(pMatch, i, l, buffer.get(), bufferSize_ - 1);
              list.append(ntlx::Text(buffer.get()).toQString());
            }
            data = list;
          }
            break;

          default:
          {
            ntlx::Any any = ntlx::Any::fromBinary<WORD>(ptr, size);
            switch (any.type())
            {
            case TYPE_NUMBER:
              data = any.toNumber().value();
              break;
            case TYPE_NUMBER_RANGE:
              data = any.toNumberRange().toQVariant();
              break;
            case TYPE_TIME:
              data = any.toTimeDate().toQDateTime();
              break;
            case TYPE_TIME_RANGE:
              data = any.toTimeDateRange().toQVariant();
              break;
            default:
              data = QString("Unknown");
              break;
            }
          }
            break;
          }
        }
        user.addItem(itemNameList[i], data);
      }
    }
    users.append(user);
  }

  OSUnlockObject(handle);
  OSMemFree(handle);
  return users;
}

const char* LookupDirectories::TextArray::operator *()
{
  QByteArrayList byteList;
  foreach (QString item, list_)
  {
    byteList.append(lmbcs(item).constData());
  }
  data_ = byteList.join('\0');
  return data_.constData();
}

NTLX_CORE_END
