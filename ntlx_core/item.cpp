﻿#include "item.h"
#include "cdrecorditerator.h"
#include "allcompositeiterator.h"
#include "textlist.h"
#include "note.h"
#include "cdrecord.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfnote.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

Item::Item()
  : Resultful()
  , name_()
  , flags_(0)
  , values_()
{
}

Item::Item(const Text& name, WORD flags)
  : Resultful()
  , name_(name)
  , flags_(flags)
  , values_()
{
}

Item::Item(
    WORD ItemFlags
    , char *Name
    , WORD NameLength
    , void *Value
    , DWORD ValueLength
    )
  : Resultful()
  , name_(Text(Name, NameLength))
  , flags_(ItemFlags)
  , values_()
{
  append(Any::fromBinary<DWORD>(Value, ValueLength));
}

Item::Item(const Item& other)
  : Resultful()
  , name_(other.name_)
  , flags_(other.flags_)
  , values_(other.values_)
{
}

Item& Item::operator =(const Item& other)
{
  if (this != &other)
  {
    name_ = other.name_;
    flags_ = other.flags_;
    values_ = other.values_;
  }
  return *this;
}

Item::~Item()
{
}

void Item::append(const Any& value)
{
  values_.append(value);
}

WORD Item::type() const
{
  if (values_.isEmpty()) return TYPE_INVALID_OR_UNKNOWN;
  return values_.at(0).type();
}
QString Item::typeName() const
{
  if (values_.isEmpty()) return Any::typeName(TYPE_INVALID_OR_UNKNOWN);
  return values_.at(0).typeName();
}

int Item::valueCount() const
{
  return values_.count();
}

Any Item::valueAt(int i) const
{
  return values_.at(i);
}

Any Item::value() const
{
  if (values_.isEmpty())
    return Any();
  return values_.at(0);
}

Text Item::name() const
{
  return name_;
}

WORD Item::flags() const
{
  return flags_;
}

TextList Item::flagNames() const
{
  TextList names;
  if (flags_ & ITEM_SIGN) names.append(Text("SIGN"));
  if (flags_ & ITEM_SEAL) names.append(Text("SEAL"));
  if (flags_ & ITEM_SUMMARY) names.append(Text("SUMMARY"));
  if (flags_ & ITEM_READWRITERS) names.append(Text("READWRITERS"));
  if (flags_ & ITEM_NAMES) names.append(Text("NAMES"));
  if (flags_ & ITEM_PLACEHOLDER) names.append(Text("PLACEHOLDER"));
  if (flags_ & ITEM_PROTECTED) names.append(Text("PROTECTED"));
  if (flags_ & ITEM_READERS) names.append(Text("READERS"));
  if (flags_ & ITEM_UNCHANGED) names.append(Text("UNCHANGED"));
  return names;
}

const Item& Item::getAllCdRecords(const Note& note, QList<CdRecord>& cdrecords) const
{
  CdRecordIterator<CdRecord, QList<CdRecord>> cdrIt(&cdrecords);
  AllCompositeIterator allCompIt(&cdrIt);
  lastResult_ = note.enumItemInfo(name_, &allCompIt).lastResult();
  return *this;
}

NTLX_CORE_END
