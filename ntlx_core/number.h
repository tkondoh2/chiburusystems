﻿#ifndef NTLX_CORE_NUMBER_H
#define NTLX_CORE_NUMBER_H

#include <ntlx_core/any.h>
#include <ntlx_core/resultful.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <intl.h>
#include <misc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT Number
    : public Resultful
{
public:

  Number();

  Number(const char* pBin, int);

  Number(NUMBER num);

  Number(const Number& other);

  virtual ~Number();

  Number& operator =(const Number& other);

  Text toText(
      const INTLFORMAT* pIntlFormat = nullptr
      , const NFMT* pTextFormat = nullptr
      ) const;

  QString toQString(
      const INTLFORMAT* pIntlFormat = nullptr
      , const NFMT* pTextFormat = nullptr
      ) const;

  NUMBER value() const;

  QVariant toQVariant() const;

private:
  NUMBER value_;
};

inline NUMBER Number::value() const
{
  return value_;
}

inline QVariant Number::toQVariant() const
{
  return value_;
}

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::Number)

#endif // NTLX_CORE_NUMBER_H
