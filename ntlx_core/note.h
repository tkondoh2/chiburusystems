﻿#ifndef NTLX_CORE_NOTE_H
#define NTLX_CORE_NOTE_H

#include <ntlx_core/resultful.h>
#include <ntlx_core/itemiterator.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class Database;
class ItemInfoIteratorInterface;
class Item;
class TextList;

#ifndef AttachFileMap
using AttachFileMap = QMap<ntlx::Text, BLOCKID>;
#endif

class NTLX_CORESHARED_EXPORT Note
    : public Resultful
{
public:

  Note();

  Note(Database& db, NOTEID noteId);

  Note(WORD noteClass, Database& db);

  Note(const Note& other);

  Note& operator =(const Note& other);

  virtual ~Note();

  operator NOTEHANDLE () const { return handle_; }

  NOTEID noteId() const;

  const Note& open(WORD openFlags = 0) const;

  const Note& close() const;

  const Note& scanItems(ItemIteratorInterface* iterator) const;

  static STATUS LNPUBLIC scanItemsCallback(
      WORD Spare
      , WORD ItemFlags
      , char* Name
      , WORD NameLength
      , void* Value
      , DWORD ValueLength
      , void* RoutineParameter
      );

  const Note& enumItemInfo(
      const Text& name
      , ItemInfoIteratorInterface* iterator
      ) const;

  static ApiResult enumItemInfo(
      NOTEHANDLE hNote
      , const Text& name
      , ItemInfoIteratorInterface* iterator
      );

  Item getItem(const Text& name, WORD flags = 0) const;
  Text getText(const Text& name, WORD flags = 0) const;
  TextList getTextList(const Text& name, WORD flags = 0) const;

  template <typename T>
  T getNumber(const Text& name, T defaultValue = 0, WORD flags = 0) const
  {
    Item item = getItem(name, flags);
    Any value = item.value();
    if (value.isValid())
      return static_cast<T>(value.toNumber().value());
    return defaultValue;
  }

  static Item getItem(NOTEHANDLE hNote, const Text& name, WORD flags = 0);

  static Text noteIdText(NOTEID noteId);
  static Text globalInstanceIdText(GLOBALINSTANCEID id);
  static Text originatorIdText(ORIGINATORID oid);
  static Text noteClassText(WORD noteClass);
  static TextList designFlagsTextList(const Text& flags);

  template <class T> // OID or UNID
  static Text universalIdText(T id)
  {
    return formatId<DWORD, uint>(id.File.Innards[1])
        + formatId<DWORD, uint>(id.File.Innards[0])
        + formatId<DWORD, uint>(id.Note.Innards[1])
        + formatId<DWORD, uint>(id.Note.Innards[0])
        ;
  }

  template <typename T>
  static T getInfo(NOTEHANDLE hNote, WORD _note_type)
  {
    T value;
    NSFNoteGetInfo(hNote, _note_type, &value);
    return value;
  }

  UNIVERSALNOTEID getUniversalNoteId() const;
  static UNIVERSALNOTEID getUniversalNoteId(NOTEHANDLE hNote);

  AttachFileMap getAllAttachFiles() const;

  QByteArray extractAttachFile(const Text& fileName) const;

  static STATUS LNPUBLIC extractAttachFileCallback(
      const BYTE* bytes
      , DWORD length
      , void* pParam
      );

  void setInfo(WORD _note_XXX, void* pData);
  void setNoteClass(WORD noteClass);

  void setTextItem(const Text& name, const Text& value, bool isSummary = true);
  void setTimeItem(const Text& name, const TimeDate& value);
  Note& update(WORD updateFlags = 0);

private:
  Database* pDb_;
  mutable NOTEID noteId_;
  mutable NOTEHANDLE handle_;
};

inline NOTEID Note::noteId() const
{
  return noteId_;
}

NTLX_CORE_END

#endif // NTLX_CORE_NOTE_H
