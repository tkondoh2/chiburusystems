﻿#include "loglevel.h"

#include <QString>
#include <QMap>

NTLX_CORE_BEGIN

QString logLevelText(LogLevel level)
{
  return LogLevelText::map_.value(level);
}

LogLevel logLevel(const QString &text)
{
  return LogLevelText::map_.key(text, LogLevel::Info);
}

const QMap<ntlx::LogLevel, QString> LogLevelText::map_ {
  { ntlx::LogLevel::None, "None" }
  , { ntlx::LogLevel::Fatal, "Fatal" }
  , { ntlx::LogLevel::Error, "Error" }
  , { ntlx::LogLevel::Warn, "Warn" }
  , { ntlx::LogLevel::Info, "Info" }
  , { ntlx::LogLevel::Debug, "Debug" }
  , { ntlx::LogLevel::Trace, "Trace" }
};

NTLX_CORE_END
