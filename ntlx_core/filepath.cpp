﻿#include "filepath.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osfile.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

FilePath::FilePath()
  : Resultful()
  , path_()
  , server_()
  , port_()
{
}

FilePath::FilePath(
    const Text& path
    , const Text& server
    , const Text& port
    )
  : Resultful()
  , path_(path)
  , server_(server)
  , port_(port)
{
}

FilePath::FilePath(
    const QString& qPath
    , const QString& qServer
    , const QString& qPort
    )
  : Resultful()
  , path_(ntlx::Text::fromQString(qPath))
  , server_(ntlx::Text::fromQString(qServer))
  , port_(ntlx::Text::fromQString(qPort))
{
}

FilePath::FilePath(const FilePath& other)
  : Resultful()
  , path_(other.path_)
  , server_(other.server_)
  , port_(other.port_)
{
}

FilePath& FilePath::operator =(const FilePath& other)
{
  if (this != &other)
  {
    path_ = other.path_;
    server_ = other.server_;
    port_ = other.port_;
  }
  return *this;
}

FilePath::~FilePath()
{
}

Text FilePath::netPath() const
{
  char retPath[MAXPATH];
  lastResult_ = OSPathNetConstruct(
        port_.isEmpty() ? nullptr : port_.constData()
        , server_.constData()
        , path_.constData()
        , retPath
        );
  return Text(retPath);
}

ApiResult FilePath::fromNetPath(const Text& netPath, FilePath* filePath)
{
  char path[MAXPATH], server[MAXPATH], port[MAXPATH];
  ApiResult result = OSPathNetParse(netPath.constData(), port, server, path);
  if (result.noError() && filePath)
  {
    filePath->port_ = Text(port);
    filePath->server_ = Text(server);
    filePath->path_ = Text(path);
  }
  return result;
}

NTLX_CORE_END
