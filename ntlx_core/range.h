﻿#ifndef NTLX_CORE_RANGE_H
#define NTLX_CORE_RANGE_H

#include <ntlx_core/resultful.h>
#include <ntlx_core/textlist.h>

NTLX_CORE_BEGIN

template <class SC, class PS, class Fmt>
class Pair
    : public Resultful
{
public:
  Pair()
    : Resultful()
    , value_()
  {
  }

  Pair(const PS* pData, int)
    : Resultful()
    , value_(*pData)
  {
  }

  Pair(const PS& data)
    : Resultful()
    , value_(data)
  {
  }

  Pair(const Pair<SC, PS, Fmt>& other)
    : Resultful()
    , value_(other.value_)
  {
  }

  Pair& operator =(const Pair<SC, PS, Fmt>& other)
  {
    if (this != &other)
    {
      value_ = other.value_;
    }
    return *this;
  }

  virtual ~Pair()
  {
  }

  virtual Text toText(
      const INTLFORMAT* pIntlFormat = nullptr
      , const Fmt* pTextFormat = nullptr
      ) const = 0;

  QString toQString(
      const INTLFORMAT* pIntlFormat = nullptr
      , const Fmt* pTextFormat = nullptr
      ) const
  {
    return toText(pIntlFormat, pTextFormat).toQString();
  }

protected:
  PS value_;
};

template <class SC, class PC, class SS, class PS, class Fmt>
class Range
    : public Resultful
{
public:
  Range()
    : Resultful()
    , singles_()
    , pairs_()
  {
  }

  Range(const char* pBin, int)
    : Resultful()
    , singles_()
    , pairs_()
  {
    RANGE* pRange = reinterpret_cast<RANGE*>(const_cast<char*>(pBin));
    SS* pSingle = reinterpret_cast<SS*>(pRange + 1);
    PS* pPair = reinterpret_cast<PS*>(pSingle + pRange->ListEntries);
    for (USHORT i = 0; i < pRange->ListEntries; ++i)
    {
      singles_.append(SC(*pSingle++));
    }
    for (USHORT i = 0; i < pRange->RangeEntries; ++i)
    {
      pairs_.append(PC(*pPair++));
    }
  }

  Range(const Range<SC, PC, SS, PS, Fmt>& other)
    : Resultful()
    , singles_(other.singles_)
    , pairs_(other.pairs_)
  {
  }

  Range& operator =(const Range<SC, PC, SS, PS, Fmt>& other)
  {
    if (this != &other)
    {
      singles_ = other.singles_;
      pairs_ = other.pairs_;
    }
    return *this;
  }

  virtual ~Range()
  {
  }

  TextList toTextList(
      const INTLFORMAT* pIntlFormat = nullptr
      , const Fmt* pTextFormat = nullptr
      ) const
  {
    TextList list;
    foreach (SC item, singles_)
    {
      list.append(item.toText(pIntlFormat, pTextFormat));
    }
    foreach (PC pair, pairs_)
    {
      list.append(pair.toText(pIntlFormat, pTextFormat));
    }
    return list;
  }

  QStringList toQStringList(
      const INTLFORMAT* pIntlFormat = nullptr
      , const Fmt* pTextFormat = nullptr
      ) const
  {
    return toTextList(pIntlFormat, pTextFormat).toQStringList();
  }

  const QList<SC>& singles() const { return singles_; }
  const QList<PC>& pairs() const { return pairs_; }

  QVariant toQVariant() const
  {
    QList<QVariant> list;
    foreach (SC item, singles_)
    {
      list.append(item.toQVariant());
    }
    return list;
  }

protected:
  QList<SC> singles_;
  QList<PC> pairs_;
};

NTLX_CORE_END

#endif // NTLX_CORE_RANGE_H
