﻿#include "itemtable.h"
#include <ntlx_core/any.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdb.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

ItemTable::ItemTable()
  : match_()
  , pMap_(new QVariantMap())
{
}

ItemTable::ItemTable(SEARCH_MATCH* pMatch, ITEM_TABLE* pTable)
  : match_(*pMatch)
  , pMap_(new QVariantMap())
{
  ITEM* pItem = reinterpret_cast<ITEM*>(pTable + 1);
  char* pName = reinterpret_cast<char*>(pItem + pTable->Items);
  for (WORD i = 0; i < pTable->Items; ++i, ++pItem)
  {
    Text name(pName, pItem->NameLength);
    QString qName = name.toQString();

    char* pValue = pName + pItem->NameLength;
    Any value(
          *reinterpret_cast<WORD*>(pValue)
          , pValue + sizeof(WORD)
          , static_cast<int>(pItem->ValueLength - sizeof(WORD))
          );
    pMap_->insert(qName, QVariant::fromValue<Any>(value));
    pName += pItem->NameLength + pItem->ValueLength;
  }
}

ItemTable::ItemTable(const ItemTable& other)
  : match_(other.match_)
  , pMap_(new QVariantMap(*other.pMap_))
{
}

ItemTable& ItemTable::operator =(const ItemTable& other)
{
  if (this != &other)
  {
    match_ = other.match_;
    *pMap_ = *other.pMap_;
  }
  return *this;
}

ItemTable::~ItemTable()
{
  delete pMap_;
}

const SEARCH_MATCH& ItemTable::match() const
{
  return match_;
}

const QVariantMap& ItemTable::map() const
{
  return *pMap_;
}

Text ItemTable::getText(const QString& key) const
{
  QVariant v = pMap_->value(key);
  if (!v.isValid())
    return Text();

  else if (v.canConvert<Text>())
    return v.value<Text>();

  else if (v.canConvert<Any>())
  {
    Any any = v.value<Any>();
    return any.extract<TYPE_TEXT, Text>();
  }
  return Text();
}

QVariant ItemTable::operator [](const QString& key) const
{
  return pMap_->value(key);
}

NTLX_CORE_END
