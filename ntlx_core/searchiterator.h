﻿#ifndef NTLX_CORE_SEARCHITERATOR_H
#define NTLX_CORE_SEARCHITERATOR_H

#include <ntlx_core/ntlx_core_global.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

/**
 * @class SearchIteratorInterface
 * @brief ファイル/文書検索(NSFSearch)用巡回インターフェース
 */
class SearchIteratorInterface
{
public:
  virtual STATUS operator ()(SEARCH_MATCH* pMatch, ITEM_TABLE* pTable) = 0;
};

/**
 * @class SearchIterator
 * @brief ファイル/文書検索(NSFSearch)用巡回テンプレート
 * @tparam ItemClass コールバック時に生成するオブジェクトクラス
 * @tparam ListClass 生成したItemClassを格納するコンテナクラス
 */
template <class ItemClass, class ListClass>
class SearchIterator
    : public SearchIteratorInterface
{
public:
  /**
   * @brief コンストラクタ
   * @param pList コンテナオブジェクトへのポインタ
   */
  SearchIterator(ListClass* pList)
    : pList_(pList)
  {
  }

  virtual STATUS operator ()(SEARCH_MATCH* pMatch, ITEM_TABLE* pTable) override
  {
    if (pMatch->SERetFlags & SE_FMATCH)
      pList_->append(ItemClass(pMatch, pTable));
    return NOERROR;
  }

private:
  ListClass* pList_;
};

NTLX_CORE_END

#endif // NTLX_CORE_SEARCHITERATOR_H
