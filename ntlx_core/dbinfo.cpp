﻿#include "dbinfo.h"

NTLX_CORE_BEGIN

DbInfo::DbInfo(DBHANDLE hDB)
  : Resultful()
  , info_()
{
  Q_ASSERT(hDB);
  char buffer[NSF_INFO_SIZE];
  lastResult_ = NSFDbInfoGet(hDB, buffer);
  if (lastResult_.noError())
  {
    info_ = Text(buffer);
  }
}

Text DbInfo::getInfo(WORD what) const
{
  char buffer[NSF_INFO_SIZE];
  NSFDbInfoParse(
        const_cast<char*>(info_.constData())
        , what
        , buffer
        , NSF_INFO_SIZE - 1
        );
  return Text(buffer);
}

NTLX_CORE_END
