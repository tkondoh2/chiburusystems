﻿#include "cdrecord.h"
#include "text.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ods.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

CdRecord::CdRecord()
  : bytes_()
{
}

CdRecord::CdRecord(
    char* RecordPtr
    , WORD /*RecordType*/
    , DWORD /*RecordLength*/
    )
  : bytes_()
{
  switch (reinterpret_cast<BYTE*>(RecordPtr)[1])
  {
  case 0x00:
    bytes_ = QByteArray(
          RecordPtr
          , static_cast<int>(reinterpret_cast<LSIG*>(RecordPtr)->Length)
          );
    break;

  case 0xff:
    bytes_ = QByteArray(
          RecordPtr
          , static_cast<int>(reinterpret_cast<WSIG*>(RecordPtr)->Length)
          );
    break;

  default:
    bytes_ = QByteArray(
          RecordPtr
          , static_cast<int>(reinterpret_cast<BSIG*>(RecordPtr)->Length)
          );
    break;
  }
}

CdRecord::CdRecord(const CdRecord& other)
  : bytes_(other.bytes_)
{
}

CdRecord& CdRecord::operator =(const CdRecord& other)
{
  if (this != &other)
  {
    bytes_ = other.bytes_;
  }
  return *this;
}

CdRecord::~CdRecord()
{
}

const char* CdRecord::constData() const
{
  return bytes_.constData();
}

WORD CdRecord::signature() const
{
  switch (static_cast<BYTE>(bytes_.at(1)))
  {
  case 0x00: return reinterpret_cast<const LSIG*>(bytes_.constData())->Signature;
  case 0xff: return reinterpret_cast<const WSIG*>(bytes_.constData())->Signature;
  }
  return reinterpret_cast<const BSIG*>(bytes_.constData())->Signature;
}

WORD CdRecord::odsLength() const
{
  return static_cast<WORD>(bytes_.size() + bytes_.size() % 2);
}

Text CdRecord::signatureName() const
{
  Text name = CdRecord::signatureName(signature());
  if (name.isEmpty())
    return Text("(unknown)");
  return name;
}

Text CdRecord::signatureName(WORD sig)
{
  switch (sig)
  {
  case SIG_INVALID: return Text("INVALID");

  /* Signatures for Composite Records in items of data type COMPOSITE */

  case SIG_CD_PDEF_MAIN: return Text("CD_PDEF_MAIN");
  case SIG_CD_PDEF_TYPE: return Text("CD_PDEF_TYPE");
  case SIG_CD_PDEF_PROPERTY: return Text("CD_PDEF_PROPERTY");
  case SIG_CD_PDEF_ACTION: return Text("CD_PDEF_ACTION");
  case SIG_CD_TABLECELL_DATAFLAGS: return Text("CD_TABLECELL_DATAFLAGS");
  case SIG_CD_EMBEDDEDCONTACTLIST: return Text("CD_EMBEDDEDCONTACTLIST");
  case SIG_CD_IGNORE: return Text("CD_IGNORE");
  case SIG_CD_TABLECELL_HREF2: return Text("CD_TABLECELL_HREF2");
  case SIG_CD_HREFBORDER: return Text("CD_HREFBORDER");
  case SIG_CD_TABLEDATAEXTENSION: return Text("CD_TABLEDATAEXTENSION");
  case SIG_CD_EMBEDDEDCALCTL: return Text("CD_EMBEDDEDCALCTL");
  case SIG_CD_ACTIONEXT: return Text("CD_ACTIONEXT");
  case SIG_CD_EVENT_LANGUAGE_ENTRY: return Text("CD_EVENT_LANGUAGE_ENTRY");
  case SIG_CD_FILESEGMENT: return Text("CD_FILESEGMENT");
  case SIG_CD_FILEHEADER: return Text("CD_FILEHEADER");
  case SIG_CD_DATAFLAGS: return Text("CD_DATAFLAGS");

  case SIG_CD_BACKGROUNDPROPERTIES: return Text("CD_BACKGROUNDPROPERTIES");

  case SIG_CD_EMBEDEXTRA_INFO: return Text("CD_EMBEDEXTRA_INFO");
  case SIG_CD_CLIENT_BLOBPART: return Text("CD_CLIENT_BLOBPART");
  case SIG_CD_CLIENT_EVENT: return Text("CD_CLIENT_EVENT");
  case SIG_CD_BORDERINFO_HS: return Text("CD_BORDERINFO_HS");
  case SIG_CD_LARGE_PARAGRAPH: return Text("CD_LARGE_PARAGRAPH");
  case SIG_CD_EXT_EMBEDDEDSCHED: return Text("CD_EXT_EMBEDDEDSCHED");
  case SIG_CD_BOXSIZE: return Text("CD_BOXSIZE");
  case SIG_CD_POSITIONING: return Text("CD_POSITIONING");
  case SIG_CD_LAYER: return Text("CD_LAYER");
  case SIG_CD_DECSFIELD: return Text("CD_DECSFIELD");
  case SIG_CD_SPAN_END: return Text("CD_SPAN_END");
  case SIG_CD_SPAN_BEGIN: return Text("CD_SPAN_BEGIN");
  case SIG_CD_TEXTPROPERTIESTABLE: return Text("CD_TEXTPROPERTIESTABLE");

  case SIG_CD_HREF2: return Text("CD_HREF2");
  case SIG_CD_BACKGROUNDCOLOR: return Text("CD_BACKGROUNDCOLOR");
  case SIG_CD_INLINE: return Text("CD_INLINE");
  case SIG_CD_V6HOTSPOTBEGIN_CONTINUATION: return Text("CD_V6HOTSPOTBEGIN_CONTINUATION");
  case SIG_CD_TARGET_DBLCLK: return Text("CD_TARGET_DBLCLK");
  case SIG_CD_CAPTION: return Text("CD_CAPTION");
  case SIG_CD_LINKCOLORS: return Text("CD_LINKCOLORS");
  case SIG_CD_TABLECELL_HREF: return Text("CD_TABLECELL_HREF");
  case SIG_CD_ACTIONBAREXT: return Text("CD_ACTIONBAREXT");
  case SIG_CD_IDNAME: return Text("CD_IDNAME");
  case SIG_CD_TABLECELL_IDNAME: return Text("CD_TABLECELL_IDNAME");
  case SIG_CD_IMAGESEGMENT: return Text("CD_IMAGESEGMENT");
  case SIG_CD_IMAGEHEADER: return Text("CD_IMAGEHEADER");
  case SIG_CD_V5HOTSPOTBEGIN: return Text("CD_V5HOTSPOTBEGIN");
  case SIG_CD_V5HOTSPOTEND: return Text("CD_V5HOTSPOTEND");
  case SIG_CD_TEXTPROPERTY: return Text("CD_TEXTPROPERTY");
  case SIG_CD_PARAGRAPH: return Text("CD_PARAGRAPH");
  case SIG_CD_PABDEFINITION: return Text("CD_PABDEFINITION");
  case SIG_CD_PABREFERENCE: return Text("CD_PABREFERENCE");
  case SIG_CD_TEXT: return Text("CD_TEXT");
  case SIG_CD_HEADER: return Text("CD_HEADER");
  case SIG_CD_LINKEXPORT2: return Text("CD_LINKEXPORT2");
  case SIG_CD_BITMAPHEADER: return Text("CD_BITMAPHEADER");
  case SIG_CD_BITMAPSEGMENT: return Text("CD_BITMAPSEGMENT");
  case SIG_CD_COLORTABLE: return Text("CD_COLORTABLE");
  case SIG_CD_GRAPHIC: return Text("CD_GRAPHIC");
  case SIG_CD_PMMETASEG: return Text("CD_PMMETASEG");
  case SIG_CD_WINMETASEG: return Text("CD_WINMETASEG");
  case SIG_CD_MACMETASEG: return Text("CD_MACMETASEG");
  case SIG_CD_CGMMETA: return Text("CD_CGMMETA");
  case SIG_CD_PMMETAHEADER: return Text("CD_PMMETAHEADER");
  case SIG_CD_WINMETAHEADER: return Text("CD_WINMETAHEADER");
  case SIG_CD_MACMETAHEADER: return Text("CD_MACMETAHEADER");
  case SIG_CD_TABLEBEGIN: return Text("CD_TABLEBEGIN");
  case SIG_CD_TABLECELL: return Text("CD_TABLECELL");
  case SIG_CD_TABLEEND: return Text("CD_TABLEEND");
  case SIG_CD_STYLENAME: return Text("CD_STYLENAME");
  case SIG_CD_STORAGELINK: return Text("CD_STORAGELINK");
  case SIG_CD_TRANSPARENTTABLE: return Text("CD_TRANSPARENTTABLE");
  case SIG_CD_HORIZONTALRULE: return Text("CD_HORIZONTALRULE");
  case SIG_CD_ALTTEXT: return Text("CD_ALTTEXT");
  case SIG_CD_ANCHOR: return Text("CD_ANCHOR");
  case SIG_CD_HTMLBEGIN: return Text("CD_HTMLBEGIN");
  case SIG_CD_HTMLEND: return Text("CD_HTMLEND");
  case SIG_CD_HTMLFORMULA: return Text("CD_HTMLFORMULA");
  case SIG_CD_NESTEDTABLEBEGIN: return Text("CD_NESTEDTABLEBEGIN");
  case SIG_CD_NESTEDTABLECELL: return Text("CD_NESTEDTABLECELL");
  case SIG_CD_NESTEDTABLEEND: return Text("CD_NESTEDTABLEEND");
  case SIG_CD_COLOR: return Text("CD_COLOR");
  case SIG_CD_TABLECELL_COLOR: return Text("CD_TABLECELL_COLOR");

  /* 212 thru 219 reserved for BSIG'S - don't use until we hit 255 */

  case SIG_CD_BLOBPART: return Text("CD_BLOBPART");
  case SIG_CD_BEGIN: return Text("CD_BEGIN");
  case SIG_CD_END: return Text("CD_END");
  case SIG_CD_VERTICALALIGN: return Text("CD_VERTICALALIGN");
  case SIG_CD_FLOATPOSITION: return Text("CD_FLOATPOSITION");

  case SIG_CD_TIMERINFO: return Text("CD_TIMERINFO");
  case SIG_CD_TABLEROWHEIGHT: return Text("CD_TABLEROWHEIGHT");
  case SIG_CD_TABLELABEL: return Text("CD_TABLELABEL");
  case SIG_CD_BIDI_TEXT: return Text("CD_BIDI_TEXT");
  case SIG_CD_BIDI_TEXTEFFECT: return Text("CD_BIDI_TEXTEFFECT");
  case SIG_CD_REGIONBEGIN: return Text("CD_REGIONBEGIN");
  case SIG_CD_REGIONEND: return Text("CD_REGIONEND");
  case SIG_CD_TRANSITION: return Text("CD_TRANSITION");
  case SIG_CD_FIELDHINT: return Text("CD_FIELDHINT");
  case SIG_CD_PLACEHOLDER: return Text("CD_PLACEHOLDER");
  case SIG_CD_EMBEDDEDOUTLINE: return Text("CD_EMBEDDEDOUTLINE");
  case SIG_CD_EMBEDDEDVIEW: return Text("CD_EMBEDDEDVIEW");
  case SIG_CD_CELLBACKGROUNDDATA: return Text("CD_CELLBACKGROUNDDATA");

  /* Signatures for Frameset CD records */
  case SIG_CD_FRAMESETHEADER: return Text("CD_FRAMESETHEADER");
  case SIG_CD_FRAMESET: return Text("CD_FRAMESET");
  case SIG_CD_FRAME: return Text("CD_FRAME");
  /* Signature for Target Frame info on a link	*/
  case SIG_CD_TARGET: return Text("CD_TARGET");

  case SIG_CD_MAPELEMENT: return Text("CD_MAPELEMENT");
  case SIG_CD_AREAELEMENT: return Text("CD_AREAELEMENT");
  case SIG_CD_HREF: return Text("CD_HREF");
  case SIG_CD_EMBEDDEDCTL: return Text("CD_EMBEDDEDCTL");
  case SIG_CD_HTML_ALTTEXT: return Text("CD_HTML_ALTTEXT");
  case SIG_CD_EVENT: return Text("CD_EVENT");
  case SIG_CD_PRETABLEBEGIN: return Text("CD_PRETABLEBEGIN");
  case SIG_CD_BORDERINFO: return Text("CD_BORDERINFO");
  case SIG_CD_EMBEDDEDSCHEDCTL: return Text("CD_EMBEDDEDSCHEDCTL");

  case SIG_CD_EXT2_FIELD: return Text("CD_EXT2_FIELD");
  case SIG_CD_EMBEDDEDEDITCTL: return Text("CD_EMBEDDEDEDITCTL");

  /* Can not go beyond 255.  However, there may be room at the beginning of
    the list.  Check there.   */

  /* Signatures for Composite Records that are reserved internal records, */
  /* whose format may change between releases. */

  case SIG_CD_DOCUMENT_PRE_26: return Text("CD_DOCUMENT_PRE_26");
  case SIG_CD_FIELD_PRE_36: return Text("CD_FIELD_PRE_36");
  case SIG_CD_FIELD: return Text("CD_FIELD");
  case SIG_CD_DOCUMENT: return Text("CD_DOCUMENT");
  case SIG_CD_METAFILE: return Text("CD_METAFILE");
  case SIG_CD_BITMAP: return Text("CD_BITMAP");
  case SIG_CD_FONTTABLE: return Text("CD_FONTTABLE");
  case SIG_CD_LINK: return Text("CD_LINK");
  case SIG_CD_LINKEXPORT: return Text("CD_LINKEXPORT");
  case SIG_CD_KEYWORD: return Text("CD_KEYWORD");
  case SIG_CD_LINK2: return Text("CD_LINK2");
  case SIG_CD_CGM: return Text("CD_CGM");
  case SIG_CD_TIFF: return Text("CD_TIFF");
  case SIG_CD_PATTERNTABLE: return Text("CD_PATTERNTABLE");
  case SIG_CD_DDEBEGIN: return Text("CD_DDEBEGIN");
  case SIG_CD_DDEEND: return Text("CD_DDEEND");
  case SIG_CD_OLEBEGIN: return Text("CD_OLEBEGIN");
  case SIG_CD_OLEEND: return Text("CD_OLEEND");
  case SIG_CD_HOTSPOTBEGIN: return Text("CD_HOTSPOTBEGIN");
  case SIG_CD_HOTSPOTEND: return Text("CD_HOTSPOTEND");
  case SIG_CD_BUTTON: return Text("CD_BUTTON");
  case SIG_CD_BAR: return Text("CD_BAR");
  case SIG_CD_V4HOTSPOTBEGIN: return Text("CD_V4HOTSPOTBEGIN");
  case SIG_CD_V4HOTSPOTEND: return Text("CD_V4HOTSPOTEND");
  case SIG_CD_EXT_FIELD: return Text("CD_EXT_FIELD");
  case SIG_CD_LSOBJECT: return Text("CD_LSOBJECT");
  case SIG_CD_HTMLHEADER: return Text("CD_HTMLHEADER");
  case SIG_CD_HTMLSEGMENT: return Text("CD_HTMLSEGMENT");
  case SIG_CD_LAYOUT: return Text("CD_LAYOUT");
  case SIG_CD_LAYOUTTEXT: return Text("CD_LAYOUTTEXT");
  case SIG_CD_LAYOUTEND: return Text("CD_LAYOUTEND");
  case SIG_CD_LAYOUTFIELD: return Text("CD_LAYOUTFIELD");
  case SIG_CD_PABHIDE: return Text("CD_PABHIDE");
  case SIG_CD_PABFORMREF: return Text("CD_PABFORMREF");
  case SIG_CD_ACTIONBAR: return Text("CD_ACTIONBAR");
  case SIG_CD_ACTION: return Text("CD_ACTION");

  case SIG_CD_DOCAUTOLAUNCH: return Text("CD_DOCAUTOLAUNCH");
  case SIG_CD_LAYOUTGRAPHIC: return Text("CD_LAYOUTGRAPHIC");
  case SIG_CD_OLEOBJINFO: return Text("CD_OLEOBJINFO");
  case SIG_CD_LAYOUTBUTTON: return Text("CD_LAYOUTBUTTON");
  case SIG_CD_TEXTEFFECT: return Text("CD_TEXTEFFECT");

  /*	Saved Query records for items of type TYPE_QUERY */

//  case SIG_QUERY_HEADER: return Text("QUERY_HEADER");
//  case SIG_QUERY_TEXTTERM: return Text("QUERY_TEXTTERM");
  case SIG_QUERY_BYFIELD: return Text("QUERY_BYFIELD");
//  case SIG_QUERY_BYDATE: return Text("QUERY_BYDATE");
//  case SIG_QUERY_BYAUTHOR: return Text("QUERY_BYAUTHOR");
  case SIG_QUERY_FORMULA: return Text("QUERY_FORMULA");
//  case SIG_QUERY_BYFORM: return Text("QUERY_BYFORM");
//  case SIG_QUERY_BYFOLDER: return Text("QUERY_BYFOLDER");
  case SIG_QUERY_USESFORM: return Text("QUERY_USESFORM");
//  case SIG_QUERY_TOPIC: return Text("QUERY_TOPIC");
  /*	Save Action records for items of type TYPE_ACTION */

//  case SIG_ACTION_HEADER: return Text("ACTION_HEADER");
//  case SIG_ACTION_MODIFYFIELD: return Text("ACTION_MODIFYFIELD");
//  case SIG_ACTION_REPLY: return Text("ACTION_REPLY");
//  case SIG_ACTION_FORMULA: return Text("ACTION_FORMULA");
//  case SIG_ACTION_LOTUSSCRIPT: return Text("ACTION_LOTUSSCRIPT");
//  case SIG_ACTION_SENDMAIL: return Text("ACTION_SENDMAIL");
//  case SIG_ACTION_DBCOPY: return Text("ACTION_DBCOPY");
  case SIG_ACTION_DELETE: return Text("ACTION_DELETE");
//  case SIG_ACTION_BYFORM: return Text("ACTION_BYFORM");
  case SIG_ACTION_MARKREAD: return Text("ACTION_MARKREAD");
  case SIG_ACTION_MARKUNREAD: return Text("ACTION_MARKUNREAD");
  case SIG_ACTION_MOVETOFOLDER: return Text("ACTION_MOVETOFOLDER");
  case SIG_ACTION_COPYTOFOLDER: return Text("ACTION_COPYTOFOLDER");
//  case SIG_ACTION_REMOVEFROMFOLDER: return Text("ACTION_REMOVEFROMFOLDER");
//  case SIG_ACTION_NEWSLETTER: return Text("ACTION_NEWSLETTER");
  case SIG_ACTION_RUNAGENT: return Text("ACTION_RUNAGENT");
  case SIG_ACTION_SENDDOCUMENT: return Text("ACTION_SENDDOCUMENT");
//  case SIG_ACTION_FORMULAONLY: return Text("ACTION_FORMULAONLY");
//  case SIG_ACTION_JAVAAGENT: return Text("ACTION_JAVAAGENT");
  case SIG_ACTION_JAVA: return Text("ACTION_JAVA");


  /* Signatures for items of type TYPE_VIEWMAP_DATASET */

  case SIG_VIEWMAP_DATASET: return Text("VIEWMAP_DATASET");

  /* Signatures for items of type TYPE_VIEWMAP */

  case SIG_CD_VMHEADER: return Text("CD_VMHEADER");
  case SIG_CD_VMBITMAP: return Text("CD_VMBITMAP");
  case SIG_CD_VMRECT: return Text("CD_VMRECT");
  case SIG_CD_VMPOLYGON_BYTE: return Text("CD_VMPOLYGON_BYTE");
  case SIG_CD_VMPOLYLINE_BYTE: return Text("CD_VMPOLYLINE_BYTE");
  case SIG_CD_VMREGION: return Text("CD_VMREGION");
  case SIG_CD_VMACTION: return Text("CD_VMACTION");
  case SIG_CD_VMELLIPSE: return Text("CD_VMELLIPSE");
//  case SIG_CD_VMRNDRECT: return Text("CD_VMRNDRECT");
//  case SIG_CD_VMBUTTON: return Text("CD_VMBUTTON");
  case SIG_CD_VMACTION_2: return Text("CD_VMACTION_2");
//  case SIG_CD_VMTEXTBOX: return Text("CD_VMTEXTBOX");
  case SIG_CD_VMPOLYGON: return Text("CD_VMPOLYGON");
  case SIG_CD_VMPOLYLINE: return Text("CD_VMPOLYLINE");
//  case SIG_CD_VMPOLYRGN: return Text("CD_VMPOLYRGN");
  case SIG_CD_VMCIRCLE: return Text("CD_VMCIRCLE");
//  case SIG_CD_VMPOLYRGN_BYTE: return Text("CD_VMPOLYRGN_BYTE");

  /* Signatures for alternate CD sequences*/
  case SIG_CD_ALTERNATEBEGIN: return Text("CD_ALTERNATEBEGIN");
  case SIG_CD_ALTERNATEEND: return Text("CD_ALTERNATEEND");

  case SIG_CD_OLERTMARKER: return Text("CD_OLERTMARKER");
  }
  return Text();
}

NTLX_CORE_END
