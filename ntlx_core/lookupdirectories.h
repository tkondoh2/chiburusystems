﻿#ifndef NTLX_CORE_LOOKUPDIRECTORIES_H
#define NTLX_CORE_LOOKUPDIRECTORIES_H

#include <ntlx_core/resultful.h>
#include <QVariant>

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT LookupUser
{
public:
  LookupUser();
  LookupUser(const QString& name);
  const QString& name() const;
  const QVariantMap& items() const;
  void addItem(const QString& itemName, const QVariant& itemValue);

  static QMap<QString,QVariant> toMap(const QList<LookupUser>& users);
  static QJsonDocument toJson(const QList<LookupUser>& users);

private:
  QString name_;
  QVariantMap items_;
};

class NTLX_CORESHARED_EXPORT LookupDirectories
    : public Resultful
{
public:
  LookupDirectories(
      const QString& nameSpace = "($Users)"
      , int bufferSize = 1024
      );

  QList<LookupUser> operator ()(
      const QStringList& nameList
      , const QStringList& itemNameList
      ) const;

  int bufferSize() const;
  void setBufferSize(int size);

private:
  class TextArray
  {
  public:
    TextArray(const QString& item);
    TextArray(const QStringList& list);
    const char* operator *();
  private:
    QStringList list_;
    QByteArray data_;
  };

  QString nameSpace_;
  int bufferSize_;
};

inline LookupUser::LookupUser()
  : name_()
  , items_()
{
}

inline LookupUser::LookupUser(const QString& name)
  : name_(name)
  , items_()
{
}

inline const QString& LookupUser::name() const
{
  return name_;
}

inline const QVariantMap& LookupUser::items() const
{
  return items_;
}

inline void LookupUser::addItem(
    const QString& itemName
    , const QVariant& itemValue)
{
  items_.insert(itemName, itemValue);
}

inline int LookupDirectories::bufferSize() const
{
  return bufferSize_;
}

inline void LookupDirectories::setBufferSize(int size)
{
  if (size >= 16 && size < MAXINT - 1)
    bufferSize_ = size;
}

inline LookupDirectories::TextArray::TextArray(const QString& item)
  : list_(item)
  , data_()
{
}

inline LookupDirectories::TextArray::TextArray(const QStringList &list)
  : list_(list)
  , data_()
{
}

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::LookupUser)

#endif // NTLX_CORE_LOOKUPDIRECTORIES_H
