﻿#ifndef NTLX_CORE_DISTINGUISHEDNAME_H
#define NTLX_CORE_DISTINGUISHEDNAME_H

#include <ntlx_core/resultful.h>
#include <ntlx_core/text.h>

NTLX_CORE_BEGIN

/**
 * @brief 識別名クラス
 */
class NTLX_CORESHARED_EXPORT DistinguishedName
    : public Resultful
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  DistinguishedName();

  /**
   * @brief コンストラクタ
   * @param name 識別名となるLMBCSテキスト
   */
  DistinguishedName(const Text& name);

  /**
   * @brief 基準書式に変換する
   * @return 基準書式化後のLMBCSテキスト
   */
  Text canonical() const;

  /**
   * @brief 省略書式に変換する
   * @return 省略書式化後のLMBCSテキスト
   */
  Text abbreviated() const;

  /**
   * @brief 識別名をQStringに変換する
   * @return QStringオブジェクト
   */
  QString toQString() const;

  /**
   * @brief 基準書式に変換する
   * @param before 変換前のLMBCSテキスト
   * @param after 変換後のLMBCSテキスト
   * @return APIの処理結果
   */
  static ApiResult canonicalize(const Text& before, Text& after);

  /**
   * @brief 省略書式に変換する
   * @param before 変換前のLMBCSテキスト
   * @param after 変換後のLMBCSテキスト
   * @return APIの処理結果
   */
  static ApiResult abbreviate(const Text& before, Text& after);

  bool isEmpty() const;

  static DistinguishedName currentUserName(ApiResult* result = nullptr);

  Text orgName() const;

private:
  Text value_;
};

inline bool DistinguishedName::isEmpty() const
{
  return value_.isEmpty();
}

typedef DistinguishedName DName;

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::DistinguishedName)

#endif // NTLX_CORE_DISTINGUISHEDNAME_H
