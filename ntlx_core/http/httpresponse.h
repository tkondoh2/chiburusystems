﻿#ifndef NTLX_CORE_HTTP_HTTPRESPONSE_H
#define NTLX_CORE_HTTP_HTTPRESPONSE_H

#include <ntlx_core/http/httpcontext.h>

#include <QString>
#include <QVariant>

NTLX_CORE_BEGIN
namespace http {

class NTLX_CORESHARED_EXPORT HttpResponse
    : public HttpContextBase
{
public:
  HttpResponse(
        FilterContext* pCtx
        , FilterResponse* pEventData
        );

  QString header(const QString& key) const;

  void setHeader(const QString& key, const QString& value);

  QString userName() const;
  uint code() const { return pEvent_->responseCode; }
  const char* reasonText() const { return pEvent_->reasonText; }

private:
  FilterResponse* pEvent_;
};

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPRESPONSE_H
