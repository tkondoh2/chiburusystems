﻿#ifndef NTLX_CORE_HTTP_HTTPFULLRESPONSE_H
#define NTLX_CORE_HTTP_HTTPFULLRESPONSE_H

#include <ntlx_core/http/httpcontext.h>

#include <QString>
#include <QVariant>

NTLX_CORE_BEGIN
namespace http {

class HttpRequest;

class NTLX_CORESHARED_EXPORT HttpFullResponse
    : public HttpContextBase
{
public:
  HttpFullResponse(const HttpRequest& req);
  void setHeader(const QString& key, const QVariant& value);
  void setContentType(const QString& type);
  void setHtml(const QString& content);
  void setJson(const QJsonDocument& content);
  int end(uint retCode, const QString& status);
  void redirect(const QString& location);

private:
  QString httpVer_;
  QVariantMap headers_;
  QString body_;
};

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPFULLRESPONSE_H
