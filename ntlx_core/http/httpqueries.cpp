﻿#include "httpqueries.h"

NTLX_CORE_BEGIN
namespace http {

HttpQueries::HttpQueries(const QString& query)
  : urlQuery_(query)
{
}

QString HttpQueries::value(
      const QString &key
      , QUrl::ComponentFormattingOption encoding
      ) const
{
  if (urlQuery_.hasQueryItem(key))
  {
    return urlQuery_.queryItemValue(key, encoding);
  }
  return QString();
}

QStringList HttpQueries::values(
      const QString &key
      , QUrl::ComponentFormattingOption encoding
      ) const
{
  if (urlQuery_.hasQueryItem(key))
  {
    return urlQuery_.allQueryItemValues(key, encoding);
  }
  return QStringList();
}

QStringList HttpQueries::keys(
      QUrl::ComponentFormattingOption encoding
      ) const
{
  QList<QPair<QString,QString>> items = urlQuery_.queryItems(encoding);
  QStringList keyList;
  QPair<QString,QString> item;
  foreach (item, items)
  {
    keyList.append(item.first);
  }
  return keyList;
}

bool HttpQueries::contains(const QString &key) const
{
  QStringList list = keys();
  return list.contains(key);
}

bool HttpQueries::contains(const QStringList &keyList) const
{
  QStringList list = keys();
  foreach (QString key, keyList)
  {
    if (list.contains(key))
      return true;
  }
  return false;
}

} // namespace http
NTLX_CORE_END
