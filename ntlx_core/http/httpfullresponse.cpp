﻿#include "httpfullresponse.h"
#include "httprequest.h"

#include <QTextStream>
#include <QJsonDocument>

NTLX_CORE_BEGIN
namespace http {

HttpFullResponse::HttpFullResponse(const HttpRequest& req)
  : HttpContextBase(req.pCtx())
  , httpVer_(req.version())
{
}

void HttpFullResponse::setHeader(const QString& key, const QVariant& value)
{
  headers_.insert(key, value);
}

void HttpFullResponse::setContentType(const QString& type)
{
  setHeader("Content-Type", type);
}

void HttpFullResponse::setHtml(const QString& content)
{
  setContentType("text/html");
  body_ = content;
}

void HttpFullResponse::setJson(const QJsonDocument& content)
{
  setContentType("application/json");
  body_ = QString(content.toJson(QJsonDocument::Compact));
}

int HttpFullResponse::end(uint retCode, const QString& status)
{
  QString buffer;
  QTextStream str(&buffer, QIODevice::WriteOnly);
  str << QString("%1 %2 %3").arg(httpVer_).arg(retCode).arg(status) << endl;
  foreach (QString key, headers_.keys())
  {
    QVariant value = headers_.value(key);
    str << key << ": " << value.toString() << endl;
  }
  if (!body_.isEmpty())
  {
    str << endl << body_;
  }
  str.flush();
  QByteArray retValue = buffer.toUtf8();
  return pCtx()->WriteClient(
        pCtx()
        , retValue.data()
        , retValue.size()
        , 0
        , &errID_
        );
}

void HttpFullResponse::redirect(const QString &location)
{
  setHeader("Location", location);
  end(302, "Found");
}

} // namespace http
NTLX_CORE_END
