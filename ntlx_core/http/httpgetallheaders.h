﻿#ifndef NTLX_CORE_HTTP_HTTPGETALLHEADERS_H
#define NTLX_CORE_HTTP_HTTPGETALLHEADERS_H

#include <ntlx_core/http/httpcontext.h>
#include <QString>
#include <QVariantMap>

NTLX_CORE_BEGIN
namespace http {

template <class EventData>
class HttpGetAllHeaders
    : public HttpContextBase
{
public:
  HttpGetAllHeaders(FilterContext* pContext, EventData* pEventData)
    : HttpContextBase(pContext)
    , pEventData_(pEventData)
  {
  }

  QVariantMap operator ()() const
  {
    char* buffer;
    int retSize = pEventData_->GetAllHeaders(
          pCtx()
          , &buffer
          , &errID_
          );
    QVariantMap map;
    if (noError() && retSize > 0)
    {
      QString contents = QString::fromLocal8Bit(buffer, retSize);
      int pos = contents.indexOf("\r\n\r\n");
      QString headers = contents.left(pos);
      QStringList headerList = headers.split("\r\n");
      headerList.removeFirst();
      foreach (QString headerLine, headerList)
      {
        pos = headerLine.indexOf(":");
        QString key = headerLine.left(pos).trimmed();
        if (!key.isEmpty())
        {
          QString value = headerLine.right(headerLine.length() - pos - 1)
              .trimmed();
          if (map.contains(key))
          {
            QVariant exValue = map.take(key);
            QStringList list;
            if (exValue.type() == QMetaType::QStringList)
            {
              list = exValue.toStringList();
              list.append(value);
            }
            else
            {
              list.append(exValue.toString());
              list.append(value);
            }
            map.insert(key, list);
          }
          else
          {
            map.insert(key, value);
          }
        }
      }
    }
    return map;
  }

private:
  EventData* pEventData_;
};

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPGETALLHEADERS_H
