﻿#include "httpcontext.h"
#include "distinguishedname.h"

#include <QVariant>
#include <QJsonDocument>

NTLX_CORE_BEGIN
namespace http {

bool HttpContextBase::savePrivateContext(const QVariant& ctx)
{
  QByteArray buffer
      = QJsonDocument::fromVariant(ctx).toJson(QJsonDocument::Compact);
  void* ptr = pCtx()->AllocMem(pCtx(), buffer.size() + 1, 0, &errID_);
  if (noError())
  {
    memcpy(ptr, buffer.constData(), buffer.size() + 1);
    pCtx()->privateContext = ptr;
    return true;
  }
  return false;
}

QVariant HttpContextBase::restorePrivateContext() const
{
  void* ptr = pCtx()->privateContext;
  if (ptr == nullptr)
    return QVariant();

  QByteArray buffer((char*)ptr);
  return QJsonDocument::fromJson(buffer).toVariant();
}

DistinguishedName HttpContextBase::authCanonicalUser() const
{
  static const FilterAuthenticatedUserFields types
      = (FilterAuthenticatedUserFields)kCannonicalUserName;
  return DistinguishedName(
        ntlx::Text::fromQString(
          authUser(types).value(types)
          )
        );
}

QMap<FilterAuthenticatedUserFields,QString>
    HttpContextBase::authUser(FilterAuthenticatedUserFields fields) const
{
  FilterAuthenticatedUser authUser;
  authUser.fieldFlags = fields;
  QMap<FilterAuthenticatedUserFields,QString> map;
  if (pCtx()->ServerSupport(
        pCtx()
        , kGetAuthenticatedUserInfo
        , &authUser
        , nullptr
        , 0
        , &errID_
        ) && noError())
  {
    if (fields & kCannonicalUserName)
      map[kCannonicalUserName] = QString(authUser.pUserCannonicalName);
    if (fields & kWebUserName)
      map[kWebUserName] = QString(authUser.pWebUserName);
    if (fields & kUserGroupList)
      map[kUserGroupList] = QString(authUser.pUserGroupList);
    if (fields & kUserPassword)
      map[kUserPassword] = QString(authUser.pUserPassword);
  }
  return map;
}

} // namespace http
NTLX_CORE_END
