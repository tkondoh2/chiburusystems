﻿#ifndef NTLX_CORE_HTTP_HTTPGETHEADER_H
#define NTLX_CORE_HTTP_HTTPGETHEADER_H

#include <ntlx_core/http/httpcontext.h>

#include <QRegExp>

NTLX_CORE_BEGIN
namespace http {

template <class EventData>
class HttpGetHeader
    : public HttpContextBase
{
public:
  HttpGetHeader(FilterContext* pContext, EventData* pEventData)
    : HttpContextBase(pContext)
    , pEventData_(pEventData)
  {
  }

  QString operator ()(const QString& key, int size = 4095) const
  {
    char* buffer = new char[size + 1];
    int retSize = pEventData_->GetHeader(
          pCtx()
          , key.toLocal8Bit().data()
          , buffer
          , size
          , &errID_
          );
    if (noError() && retSize > 0)
    {
      QByteArray value(buffer, retSize < size ? retSize : size);
      delete[] buffer;
      return QString(value);
    }
    delete[] buffer;
    return QString();
  }

  QString getAuthorizationCode(const QString& type, int size = 4095) const
  {
    QString value = (*this)("Authorization", size);
    QRegExp rx(QString("^%1 (.+)$").arg(type));
    if (rx.indexIn(value) == 0)
      return rx.cap(1);
    return QString();
  }

private:
  EventData* pEventData_;
};

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPGETHEADER_H
