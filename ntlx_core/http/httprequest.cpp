﻿#include "httprequest.h"
//#include <ntlx_core/svt/log.h>

#include <QUrlQuery>

NTLX_CORE_BEGIN
namespace http {

HttpRequest::HttpRequest(FilterContext *pContext)
  : HttpContextBase(pContext)
  , request_()
  , url_()
{
  if (pCtx()->GetRequest(pCtx(), &request_, &errID_) && noError())
    url_ = QUrl(request_.URL);
}

QString HttpRequest::method() const
{
  switch (request_.method)
  {
  case kRequestNone: return QString("None");
  case kRequestHEAD: return QString("HEAD");
  case kRequestGET: return QString("GET");
  case kRequestPOST: return QString("POST");
  case kRequestPUT: return QString("PUT");
  case kRequestDELETE: return QString("DELETE");
  case kRequestTRACE: return QString("TRACE");
  case kRequestCONNECT: return QString("CONNECT");
  case kRequestOPTIONS: return QString("OPTIONS");
  case kRequestUNKNOWN: return QString("UNKNOWN");
  case kRequestBAD: return QString("BAD");
  }
  return QString();
}

QUrl HttpRequest::url(QUrl::ParsingMode mode) const
{
  return QUrl(request_.URL, mode);
}

bool HttpRequest::matchUrlPath(
      const QString& rxStr
      , Qt::CaseSensitivity cs
      ) const
{
  return QRegExp(rxStr, cs).exactMatch(urlPath());
}

QByteArray HttpRequest::rawBody() const
{
  char* pBody;
  int size = pCtx()->GetRequestContents(pCtx(), &pBody, &errID_);

  if (noError() && size > 0)
    return QByteArray(pBody, size);
  return QByteArray();
}

QString HttpRequest::body() const
{
  return QString::fromUtf8(rawBody());
}

} // namespace http
NTLX_CORE_END
