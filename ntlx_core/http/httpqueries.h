﻿#ifndef NTLX_CORE_HTTP_HTTPQUERIES_H
#define NTLX_CORE_HTTP_HTTPQUERIES_H

#include <ntlx_core/ntlx_core_global.h>

#include <QUrlQuery>

NTLX_CORE_BEGIN
namespace http {

class NTLX_CORESHARED_EXPORT HttpQueries
{
public:
  HttpQueries(const QString& query = QString());

  QString value(
        const QString& key
        , QUrl::ComponentFormattingOption encoding
        = QUrl::PrettyDecoded
        ) const;

  QStringList values(
        const QString& key
        , QUrl::ComponentFormattingOption encoding
        = QUrl::PrettyDecoded
        ) const;

  QStringList keys(
        QUrl::ComponentFormattingOption encoding
        = QUrl::PrettyDecoded
      ) const;

  bool contains(const QString& key) const;

  bool contains(const QStringList& key) const;

private:
  QUrlQuery urlQuery_;
};

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPQUERIES_H
