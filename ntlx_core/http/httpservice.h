﻿#ifndef NTLX_CORE_HTTP_HTTPSERVICE_H
#define NTLX_CORE_HTTP_HTTPSERVICE_H

#include <ntlx_core/http/httpevent.h>
#include <QList>

NTLX_CORE_BEGIN
class Text;
namespace http {

class NTLX_CORESHARED_EXPORT HttpService
{
public:
  /**
   * @brief コンストラクタ イベントを登録しておきます。
   */
  HttpService();

  virtual ~HttpService();

  /**
   * @brief フィルターイベントFilterInitから呼び出して初期化します。
   * @param pInitData 初期化データへのポインタ
   * @return 処理結果
   */
  virtual uint init(FilterInitData* pInitData);

  /**
   * @brief フィルターイベントHttpFilterProcから呼び出してイベントを処理します。
   * @param pContext コンテキストへのポインタ
   * @param eventType イベントタイプ
   * @param ptr 各イベント用データ構造体へのポインタ
   * @return 処理結果
   */
  virtual uint doEvent(FilterContext* pContext, uint eventType, void* ptr);

  /**
   * @brief イベントを追加します。
   * @param pEvent イベントオブジェクトへのポインタ
   */
  void addEvent(HttpEvent* pEvent);

  virtual uint eventFlags() const;
  virtual ntlx::Text description() const = 0;

private:
  QList<HttpEvent*> events_;
};

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPSERVICE_H
