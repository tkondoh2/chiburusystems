﻿#ifndef NTLX_CORE_HTTP_HTTPAUTHENTICATE_H
#define NTLX_CORE_HTTP_HTTPAUTHENTICATE_H

#include <ntlx_core/http/httpgetheader.h>

NTLX_CORE_BEGIN
namespace http {

class NTLX_CORESHARED_EXPORT HttpAuthenticate
      : public HttpGetHeader<FilterAuthenticate>
{
public:
  HttpAuthenticate(
        FilterContext* pContext
        , FilterAuthenticate* pEventData
        );

  void setAuthName(const QString& name);
  void setAuthType(uint type);

private:
  FilterAuthenticate* pData_;
};

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPAUTHENTICATE_H
