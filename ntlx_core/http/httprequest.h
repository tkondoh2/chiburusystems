﻿#ifndef NTLX_CORE_HTTP_HTTPREQUEST_H
#define NTLX_CORE_HTTP_HTTPREQUEST_H

#include <ntlx_core/http/httpcontext.h>
#include <ntlx_core/http/httpqueries.h>

#include <QUrl>
#include <QMap>
#include <QVariantMap>

NTLX_CORE_BEGIN
namespace http {

class NTLX_CORESHARED_EXPORT HttpRequest
    : public HttpContextBase
{
public:
  HttpRequest(FilterContext* pContext);
  QString method() const;
  QUrl url(QUrl::ParsingMode mode = QUrl::TolerantMode) const;
  QString version() const;
  bool isSecurePort() const;
  QString urlPath(
        QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
      ) const;
  HttpQueries urlQueries(
        QUrl::ComponentFormattingOption options = QUrl::FullyDecoded
      ) const;
  bool isGet() const;
  bool isPost() const;
  bool methodIs(uint method) const;
  bool matchUrlPath(
        const QString& rxStr
        , Qt::CaseSensitivity cs = Qt::CaseInsensitive
      ) const;
  QByteArray rawBody() const;
  QString body() const;
  HttpQueries bodyParams() const;

private:
  FilterRequest request_;
  QUrl url_;
};

inline QString HttpRequest::version() const
{
  return QString::fromLocal8Bit(request_.version);
}

inline bool HttpRequest::isSecurePort() const
{
  return (pCtx()->securePort == 1);
}

inline QString HttpRequest::urlPath(
      QUrl::ComponentFormattingOption options
      ) const
{
  return url_.path(options);
}

inline HttpQueries HttpRequest::urlQueries(
      QUrl::ComponentFormattingOption options
    ) const
{
  return HttpQueries(url_.query(options));
}

inline bool HttpRequest::isGet() const
{
  return methodIs(kRequestGET);
}

inline bool HttpRequest::isPost() const
{
  return methodIs(kRequestPOST);
}

inline bool HttpRequest::methodIs(uint method) const
{
  return (request_.method == method);
}

inline HttpQueries HttpRequest::bodyParams() const
{
  return HttpQueries(body());
}

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPREQUEST_H
