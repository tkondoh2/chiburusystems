﻿#include "httpauthenticate.h"
#include <ntlx_core/text.h>

NTLX_CORE_BEGIN
namespace http {

HttpAuthenticate::HttpAuthenticate(
      FilterContext* pContext
      , FilterAuthenticate* pEventData
      )
  : HttpGetHeader<FilterAuthenticate>(pContext, pEventData)
  , pData_(pEventData)
{
}

void HttpAuthenticate::setAuthName(const QString& name)
{
  ntlx::Text lmbcs = ntlx::Text::fromQString(name);
  qstrncpy((char*)pData_->authName, lmbcs.constData(), pData_->authNameSize);
}

void HttpAuthenticate::setAuthType(uint type)
{
  pData_->authType = type;
}

} // namespace http
NTLX_CORE_END
