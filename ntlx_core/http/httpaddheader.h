﻿#ifndef NTLX_CORE_HTTP_HTTPADDHEADER_H
#define NTLX_CORE_HTTP_HTTPADDHEADER_H

#include <ntlx_core/http/httpcontext.h>
#include <QString>

NTLX_CORE_BEGIN
namespace http {

template <class EventData>
class HttpAddHeader
    : public HttpContextBase
{
public:
  HttpAddHeader(FilterContext* pContext, EventData* pEventData)
    : HttpContextBase(pContext)
    , pEventData_(pEventData)
  {
  }

  bool operator ()(const QString& key, const QString& value) const
  {
    QString data = QString("%1: %2").arg(key).arg(value);
    return pEventData_->AddHeader(pCtx(), data.toLocal8Bit().data(), &errID_);
  }

private:
  EventData* pEventData_;
};

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPADDHEADER_H
