﻿#include "httpservice.h"

#include <ntlx_core/text.h>

NTLX_CORE_BEGIN
namespace http {

HttpService::HttpService()
  : events_()
{
}

HttpService::~HttpService()
{
  qDeleteAll(events_);
}

uint HttpService::init(FilterInitData* pInitData)
{
  pInitData->appFilterVersion = kInterfaceVersion;
  pInitData->eventFlags = eventFlags();
  qstrcpy(pInitData->filterDesc, description().constData());

  return kFilterHandledEvent;
}

uint HttpService::doEvent(FilterContext* pContext, uint eventType, void* ptr)
{
  uint result = kFilterNotHandled;

  foreach (HttpEvent* pEvent, events_)
  {
    if (eventType & pEvent->eventType())
    {
      pEvent->setContext(pContext);
      pEvent->setEventData(ptr);
      if ((*pEvent)(result))
        return result;
    }
  }
  return result;
}

void HttpService::addEvent(HttpEvent* pEvent)
{
  events_.append(pEvent);
}

uint HttpService::eventFlags() const
{
  uint retFlags = 0;
  foreach (HttpEvent* pEvent, events_)
  {
    retFlags |= pEvent->eventType();
  }
  return retFlags;
}

} // namespace http
NTLX_CORE_END
