﻿#include "httpresponse.h"

#include <QTextStream>
#include <QJsonDocument>

NTLX_CORE_BEGIN
namespace http {

HttpResponse::HttpResponse(
      FilterContext* pCtx
      , FilterResponse* pEventData
      )
  : HttpContextBase(pCtx)
  , pEvent_(pEventData)
{
}

QString HttpResponse::header(const QString &key) const
{
  static const uint BufferSize = 1024;
  char buffer[BufferSize] = "";
  int size = pEvent_->GetHeader(
        pCtx()
        , key.toLocal8Bit().data()
        , buffer
        , BufferSize
        , &errID_
        );
  if (errID_ = 0 && size > 0)
  {
    return QString::fromLocal8Bit(buffer, size);
  }
  return QString();
}

void HttpResponse::setHeader(const QString& key, const QString& value)
{
  pEvent_->SetHeader(
        pCtx()
        , key.toLocal8Bit().data()
        , value.toLocal8Bit().data()
        , &errID_
        );
}

QString HttpResponse::userName() const
{
  return QString(pEvent_->userName);
}

} // namespace http
NTLX_CORE_END
