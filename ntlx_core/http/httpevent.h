﻿#ifndef NTLX_CORE_HTTP_HTTPEVENT_H
#define NTLX_CORE_HTTP_HTTPEVENT_H

#include <ntlx_core/http/httpcontext.h>

NTLX_CORE_BEGIN
namespace http {

class NTLX_CORESHARED_EXPORT HttpEvent
    : public ntlx::http::HttpContextBase
{
public:
  HttpEvent();
  virtual uint eventType() const = 0;
  virtual void setContext(FilterContext* pContext);
  virtual void setEventData(void* ptr);
  virtual bool operator ()(uint& result) = 0;
};

template <uint EventType>
class HttpNoDataEventBase
    : public HttpEvent
{
public:
  virtual uint eventType() const override
  {
    return EventType;
  }
};

template <class T, uint EventType>
class HttpEventBase
    : public HttpNoDataEventBase<EventType>
{
public:
  virtual void setEventData(void* ptr) override
  {
    pData_ = reinterpret_cast<T*>(ptr);
  }

protected:
  T* pData_;
};

using HttpStartRequestEvent = HttpNoDataEventBase<kFilterStartRequest>;
using HttpEndRequestEvent = HttpNoDataEventBase<kFilterEndRequest>;

using HttpRawRequestEvent = HttpEventBase<FilterRawRequest, kFilterRawRequest>;
using HttpParsedRequestEvent = HttpEventBase<FilterParsedRequest, kFilterParsedRequest>;
using HttpRewriteURLEvent = HttpEventBase<FilterMapURL, kFilterRewriteURL>;
using HttpUserNameListEvent = HttpEventBase<FilterUserNameList, kFilterUserNameList>;
using HttpTranslateRequestEvent = HttpEventBase<FilterMapURL, kFilterTranslateRequest>;
using HttpPostTranslateEvent = HttpEventBase<FilterMapURL, kFilterPostTranslate>;
using HttpProcessRequestEvent = HttpEventBase<FilterMapURL, kFilterProcessRequest>;

using HttpAuthenticateEvent = HttpEventBase<FilterAuthenticate, kFilterAuthenticate>;
using HttpAuthorizedEvent = HttpEventBase<FilterAuthorize, kFilterAuthorized>;
using HttpAuthUserEvent = HttpEventBase<FilterAuthenticate, kFilterAuthUser>;

using HttpResponseEvent = HttpEventBase<FilterResponse, kFilterResponse>;
using HttpRawWriteEvent = HttpEventBase<FilterRawWrite, kFilterRawWrite>;


inline HttpEvent::HttpEvent()
  : ntlx::http::HttpContextBase(nullptr)
{
}

inline void HttpEvent::setContext(FilterContext *pContext)
{
  pContext_ = pContext;
}

inline void HttpEvent::setEventData(void*)
{
}

} // namespace http
NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPEVENT_H
