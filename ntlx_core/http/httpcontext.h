﻿#ifndef NTLX_CORE_HTTP_HTTPCONTEXT_H
#define NTLX_CORE_HTTP_HTTPCONTEXT_H

#include <ntlx_core/ntlx_core_global.h>
#include <dsapi.h>

class QVariant;

NTLX_CORE_BEGIN

class DistinguishedName;

namespace http {

class NTLX_CORESHARED_EXPORT HttpContextBase
{
public:
  explicit HttpContextBase(FilterContext* pCtx);

  FilterContext* pCtx() const;
  bool hasError() const;
  bool noError() const;
  uint takeError() const;

  bool savePrivateContext(const QVariant& ctx);

  QVariant restorePrivateContext() const;

  QMap<FilterAuthenticatedUserFields,QString>
    authUser(FilterAuthenticatedUserFields fields) const;

  DistinguishedName authCanonicalUser() const;

protected:
  mutable FilterContext* pContext_;
  mutable uint errID_;
};


inline HttpContextBase::HttpContextBase(FilterContext* pCtx)
  : pContext_(pCtx)
  , errID_(0)
{
}

inline FilterContext* HttpContextBase::pCtx() const
{
  return pContext_;
}

inline bool HttpContextBase::hasError() const
{
  return (errID_ != 0);
}

inline bool HttpContextBase::noError() const
{
  return !hasError();
}

inline uint HttpContextBase::takeError() const
{
  uint err = errID_;
  errID_ = 0;
  return err;
}


} // namespace http

NTLX_CORE_END

#endif // NTLX_CORE_HTTP_HTTPCONTEXT_H
