﻿#ifndef NTLX_CORE_ITEMINFOITERATOR_H
#define NTLX_CORE_ITEMINFOITERATOR_H

#include <ntlx_core/ntlx_core_global.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <pool.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class ItemInfoIteratorInterface
{
public:
  virtual void operator ()(
      BLOCKID& bidItem
      , WORD dataType
      , BLOCKID& bidValue
      , DWORD valueLength
      ) = 0;
};

template <class ItemClass, class ListClass>
class ItemInfoIterator
    : public ItemInfoIteratorInterface
{
public:
  ItemInfoIterator(ListClass* pList)
    : pList_(pList)
  {
  }

  virtual void operator ()(
      BLOCKID& bidItem
      , WORD dataType
      , BLOCKID& bidValue
      , DWORD valueLength
      ) override
  {
    pList_->append(ItemClass(bidItem, dataType, bidValue, valueLength));
  }

private:
  ListClass* pList_;
};

NTLX_CORE_END

#endif // NTLX_CORE_ITEMINFOITERATOR_H
