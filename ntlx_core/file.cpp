﻿#include <ntlx_core/file.h>
#include <ntlx_core/searchiterator.h>
#include <ntlx_core/formula.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdb.h>

#if defined(NT)
#pragma pack(pop)
#endif

#include <QtDebug>

NTLX_CORE_BEGIN

File::File()
  : Resultful()
  , path_()
  , handle_(NULLHANDLE)
{
}

File::File(const FilePath& path, bool withOpen)
  : Resultful()
  , path_(path)
  , handle_(NULLHANDLE)
{
  if (withOpen)
    open();
}

File::~File()
{
  close();
}

File& File::open(
    WORD options
    , DHANDLE hNames
    , TIMEDATE* pModifiedTime
    , TIMEDATE* pDataModified
    , TIMEDATE* pNonDataModified
    )
{
  if (handle_ == NULLHANDLE)
    lastResult_ = NSFDbOpenExtended(
          path_.netPath().constData()
          , options
          , hNames
          , pModifiedTime
          , &handle_
          , pDataModified
          , pNonDataModified
          );
  return *this;
}

File& File::close()
{
  if (handle_ != NULLHANDLE)
  {
    lastResult_ = NSFDbClose(handle_);
    handle_ = NULLHANDLE;
  }
  return *this;
}

const File& File::search(
    SearchIteratorInterface* iterator
    , const Text& formulaText
    , const Text& viewTitle
    , WORD searchFlags
    , WORD noteClassMask
    , TIMEDATE* pSince
    , TIMEDATE* pUntil
    ) const
{
  Formula f;
  if (!formulaText.isEmpty())
    if (f.compile(formulaText).lastResult().hasError())
    {
      qDebug() << f.lastResult().toQString()
               << formulaText.toQString();
      return *this;
    }

  char* pViewTitle = viewTitle.isEmpty()
      ? nullptr
      : const_cast<char*>(viewTitle.constData())
        ;
  lastResult_ = NSFSearch(
        handle_
        , f
        , pViewTitle
        , searchFlags
        , noteClassMask
        , pSince
        , searchCallback
        , iterator
        , pUntil
        );
  return *this;
}

STATUS LNPUBLIC File::searchCallback(
    void* ptr
    , SEARCH_MATCH* pMatch
    , ITEM_TABLE* pTable
    )
{
  SearchIteratorInterface* iterator = static_cast<SearchIteratorInterface*>(ptr);
  SEARCH_MATCH searchMatch;
  memcpy(&searchMatch, pMatch, sizeof(SEARCH_MATCH));
  return (*iterator)(&searchMatch, pTable);
}

ApiResult File::getPath(DHANDLE hDB, FilePath* filePath, FilePath* fullPath)
{
  char canonical[MAXPATH], expanded[MAXPATH];
  ApiResult result = NSFDbPathGet(hDB, canonical, expanded);
  if (result.noError())
  {
    if (filePath)
    {
      Text path(canonical);
      result = FilePath::fromNetPath(path, filePath);
    }
    if (fullPath)
    {
      Text path(expanded);
      result = FilePath::fromNetPath(path, fullPath);
    }
  }
  return result;
}

NTLX_CORE_END
