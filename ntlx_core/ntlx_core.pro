#-------------------------------------------------
#
# Project created by QtCreator 2018-04-12T16:44:10
#
#-------------------------------------------------

QT       -= gui

VERSION = 1.1.0.0
TARGET = ntlx_core
TARGET_EXT = .dll
TEMPLATE = lib

TARGET = ntlx_core
TEMPLATE = lib

DEFINES += NTLX_CORE_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH -= $$PWD
INCLUDEPATH += $$PWD/..

DEPENDPATH -= $$PWD
DEPENDPATH += $$PWD/..

SOURCES += \
        apiresult.cpp \
    allcompositeiterator.cpp \
    any.cpp \
    cdrecord.cpp \
    computedformula.cpp \
    database.cpp \
    dbinfo.cpp \
    dir.cpp \
    distinguishedname.cpp \
    environment.cpp \
    file.cpp \
    fileinfo.cpp \
    filepath.cpp \
    formula.cpp \
    item.cpp \
    itemtable.cpp \
    lockedformula.cpp \
    main.cpp \
    note.cpp \
    noteidcollection.cpp \
    number.cpp \
    numberrange.cpp \
    text.cpp \
    textlist.cpp \
    thread.cpp \
    timedate.cpp \
    timedaterange.cpp \
    cd/cdtext.cpp \
    hook.cpp \
    http/httpevent.cpp \
    http/httprequest.cpp \
    http/httpresponse.cpp \
    http/httpauthenticate.cpp \
    http/httpfullresponse.cpp \
    attachfilesiterator.cpp \
    lookupdirectories.cpp \
    http/httpservice.cpp \
    http/httpqueries.cpp \
    http/httpcontext.cpp \
    svt/consolelogger.cpp \
    svt/logmessagetext.cpp \
    http/httpgetallheaders.cpp \
    loglevel.cpp

HEADERS += \
        apiresult.h \
        ntlx_core_global.h \
    allcompositeiterator.h \
    any.h \
    cdrecord.h \
    cdrecorditerator.h \
    computedformula.h \
    database.h \
    dbinfo.h \
    dir.h \
    distinguishedname.h \
    environment.h \
    file.h \
    fileinfo.h \
    filepath.h \
    formula.h \
    item.h \
    iteminfoiterator.h \
    itemiterator.h \
    itemtable.h \
    lockedformula.h \
    main.h \
    note.h \
    noteidcollection.h \
    number.h \
    numberrange.h \
    range.h \
    resultful.h \
    searchiterator.h \
    serveriterator.h \
    text.h \
    textlist.h \
    thread.h \
    timedate.h \
    timedaterange.h \
    cd/cd.h \
    cd/cdtext.h \
    hook.h \
    http/httpevent.h \
    http/httprequest.h \
    http/httpresponse.h \
    http/httpauthenticate.h \
    http/httpfullresponse.h \
    attachfilesiterator.h \
    lookupdirectories.h \
    http/httpservice.h \
    http/httpqueries.h \
    http/httpgetheader.h \
    http/httpcontext.h \
    svt/consolelogger.h \
    svt/logmessagetext.h \
    http/httpgetallheaders.h \
    http/httpaddheader.h \
    loglevel.h

#
# Notes API用マクロ定義
#
win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
}
else:unix {
    DEFINES += UNIX LINUX W32
    QMAKE_CXXFLAGS += -std=c++0x
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -lnotes

#
# Windows/32
# "INCLUDEPATH+=C:/Users/Public/notesapi/include" "LIBS+=-LC:/Users/Public/notesapi/lib/mswin32"
#

#
# Windows/64
# "INCLUDEPATH+=C:/Users/Public/notesapi/include" "LIBS+=-LC:/Users/Public/notesapi/lib/mswin64"
#

#
# MacOS
# "INCLUDEPATH+=/Users/Shared/notesapi/include" "LIBS+=-L'/Applications/IBM Notes.app/Contents/MacOS'"
#
