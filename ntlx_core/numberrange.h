﻿#ifndef NTLX_CORE_NUMBERRANGE_H
#define NTLX_CORE_NUMBERRANGE_H

#include <ntlx_core/number.h>
#include <ntlx_core/range.h>

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT NumberPair
    : public Pair<Number, NUMBER_PAIR, NFMT>
{
public:
  NumberPair();

  NumberPair(const NUMBER_PAIR* pData, int size);

  NumberPair(const NUMBER_PAIR& data);

  NumberPair(const NumberPair& other);

  NumberPair& operator =(const NumberPair& other);

  virtual ~NumberPair();

  Text toText(
      const INTLFORMAT* pIntlFormat = nullptr
      , const NFMT* pTextFormat = nullptr
      ) const override;
};

class NTLX_CORESHARED_EXPORT NumberRange
    : public Range<Number, NumberPair, NUMBER, NUMBER_PAIR, NFMT>
{
public:
  NumberRange();

  NumberRange(const char* pBin, int size);

  NumberRange(const NumberRange& other);

  NumberRange& operator =(const NumberRange& other);

  virtual ~NumberRange();
};

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::NumberPair)
Q_DECLARE_METATYPE(ntlx::NumberRange)

#endif // NTLX_CORE_NUMBERRANGE_H
