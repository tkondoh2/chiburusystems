﻿#ifndef NTLX_CORE_DBINFO_H
#define NTLX_CORE_DBINFO_H

#include <ntlx_core/resultful.h>
#include <ntlx_core/text.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdb.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class Database;

class DbInfoInterface
{
public:
  virtual Text title() const
  {
    return getInfo(INFOPARSE_TITLE);
  }

  virtual Text categories() const
  {
    return getInfo(INFOPARSE_CATEGORIES);
  }

  virtual Text templateName() const
  {
    return getInfo(INFOPARSE_CLASS);
  }

  virtual Text inheritedTemplateName() const
  {
    return getInfo(INFOPARSE_DESIGN_CLASS);
  }

protected:
  virtual Text getInfo(WORD what) const = 0;
};

class NTLX_CORESHARED_EXPORT DbInfo
    : public Resultful
    , public DbInfoInterface
{
public:
  DbInfo(DBHANDLE hDB);

protected:
  virtual Text getInfo(WORD what) const override;

private:
  Text info_;
};

NTLX_CORE_END

#endif // NTLX_CORE_DBINFO_H
