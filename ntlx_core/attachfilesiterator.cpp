﻿#include "attachfilesiterator.h"
#include <QMap>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdata.h>
#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

void AttachFilesIterator::operator ()(
    BLOCKID& bidItem
    , WORD dataType
    , BLOCKID& bidValue
    , DWORD /*valueLength*/
    )
{
  if (dataType != TYPE_OBJECT) return;

  char* pValue = OSLockBlock(char, bidValue) + sizeof(WORD);
  if (reinterpret_cast<OBJECT_DESCRIPTOR*>(pValue)->ObjectType == OBJECT_FILE)
  {
    FILEOBJECT* pFileObject = reinterpret_cast<FILEOBJECT*>(pValue);
    ntlx::Text fileName(
          pValue + sizeof(FILEOBJECT)
          , pFileObject->FileNameLength
          );
    pMap_->insert(fileName, bidItem);
  }
}

NTLX_CORE_END
