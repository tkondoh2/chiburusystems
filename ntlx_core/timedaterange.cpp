﻿#include "timedaterange.h"

NTLX_CORE_BEGIN

TimeDatePair::TimeDatePair()
  : Pair<TimeDate, TIMEDATE_PAIR, TFMT>()
{
}

TimeDatePair::TimeDatePair(const TIMEDATE_PAIR* pData, int size)
  : Pair<TimeDate, TIMEDATE_PAIR, TFMT>(pData, size)
{
}

TimeDatePair::TimeDatePair(const TIMEDATE_PAIR& data)
  : Pair<TimeDate, TIMEDATE_PAIR, TFMT>(data)
{
}

TimeDatePair::TimeDatePair(const TimeDatePair& other)
  : Pair<TimeDate, TIMEDATE_PAIR, TFMT>(other)
{
}

TimeDatePair& TimeDatePair::operator =(const TimeDatePair& other)
{
  if (this != &other)
    Pair<TimeDate, TIMEDATE_PAIR, TFMT>::operator =(other);
  return *this;
}

TimeDatePair::~TimeDatePair()
{
}

Text TimeDatePair::toText(
    const INTLFORMAT* pIntlFormat
    , const TFMT* pTextFormat
    ) const
{
  char text[MAXALPHATIMEDATEPAIR + 1];
  WORD len;
  lastResult_ = ConvertTIMEDATEPAIRToText(
        pIntlFormat
        , pTextFormat
        , &value_
        , text
        , MAXALPHATIMEDATEPAIR
        , &len
        );
  Q_ASSERT(len < MAXALPHATIMEDATEPAIR + 1);
  return Text(text, len);
}

TimeDateRange::TimeDateRange()
  : Range<TimeDate, TimeDatePair, TIMEDATE, TIMEDATE_PAIR, TFMT>()
{
}

TimeDateRange::TimeDateRange(const char* pBin, int size)
  : Range<TimeDate, TimeDatePair, TIMEDATE, TIMEDATE_PAIR, TFMT>(pBin, size)
{
}

TimeDateRange::TimeDateRange(const TimeDateRange& other)
  : Range<TimeDate, TimeDatePair, TIMEDATE, TIMEDATE_PAIR, TFMT>(other)
{
}

TimeDateRange& TimeDateRange::operator =(const TimeDateRange& other)
{
  if (this != &other)
    Range<TimeDate, TimeDatePair, TIMEDATE, TIMEDATE_PAIR, TFMT>::operator =(other);
  return *this;
}

TimeDateRange::~TimeDateRange()
{
}

NTLX_CORE_END
