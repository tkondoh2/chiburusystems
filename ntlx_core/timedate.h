﻿#ifndef NTLX_CORE_TIMEDATE_H
#define NTLX_CORE_TIMEDATE_H

#include <ntlx_core/any.h>
#include <ntlx_core/resultful.h>

#include <QDateTime>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <intl.h>
#include <misc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT TimeDate
    : public Resultful
{
public:

  TimeDate();

  TimeDate(const char* pBin, int);

  TimeDate(const TIMEDATE& timeDate);

  TimeDate(const TimeDate& other);

  virtual ~TimeDate();

  TimeDate& operator =(const TimeDate& other);

  TIMEDATE value() const;

  const TIMEDATE* constData() const;

  Text toText(
      const INTLFORMAT* pIntlFormat = nullptr
      , const TFMT* pTextFormat = nullptr
      ) const;

  QString toQString(
      const INTLFORMAT* pIntlFormat = nullptr
      , const TFMT* pTextFormat = nullptr
      ) const;

  QDateTime toQDateTime() const;

  static TIMEDATE getCurrentTIMEDATE();

  TimeDate addSecs(long s) const;

  static TimeDate fromQDateTime(const QDateTime& dateTime);

  QVariant toQVariant() const;

private:
  TIMEDATE value_;
};

inline TIMEDATE TimeDate::value() const
{
  return value_;
}

inline const TIMEDATE* TimeDate::constData() const
{
  return &value_;
}

inline QVariant TimeDate::toQVariant() const
{
  return toQDateTime();
}

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::TimeDate)

#endif // NTLX_CORE_TIMEDATE_H
