﻿#ifndef NTLX_CORE_ATTACHFILESITERATOR_H
#define NTLX_CORE_ATTACHFILESITERATOR_H

#include <ntlx_core/iteminfoiterator.h>
#include <ntlx_core/text.h>

NTLX_CORE_BEGIN

#ifndef AttachFileMap
using AttachFileMap = QMap<ntlx::Text, BLOCKID>;
#endif

class AttachFilesIterator
    : public ItemInfoIteratorInterface
{
public:
  AttachFilesIterator(AttachFileMap* pMap)
    : pMap_(pMap)
  {
  }

  void operator ()(
      BLOCKID& bidItem
      , WORD dataType
      , BLOCKID& bidValue
      , DWORD valueLength
      ) override;

private:
  AttachFileMap* pMap_;
};

NTLX_CORE_END

#endif // NTLX_CORE_ATTACHFILESITERATOR_H
