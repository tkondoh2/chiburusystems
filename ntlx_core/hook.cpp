﻿#include "hook.h"
#include "database.h"
#include "dbinfo.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfnote.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

Hook::Hook(
    DBHOOKVEC* vec
    , char* UserName
    , LIST* GroupList
    , DBHANDLE hDB
    )
  : Resultful()
  , vec_(vec)
  , UserName_(UserName)
  , GroupList_(GroupList)
  , hDB_(hDB)
{
}

Hook::Hook(const Hook& other)
  : Resultful()
  , vec_(other.vec_)
  , UserName_(other.UserName_)
  , GroupList_(other.GroupList_)
  , hDB_(other.hDB_)
{
}

Hook& Hook::operator =(const Hook& other)
{
  if (this != &other)
  {
    vec_ = other.vec_;
    UserName_ = other.UserName_;
    GroupList_ = other.GroupList_;
    hDB_ = other.hDB_;
  }
  return *this;
}

Hook::~Hook()
{
}

const FilePath& Hook::filePath() const
{
  if (filePath_.isEmpty())
  {
    lastResult_ = ntlx::Database::getPath(hDB_, &filePath_);
  }
  return filePath_;
}

Text Hook::dbTitle() const
{
  return ntlx::DbInfo(hDB_).title();
}

NoteHook::NoteHook(
    DBHOOKVEC* vec
    , char* UserName
    , LIST* GroupList
    , DBHANDLE hDB
    , NOTEID NoteID
    , NOTEHANDLE hNote
    )
  : Hook(vec, UserName, GroupList, hDB)
  , NoteID_(NoteID)
  , hNote_(hNote)
{
}

NoteHook::NoteHook(const NoteHook& other)
  : Hook(other)
  , NoteID_(other.NoteID_)
  , hNote_(other.hNote_)
{
}

NoteHook& NoteHook::operator =(const NoteHook& other)
{
  if (this != &other)
  {
    Hook::operator =(other);
    NoteID_ = other.NoteID_;
    hNote_ = other.hNote_;
  }
  return *this;
}

NoteHook::~NoteHook()
{
}

OpenNoteHook::OpenNoteHook(
    DBHOOKVEC* vec
    , char* UserName
    , LIST* GroupList
    , DBHANDLE hDB
    , NOTEID NoteID
    , NOTEHANDLE hNote
    , WORD OpenFlags
    )
  : NoteHook(vec, UserName, GroupList, hDB, NoteID, hNote)
  , OpenFlags_(OpenFlags)
{
}

OpenNoteHook::OpenNoteHook(const OpenNoteHook& other)
  : NoteHook(other)
  , OpenFlags_(other.OpenFlags_)
{
}

OpenNoteHook& OpenNoteHook::operator =(const OpenNoteHook& other)
{
  if (this != &other)
  {
    NoteHook::operator =(other);
    OpenFlags_ = other.OpenFlags_;
  }
  return *this;
}

OpenNoteHook::~OpenNoteHook()
{
}

const char* OpenNoteHook::typeName() const
{
  return "Open";
}

UpdateNoteHook::UpdateNoteHook(
    DBHOOKVEC* vec
    , char* UserName
    , LIST* GroupList
    , DBHANDLE hDB
    , NOTEID NoteID
    , NOTEHANDLE hNote
    , WORD* UpdateFlags
    )
  : NoteHook(vec, UserName, GroupList, hDB, NoteID, hNote)
  , UpdateFlags_(UpdateFlags)
{
}

UpdateNoteHook::UpdateNoteHook(const UpdateNoteHook& other)
  : NoteHook(other)
  , UpdateFlags_(other.UpdateFlags_)
{
}

UpdateNoteHook& UpdateNoteHook::operator =(const UpdateNoteHook& other)
{
  if (this != &other)
  {
    NoteHook::operator =(other);
    UpdateFlags_ = other.UpdateFlags_;
  }
  return *this;
}

UpdateNoteHook::~UpdateNoteHook()
{
}

const char* UpdateNoteHook::typeName() const
{
  if (NoteID() == NOTEID_ADD
      || NoteID() == NOTEID_ADD_OR_REPLACE
      || NoteID() == NOTEID_ADD_UNID
      )
  {
    if (*UpdateFlags_ & UPDATE_DELETED)
      return "DeleteStub";
    else
      return "Create";
  }
  else
  {
    if (*UpdateFlags_ & UPDATE_DELETED)
      return "Delete";
  }
  return "Update";
}

DbStampNotesHook::DbStampNotesHook(
    DBHOOKVEC* vec
    , char* UserName
    , LIST* GroupList
    , DBHANDLE hDB
    , DHANDLE hIDTable
    , char* ItemName
    , WORD ItemNameLength
    , void* Data
    , WORD Length
    )
  : Hook(vec, UserName, GroupList, hDB)
  , hIDTable_(hIDTable)
  , ItemName_(ItemName)
  , ItemNameLength_(ItemNameLength)
  , Data_(Data)
  , Length_(Length)
{
}

DbStampNotesHook::DbStampNotesHook(const DbStampNotesHook& other)
  : Hook(other)
  , hIDTable_(other.hIDTable_)
  , ItemName_(other.ItemName_)
  , ItemNameLength_(other.ItemNameLength_)
  , Data_(other.Data_)
  , Length_(other.Length_)
{
}

DbStampNotesHook& DbStampNotesHook::operator =(const DbStampNotesHook& other)
{
  if (this != &other)
  {
    Hook::operator =(other);
    hIDTable_ = other.hIDTable_;
    ItemName_ = other.ItemName_;
    ItemNameLength_ = other.ItemNameLength_;
    Data_ = other.Data_;
    Length_ = other.Length_;
  }
  return *this;
}

DbStampNotesHook::~DbStampNotesHook()
{
}

const char* DbStampNotesHook::typeName() const
{
  return "DbStampNotes";
}

NTLX_CORE_END
