﻿#include "number.h"
#include "text.h"

NTLX_CORE_BEGIN

Number::Number()
  : Resultful()
  , value_(0.0)
{
}

Number::Number(const char* pBin, int)
  : Resultful()
  , value_(*reinterpret_cast<const NUMBER*>(pBin))
{
}

Number::Number(NUMBER num)
  : Resultful()
  , value_(num)
{
}

Number::Number(const Number& other)
  : Resultful()
  , value_(other.value_)
{
}

Number& Number::operator =(const Number& other)
{
  if (this != &other)
    value_ = other.value_;
  return *this;
}

Number::~Number()
{
}

Text Number::toText(
    const INTLFORMAT* pIntlFormat
    , const NFMT* pTextFormat
    ) const
{
  char text[MAXALPHANUMBER + 1];
  WORD len;
  lastResult_ = ConvertFLOATToText(
        pIntlFormat
        , pTextFormat
        , const_cast<NUMBER*>(&value_)
        , text
        , MAXALPHANUMBER
        , &len
        );
  Q_ASSERT(len < MAXALPHANUMBER + 1);
  return Text(text, len);
}

QString Number::toQString(
    const INTLFORMAT* pIntlFormat
    , const NFMT* pTextFormat
    ) const
{
  return toText(pIntlFormat, pTextFormat).toQString();
}

NTLX_CORE_END
