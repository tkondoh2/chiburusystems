﻿#ifndef NTLX_CORE_FORMULA_H
#define NTLX_CORE_FORMULA_H

#include <ntlx_core/resultful.h>
#include <ntlx_core/text.h>
#include <QMap>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class Any;

class NTLX_CORESHARED_EXPORT Formula
    : public Resultful
{
public:

  class NTLX_CORESHARED_EXPORT CompileError
  {
  public:
    CompileError();

  private:
    STATUS status_;
    WORD line_;
    WORD column_;
    WORD offset_;
    WORD length_;

    friend class Formula;
  };

  Formula();

  Formula(const Text& source);

  Formula(const Formula& other);

  Formula& operator =(const Formula& other);

  virtual ~Formula();

  operator FORMULAHANDLE() const { return handle_; }

  WORD odsLength() const;

  Formula& compile(const Text& source, const Text& viewColumnName = Text());

  void free();

  void assign(const char* binary, WORD len);

private:
  FORMULAHANDLE handle_;
  WORD length_;
  CompileError compileError_;
};


class NTLX_CORESHARED_EXPORT FormulaStatement
{
public:
  FormulaStatement(const Text& statement);
  void setParam(const Text& placeholder, const Text& replaced);
  Text replacedText() const;
  static Text escaped(const Text& param);
private:
  Text statement_;
  QMap<Text,Text> params_;
};

NTLX_CORE_END

#endif // NTLX_CORE_FORMULA_H
