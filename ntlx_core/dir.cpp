﻿#include <ntlx_core/dir.h>

NTLX_CORE_BEGIN

Dir::Dir(const FilePath& path, bool withOpen)
  : File(path, withOpen)
{
}

Dir::~Dir()
{
}

Dir& Dir::listUp(
    SearchIteratorInterface* iterator
    , WORD searchFlags
    , WORD noteClassMask
    )
{
  File::search(iterator, Text(), Text(), searchFlags, noteClassMask);
  return *this;
}

NTLX_CORE_END
