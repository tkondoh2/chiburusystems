﻿#ifndef NTLX_CORE_ENVIRONMENT_H
#define NTLX_CORE_ENVIRONMENT_H

#include <ntlx_core/resultful.h>

NTLX_CORE_BEGIN

class Text;

class NTLX_CORESHARED_EXPORT Environment
    : public Resultful
{
public:
  Environment();

  static Text value(const Text& name);
  static void setValue(const Text& name, const Text& value);

  static Text dataDirectory();
};

NTLX_CORE_END

#endif // NTLX_CORE_ENVIRONMENT_H
