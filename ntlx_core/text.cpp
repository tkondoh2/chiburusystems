﻿#include <ntlx_core/text.h>
#include <QString>

#if defined(NT)
#pragma pack(push, 1)
#endif

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

Text::Text()
  : value_()
{
}

Text::Text(const Text& other)
  : value_(other.value_)
{
}

Text::Text(const char* pLmbcs, WORD byteSize)
  : value_()
{
  assign<WORD>(pLmbcs, byteSize);
}

Text::Text(const char* pLmbcs)
  : value_()
{
  WORD bytes = 0;
  NLS_string_bytes(
        reinterpret_cast<BYTE*>(const_cast<char*>(pLmbcs))
        , NLS_NULLTERM
        , &bytes
        , OSGetLMBCSCLS()
        );
  if (bytes > 0) --bytes;
  assign<WORD>(pLmbcs, bytes);
}

const char* Text::constData() const
{
  return value_.constData();
}

char* Text::data()
{
  return value_.data();
}

WORD Text::byteSize() const
{
  return static_cast<WORD>(value_.size());
}

WORD Text::charSize() const
{
  return getCharSize(constData(), static_cast<WORD>(value_.size()));
}

bool Text::isEmpty() const
{
  return value_.isEmpty();
}

bool Text::contains(char c) const
{
  return value_.contains(c);
}

Text& Text::append(const Text& other)
{
  // QByteArray単位でつなげる。
  QByteArray x = value_ + other.value_;

  // signed long intベースで長さを取得する。
  int nLen = x.size();

  // 長さが制限値を超えていなければそのまま値にする。
  if (nLen <= static_cast<int>(LMBCS_MAX_SIZE))
    value_ = x;

  // 長さが制限値を超えていたら
  else
  {
    // 制限値内で収まる文字数を取得する。
    WORD wCharLen = getCharSize(x.constData(), LMBCS_MAX_SIZE);

    // 制限値内で収まるバイト数を取得する。
    WORD wByteLen = getByteSize(x.constData(), wCharLen);

    // 制限バイト数分だけvalue_に反映する。
    value_ = x.left(static_cast<int>(wByteLen));
  }

  // 自身への参照を返す。
  return *this;
}

QString Text::toQString(NLS_STATUS* pStatus) const
{
  // NLSステータスの初期化
  ntlx::assign<NLS_STATUS>(pStatus, NLS_SUCCESS);

  // UNICODE文字コードセットをロード
  NLS_PINFO pUnicodeInfo;
  NLS_load_charset(NLS_CS_UNICODE, &pUnicodeInfo);

  // 変換後のUNICODE文字列格納用バッファ
  char buffer[UTF16_MAX_SIZE];
  WORD retByteSize = UTF16_MAX_SIZE;

  NLS_STATUS nlsStatus = NLS_translate(
        reinterpret_cast<BYTE*>(const_cast<char*>(constData()))
        , byteSize()
        , reinterpret_cast<BYTE*>(buffer)
        , &retByteSize
        , NLS_NONULLTERMINATE | NLS_SOURCEISLMBCS | NLS_TARGETISUNICODE
        , pUnicodeInfo
        );

  // 変換後のQStringオブジェクト
  QString qstr;
  if (nlsStatus == NLS_SUCCESS)
  {
    // UNICODEバイト列からQStringを作成して追加
    qstr = QString::fromUtf16(
          reinterpret_cast<ushort*>(buffer)
          , static_cast<int>(retByteSize / UTF16_SIZE)
          );
  }
  else
    ntlx::assign<NLS_STATUS>(pStatus, nlsStatus);

  // ロードした文字セットをアンロード
  NLS_unload_charset(pUnicodeInfo);

  // ヌル文字を改行に変換
  return qstr;
}

Text& Text::operator =(const Text& other)
{
  if (this != &other)
    value_ = other.value_;
  return *this;
}

Text& Text::operator +=(const Text& other)
{
  return append(other);
}

Text Text::fromQString(const QString& qstr, NLS_STATUS* pStatus)
{
  // NLSステータスの初期化
  ntlx::assign<NLS_STATUS>(pStatus, NLS_SUCCESS);

  // 変換する文字数が最大値を超えないようにする。
  WORD maxCharSize = qstr.size() < static_cast<int>(UTF16_MAX_CHARS)
      ? static_cast<WORD>(qstr.size())
      : UTF16_MAX_CHARS;

  // 変換後のLMBCS文字列格納用バッファ
  char buffer[LMBCS_MAX_SIZE];
  WORD retByteSize = LMBCS_MAX_SIZE;

  // QString(UTF16)からLMBCSに変換
  NLS_STATUS nlsStatus = NLS_translate(
        reinterpret_cast<BYTE*>(const_cast<ushort*>(qstr.utf16()))
        , maxCharSize * UTF16_SIZE
        , reinterpret_cast<BYTE*>(buffer)
        , &retByteSize
        , NLS_NONULLTERMINATE | NLS_SOURCEISUNICODE | NLS_TARGETISLMBCS
        , OSGetLMBCSCLS()
        );

  // 変換に成功したらバッファからTextオブジェクトを生成する。
  Text retText;
  if (nlsStatus == NLS_SUCCESS)
    retText = Text(buffer, retByteSize);
  else
    ntlx::assign<NLS_STATUS>(pStatus, nlsStatus);

  return retText;
}

WORD Text::getCharSize(const char* c_str, WORD len, NLS_PINFO pInfo)
{
  WORD size = 0;
  NLS_string_chars(reinterpret_cast<const BYTE*>(c_str), len, &size, pInfo);
  return size;
}

WORD Text::getByteSize(const char* c_str, WORD len, NLS_PINFO pInfo)
{
  WORD size = 0;
  NLS_string_bytes(reinterpret_cast<const BYTE*>(c_str), len, &size, pInfo);
  return size;
}

bool operator ==(const Text& lhs, const Text& rhs)
{
  return (lhs.value_ == rhs.value_);
}

bool operator !=(const Text& lhs, const Text& rhs)
{
  return (lhs.value_ != rhs.value_);
}

bool operator <(const Text& lhs, const Text& rhs)
{
  return (lhs.value_ < rhs.value_);
}

bool operator >(const Text& lhs, const Text& rhs)
{
  return (lhs.value_ > rhs.value_);
}

bool operator <=(const Text& lhs, const Text& rhs)
{
  return (lhs.value_ <= rhs.value_);
}

bool operator >=(const Text& lhs, const Text& rhs)
{
  return (lhs.value_ >= rhs.value_);
}

Text operator +(const Text& lhs, const Text& rhs)
{
  Text ret = lhs;
  ret += rhs;
  return ret;
}

Text::const_iterator Text::constBegin() const
{
  return Text::const_iterator(*this, 0);
}

Text::const_iterator Text::constEnd() const
{
  return Text::const_iterator(*this, charSize());
}

void Text::replace(char before, char after)
{
  value_.replace(before, after);
}

void Text::replace(const Text& before, const Text& after)
{
  value_.replace(before.value_, after.value_);
}

void Text::replaceZtoCR()
{
  replace('\0', '\n');
}

void Text::replaceCRtoZ()
{
  replace('\n', '\0');
}

// ------------------------------------------------------------------------- //

Text::const_iterator::const_iterator(const const_iterator& other)
  : pText_(other.pText_)
  , charPos_(other.charPos_)
  , curChar_(other.curChar_)
{
}

Text::const_iterator::const_iterator(const Text& text, WORD charPos)
  : pText_(&text)
  , charPos_(charPos)
  , curChar_()
{
  // 指定の文字位置が残文字数を超えないようにする。
  if (charPos_ > pText_->charSize())
    charPos_ = pText_->charSize();

  // 現在位置の文字を取得する。
  getCurrentChar();
}

const Text& Text::const_iterator::operator*() const
{
  return curChar_;
}

Text::const_iterator& Text::const_iterator::operator++()
{
  if (charPos_ < pText_->charSize())
    ++charPos_;
  getCurrentChar();
  return *this;
}

void Text::const_iterator::getCurrentChar()
{
  if (charPos_ >= pText_->charSize())
    curChar_ = Text();
  else
  {
    const char* pData = pText_->constData();
    WORD skipSize = 0;
    if (charPos_ > 0)
      skipSize = Text::getByteSize(pData, charPos_);
    WORD byteSize = Text::getByteSize(pData + skipSize, 1);
    curChar_ = Text(pData + skipSize, byteSize);
  }
}

bool operator ==(const Text::const_iterator& lhs, const Text::const_iterator& rhs)
{
  return (lhs.pText_ == rhs.pText_ && lhs.charPos_ == rhs.charPos_);
}

bool operator !=(const Text::const_iterator& lhs, const Text::const_iterator& rhs)
{
  return !operator==(lhs, rhs);
}

NTLX_CORE_END
