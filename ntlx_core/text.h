﻿#ifndef NTLX_CORE_TEXT_H
#define NTLX_CORE_TEXT_H

#include <ntlx_core/ntlx_core_global.h>
#include <QByteArray>
#include <QMetaType>
#include <QVariant>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nls.h>
#include <osmisc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

/**
 * @brief テキスト(LMBCS)クラス
 */
class NTLX_CORESHARED_EXPORT Text
{
public:

  // ===== Inner classes =====

  /**
   * @brief 定数イテレータクラス
   */
  class const_iterator;

  // ===== Constant values =====

  /**
   * @brief LMBCS文字列の最大サイズ(バイト単位)
   */
  static const WORD LMBCS_MAX_SIZE = 0xfffe;

  /**
   * @brief Unicode文字のサイズ
   */
  static const std::size_t UTF16_SIZE = sizeof(ushort);

  /**
   * @brief Unicode文字列の最大文字数(文字単位)
   */
  static const WORD UTF16_MAX_CHARS = LMBCS_MAX_SIZE / UTF16_SIZE;

  /**
   * @brief Unicode文字列の最大サイズ(バイト単位)
   */
  static const WORD UTF16_MAX_SIZE = UTF16_MAX_CHARS * UTF16_SIZE;

  // ===== Constructors & Destructors =====

  /**
   * @brief デフォルトコンストラクタ
   */
  Text();

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクトへの参照
   */
  Text(const Text& other);

  /**
   * @brief コンストラクタ
   * @param pLmbcs LMBCS形式の文字列ポインタ
   * @param byteSize 文字列のバイト長
   */
  Text(const char* pLmbcs, WORD byteSize);

  Text(const char* pLmbcs);

  // ===== Instance methods =====

  /**
   * @brief データのポインタ定数を返す。
   * @return データのポインタ定数
   */
  const char* constData() const;

  /**
   * @brief データのポインタを返す。
   * @return データのポインタ
   */
  char* data();

  /**
   * @brief データのバイトサイズを返す。
   * @return データのバイトサイズ
   */
  WORD byteSize() const;

  /**
   * @brief データの文字数を返す。
   * @return データの文字数
   */
  WORD charSize() const;

  /**
   * @brief 空かどうかを返す。
   * @return 空であればTrue
   */
  bool isEmpty() const;

  bool contains(char c) const;

  /**
   * @brief 現在のTextにもう一方のTextをつなげる。
   * @param other つなげるTextオブジェクト
   * @return otherをつなげた後の自身への参照
   */
  Text& append(const Text& other);

  /**
   * @brief QStringオブジェクトに変換する。
   * @return 変換後のQStringオブジェクト
   */
  QString toQString(NLS_STATUS* pStatus = nullptr) const;

  /**
   * @brief 最初の定数イテレータを返す。
   * @return 最初の定数イテレータ
   */
  const_iterator constBegin() const;

  /**
   * @brief 最後の定数イテレータを返す。
   * @return 最後の定数イテレータ
   */
  const_iterator constEnd() const;

  void replace(char before, char after);
  void replace(const Text& before, const Text& after);

  void replaceZtoCR();
  void replaceCRtoZ();

  QVariant toQVariant() const;

  // ===== Operator methods =====

  /**
   * @brief 代入演算子
   * @param other 代入元オブジェクトへの参照
   * @return 自身への参照
   */
  Text& operator =(const Text& other);

  /**
   * @brief 加算代入演算子
   * @param other つなげるTextオブジェクト
   * @return otherをつなげた後の自身への参照
   */
  Text& operator +=(const Text& other);

  // ===== Class methods =====

  /**
   * @brief QStringオブジェクトをTextオブジェクトに変換する。
   * @param qstr 変換元のQStringオブジェクト
   * @return 変換後のTextオブジェクト
   */
  static Text fromQString(const QString& qstr, NLS_STATUS* pStatus = nullptr);

  /**
   * @brief 文字セット文字列の文字数を返す。
   * @param c_str 文字セット文字列
   * @param len 文字セット文字列のバイト長
   * @param pInfo 文字セット(デフォルトはLMBCS)
   * @return 文字数
   */
  static WORD getCharSize(
      const char* c_str
      , WORD len
      , NLS_PINFO pInfo = OSGetLMBCSCLS()
      );

  /**
   * @brief 文字セット文字列の文字数に対応するバイト長を返す。
   * @param c_str 文字セット文字列
   * @param len 文字セット文字列の文字数
   * @param pInfo 文字セット(デフォルトはLMBCS)
   * @return バイト長
   */
  static WORD getByteSize(
      const char* c_str
      , WORD len
      , NLS_PINFO pInfo = OSGetLMBCSCLS()
      );

  // ===== Friend operators =====

  /**
   * @brief 同値演算子
   * @param lhs 左辺のText
   * @param rhs 右辺のText
   * @return 同値ならTrue
   */
  friend NTLX_CORESHARED_EXPORT bool operator ==(const Text& lhs, const Text& rhs);
  friend NTLX_CORESHARED_EXPORT bool operator !=(const Text& lhs, const Text& rhs);

  friend NTLX_CORESHARED_EXPORT bool operator <(const Text& lhs, const Text& rhs);
  friend NTLX_CORESHARED_EXPORT bool operator >(const Text& lhs, const Text& rhs);
  friend NTLX_CORESHARED_EXPORT bool operator <=(const Text& lhs, const Text& rhs);
  friend NTLX_CORESHARED_EXPORT bool operator >=(const Text& lhs, const Text& rhs);


  /**
   * @brief 加算演算子
   * @param lhs 左辺のText
   * @param rhs 右辺のText
   * @return 左辺と右辺をつなげた新しいTextオブジェクト
   */
  friend NTLX_CORESHARED_EXPORT Text operator +(
      const Text& lhs
      , const Text& rhs
      );

private:

  // ===== Private methods =====

  /**
   * @brief LMBCS文字列を適切なサイズで格納する。
   * @tparam T サイズの型
   * @param pLmbcs LMBCS文字列ポインタ
   * @param byteSize バイトサイズ
   */
  template <typename T>
  void assign(const char* pLmbcs, T byteSize)
  {
    // サイズが0以下なら文字列なし
    if (byteSize <= 0)
      value_ = QByteArray();

    // サイズが最大値を超えていたら文字区切りを考慮して切り詰める。
    else if (byteSize > static_cast<T>(LMBCS_MAX_SIZE))
    {
      WORD newCharSize = getCharSize(pLmbcs, LMBCS_MAX_SIZE);
      WORD newByteSize = getByteSize(pLmbcs, newCharSize);
      value_ = QByteArray(pLmbcs, static_cast<int>(newByteSize));
    }
    else
      value_ = QByteArray(pLmbcs, static_cast<int>(byteSize));
  }

  // ===== Instance values =====

  /**
   * @brief LMBCS文字列のバイトデータ
   */
  QByteArray value_;
};

/**
 * @brief Textオブジェクトの昇順定数イテレータクラス
 */
class NTLX_CORESHARED_EXPORT Text::const_iterator
{
public:
  // ===== Constructors & Destructors =====

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  const_iterator(const const_iterator& other);

  /**
   * @brief コンストラクタ
   * @param text 提供元のTextオブジェクトへの参照
   * @param charPos 文字の位置(0スタート)
   */
  const_iterator(const Text& text, WORD charPos);

  // ===== Instance operators =====

  /**
   * @brief ポインタ演算子
   * @return 文字(Textオブジェクト)への参照
   */
  const Text& operator*() const;

  /**
   * @brief インクリメント演算子
   * @return 自身への参照
   */
  const_iterator& operator++();

  // ===== Friend operators =====

  /**
   * @brief 等価演算子
   * @param lhs 左辺
   * @param rhs 右辺
   * @return 等価ならtrue
   */
  friend NTLX_CORESHARED_EXPORT bool operator ==(
      const const_iterator& lhs
      , const const_iterator& rhs
      );

  /**
   * @brief 不等価演算子
   * @param lhs 左辺
   * @param rhs 右辺
   * @return 不等価ならtrue
   */
  friend NTLX_CORESHARED_EXPORT bool operator !=(
      const const_iterator& lhs
      , const const_iterator& rhs
      );

private:

  // ===== Private methods =====

  /**
   * @brief 現在位置の文字を取得
   */
  void getCurrentChar();

  // ===== Instance values =====

  /**
   * @brief 提供元のTextオブジェクトのポインタ
   */
  const Text* pText_;

  /**
   * @brief 文字位置
   */
  WORD charPos_;

  /**
   * @brief 現在位置の文字
   */
  Text curChar_;

  const_iterator();
  const_iterator& operator =(const const_iterator&);
};

inline QVariant Text::toQVariant() const
{
  return toQString();
}

/**
 * @tparam T 引数に割り当てる型
 * @tparam F QByteArray::numberに適用する型
 */
template <typename T, typename F>
Text formatId(T id)
{
  size_t w = sizeof(T) * 2;
  QByteArray s
      = QByteArray("0").repeated(w - 1)
      + QByteArray::number(static_cast<F>(id), 16);
  s = s.right(w).toUpper();
  return Text(s.constData(), static_cast<WORD>(s.size()));
}

NTLX_CORE_END

inline ntlx::Text lmbcs(const QString& qs)
{
  return ntlx::Text::fromQString(qs);
}

Q_DECLARE_METATYPE(ntlx::Text)

#endif // NTLX_CORE_TEXT_H
