﻿#include "lockedformula.h"
#include "formula.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

LockedFormula::LockedFormula(Formula &formula)
  : pFormula_(&formula)
  , pointer_(nullptr)
{
  if (*pFormula_ == NULLHANDLE)
    return;

  pointer_ = static_cast<char*>(OSLockObject(*pFormula_));
}

LockedFormula::~LockedFormula()
{
  if (pointer_ != nullptr && *pFormula_ != NULLHANDLE)
    OSUnlockObject(*pFormula_);
}

NTLX_CORE_END
