﻿#ifndef NTLX_CORE_HOOK_H
#define NTLX_CORE_HOOK_H

#include <ntlx_core/filepath.h>
#include <ntlx_core/note.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <nsfdb.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

/**
 * @brief フックドライバ共通クラス
 * @class Hook
 */
class NTLX_CORESHARED_EXPORT Hook
    : public Resultful
{
public:
  /**
   * @brief コンストラクタ
   * @param vec フックドライバ構造体へのポインタ
   * @param UserName ユーザ名へのポインタ
   * @param GroupList グループリストへのポインタ
   * @param hDB データベースハンドル
   */
  Hook(
      DBHOOKVEC* vec
      , char* UserName
      , LIST* GroupList
      , DBHANDLE hDB
      );

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  Hook(const Hook& other);

  /**
   * @brief 代入演算子
   * @param other 代入元オブジェクト
   * @return 自身への参照
   */
  Hook& operator =(const Hook& other);

  /**
   * @brief デストラクタ
   */
  virtual ~Hook();

  /**
   * @brief フックドライバ構造体へのポインタを返す。
   * @return  フックドライバ構造体へのポインタ
   */
  DBHOOKVEC* vec() const;

  /**
   * @brief ユーザ名へのポインタを返す。
   * @return ユーザ名へのポインタ
   */
  const char* UserName() const;

  /**
   * @brief グループリストへのポインタを返す。
   * @return グループリストへのポインタ
   */
  const LIST* GroupList() const;

  /**
   * @brief データベースハンドルを返す。
   * @return データベースハンドル
   */
  DBHANDLE hDB() const;

  /**
   * @brief データベースのパスを返す。
   * @return データベースのパス
   */
  const FilePath& filePath() const;

  /**
   * @brief データベースタイトルを返す。
   * @return データベースタイトル
   */
  Text dbTitle() const;

  /**
   * @brief 処理種別を文字列で返す(インターフェース)。
   * @return 処理種別
   */
  virtual const char* typeName() const = 0;

private:
  DBHOOKVEC* vec_;
  char* UserName_;
  LIST* GroupList_;
  DBHANDLE hDB_;
  mutable FilePath filePath_;
};

/**
 * @brief 文書単位の共通フックドライバ
 * @class NoteHook
 */
class NTLX_CORESHARED_EXPORT NoteHook
    : public Hook
{
public:
  /**
   * @brief コンストラクタ
   * @param vec フックドライバ構造体へのポインタ
   * @param UserName ユーザ名へのポインタ
   * @param GroupList グループリストへのポインタ
   * @param hDB データベースハンドル
   * @param NoteID 文書ID
   * @param hNote 文書ハンドル
   */
  NoteHook(
      DBHOOKVEC* vec
      , char* UserName
      , LIST* GroupList
      , DBHANDLE hDB
      , NOTEID NoteID
      , NOTEHANDLE hNote
      );

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  NoteHook(const NoteHook& other);

  /**
   * @brief 代入演算子
   * @param other 代入元オブジェクト
   * @return 自身への参照
   */
  NoteHook& operator =(const NoteHook& other);

  /**
   * @brief デストラクタ
   */
  virtual ~NoteHook();

  /**
   * @brief 文書IDを返す。
   * @return 文書ID
   */
  NOTEID NoteID() const;

  /**
   * @brief 文書ハンドルを返す。
   * @return 文書ハンドル
   */
  NOTEHANDLE hNote() const;

  /**
   * @brief UNIDを返す。
   * @return UNID
   */
  UNIVERSALNOTEID UniversalNoteID() const;

private:
  NOTEID NoteID_;
  NOTEHANDLE hNote_;
};

/**
 * @brief 文書を開いたときにフック処理するクラス
 * @class OpenNoteHook
 */
class NTLX_CORESHARED_EXPORT OpenNoteHook
    : public NoteHook
{
public:
  /**
   * @brief コンストラクタ
   * @param vec フックドライバ構造体へのポインタ
   * @param UserName ユーザ名へのポインタ
   * @param GroupList グループリストへのポインタ
   * @param hDB データベースハンドル
   * @param NoteID 文書ID
   * @param hNote 文書ハンドル
   * @param OpenFlags オープンフラグ
   */
  OpenNoteHook(
      DBHOOKVEC* vec
      , char* UserName
      , LIST* GroupList
      , DBHANDLE hDB
      , NOTEID NoteID
      , NOTEHANDLE hNote
      , WORD OpenFlags
      );

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  OpenNoteHook(const OpenNoteHook& other);

  /**
   * @brief 代入演算子
   * @param other 代入元オブジェクト
   * @return 自身への参照
   */
  OpenNoteHook& operator =(const OpenNoteHook& other);

  /**
   * @brief デストラクタ
   */
  virtual ~OpenNoteHook();

  /**
   * @brief オープンフラグを返す。
   * @return オープンフラグ
   */
  WORD OpenFlags() const;

  /**
   * @brief 処理種別を文字列で返す(インターフェース)。
   * @return 処理種別
   */
  virtual const char* typeName() const override;

protected:
  WORD OpenFlags_;
};

/**
 * @brief 文書を開いたときにフック処理するクラス
 * @class UpdateNoteHook
 */
class NTLX_CORESHARED_EXPORT UpdateNoteHook
    : public NoteHook
{
public:
  /**
   * @brief コンストラクタ
   * @param vec フックドライバ構造体へのポインタ
   * @param UserName ユーザ名へのポインタ
   * @param GroupList グループリストへのポインタ
   * @param hDB データベースハンドル
   * @param NoteID 文書ID
   * @param hNote 文書ハンドル
   * @param UpdateFlags 更新フラグへのポインタ
   */
  UpdateNoteHook(
      DBHOOKVEC* vec
      , char* UserName
      , LIST* GroupList
      , DBHANDLE hDB
      , NOTEID NoteID
      , NOTEHANDLE hNote
      , WORD* UpdateFlags
      );
  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  UpdateNoteHook(const UpdateNoteHook& other);

  /**
   * @brief 代入演算子
   * @param other 代入元オブジェクト
   * @return 自身への参照
   */
  UpdateNoteHook& operator =(const UpdateNoteHook& other);

  /**
   * @brief デストラクタ
   */
  virtual ~UpdateNoteHook();

  /**
   * @brief 更新フラグへのポインタを返す。
   * @return 更新フラグへのポインタ
   */
  WORD* UpdateFlags() const;

  /**
   * @brief 処理種別を文字列で返す(インターフェース)。
   * @return 処理種別
   */
  virtual const char* typeName() const override;

protected:
  WORD* UpdateFlags_;
};

/**
 * @brief 複数文書にスタンプ(カテゴリ変更)するときにフック処理するクラス
 * @class DbStampNotesHook
 */
class NTLX_CORESHARED_EXPORT DbStampNotesHook
    : public Hook
{
public:
  /**
   * @brief コンストラクタ
   * @brief コンストラクタ
   * @param vec フックドライバ構造体へのポインタ
   * @param UserName ユーザ名へのポインタ
   * @param GroupList グループリストへのポインタ
   * @param hDB データベースハンドル
   * @param hIDTable IDテーブルハンドル
   * @param ItemName 変更対象アイテム名へのポインタ
   * @param ItemNameLength 変更対象アイテム名の長さ
   * @param Data 変更データへのポインタ
   * @param Length 変更データの長さ
   */
  DbStampNotesHook(
      DBHOOKVEC* vec
      , char* UserName
      , LIST* GroupList
      , DBHANDLE hDB
      , DHANDLE hIDTable
      , char* ItemName
      , WORD ItemNameLength
      , void* Data
      , WORD Length
      );

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  DbStampNotesHook(const DbStampNotesHook& other);

  /**
   * @brief 代入演算子
   * @param other 代入元オブジェクト
   * @return 自身への参照
   */
  DbStampNotesHook& operator =(const DbStampNotesHook& other);

  /**
   * @brief デストラクタ
   */
  virtual ~DbStampNotesHook();

  /**
   * @brief IDテーブルハンドルを返す。
   * @return IDテーブルハンドル
   */
  DHANDLE hIDTable() const;

  /**
   * @brief 変更対象アイテム名へのポインタを返す。
   * @return 変更対象アイテム名へのポインタ
   */
  const char* ItemName() const;

  /**
   * @brief 変更対象アイテム名の長さを返す。
   * @return 変更対象アイテム名の長さ
   */
  WORD ItemNameLength() const;

  /**
   * @brief 変更データへのポインタを返す。
   * @return 変更データへのポインタ
   */
  const void* Data() const;

  /**
   * @brief 変更データの長さを返す。
   * @return 変更データの長さ
   */
  WORD Length() const;

  /**
   * @brief 処理種別を文字列で返す(インターフェース)。
   * @return 処理種別
   */
  virtual const char* typeName() const override;

private:
  DHANDLE hIDTable_;
  char* ItemName_;
  WORD ItemNameLength_;
  void* Data_;
  WORD Length_;
};

inline DBHOOKVEC* Hook::vec() const
{
  return vec_;
}

inline const char* Hook::UserName() const
{
  return UserName_;
}

inline const LIST* Hook::GroupList() const
{
  return GroupList_;
}

inline DBHANDLE Hook::hDB() const
{
  return hDB_;
}

inline NOTEID NoteHook::NoteID() const
{
  return NoteID_;
}

inline NOTEHANDLE NoteHook::hNote() const
{
  return hNote_;
}

inline UNIVERSALNOTEID NoteHook::UniversalNoteID() const
{
  return Note::getUniversalNoteId(hNote_);
}

inline WORD OpenNoteHook::OpenFlags() const
{
  return OpenFlags_;
}

inline WORD* UpdateNoteHook::UpdateFlags() const
{
  return UpdateFlags_;
}

inline DHANDLE DbStampNotesHook::hIDTable() const
{
  return hIDTable_;
}

inline const char* DbStampNotesHook::ItemName() const
{
  return ItemName_;
}

inline WORD DbStampNotesHook::ItemNameLength() const
{
  return ItemNameLength_;
}

inline const void* DbStampNotesHook::Data() const
{
  return Data_;
}

inline WORD DbStampNotesHook::Length() const
{
  return Length_;
}

NTLX_CORE_END

#endif // NTLX_CORE_HOOK_H
