﻿#ifndef NTLX_CORE_FILEINFO_H
#define NTLX_CORE_FILEINFO_H

#include <ntlx_core/itemtable.h>
#include <ntlx_core/dbinfo.h>

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT FileInfo
    : public ItemTable
    , public DbInfoInterface
{
public:
  FileInfo();

  FileInfo(SEARCH_MATCH* pMatch, ITEM_TABLE* pTable);

  FileInfo(const FileInfo& other);

  FileInfo& operator =(const FileInfo& other);

  virtual ~FileInfo();

  Text path() const;
  Text info() const;
  Text type() const;

//  Text title() const;
//  Text categories() const;
//  Text templateName() const;
//  Text inheritedTemplateName() const;

protected:
  virtual Text getInfo(WORD what) const override;
};

using FileInfoList = QList<FileInfo>;

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::FileInfo)

#endif // NTLX_CORE_FILEINFO_H
