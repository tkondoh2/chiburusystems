﻿#ifndef NTLX_CORE_LOCKEDFORMULA_H
#define NTLX_CORE_LOCKEDFORMULA_H

#include <ntlx_core/ntlx_core_global.h>

NTLX_CORE_BEGIN

class Formula;

class NTLX_CORESHARED_EXPORT LockedFormula
{
public:
  explicit LockedFormula(Formula& formula);
  virtual ~LockedFormula();
  const char* pointer() const { return pointer_; }

private:
  Formula* pFormula_;
  char* pointer_;

  LockedFormula(const LockedFormula&);
  LockedFormula& operator =(const LockedFormula&);
};

NTLX_CORE_END

#endif // NTLX_CORE_LOCKEDFORMULA_H
