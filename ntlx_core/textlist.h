﻿#ifndef NTLX_CORE_TEXTLIST_H
#define NTLX_CORE_TEXTLIST_H

#include <ntlx_core/text.h>
#include <QList>

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT TextList
{
public:
  TextList();

  TextList(const char* pData, int);

  TextList(const TextList& other);

  virtual ~TextList();

  TextList& operator =(const TextList& other);

  QStringList toQStringList() const;

  bool isEmpty() const;

  void append(const Text& item);

  const Text& at(int index) const;

  int count() const;

  QVariant toQVariant() const;

private:
  QList<Text> list_;
};

inline int TextList::count() const
{
  return list_.count();
}

inline QVariant TextList::toQVariant() const
{
  return toQStringList();
}

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::TextList)

#endif // NTLX_CORE_TEXTLIST_H
