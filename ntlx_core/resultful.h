﻿#ifndef NTLX_CORE_RESULTFUL_H
#define NTLX_CORE_RESULTFUL_H

#include <ntlx_core/apiresult.h>

NTLX_CORE_BEGIN

/**
 * @brief 処理結果をオブジェクト内に保持できるクラス
 */
class NTLX_CORESHARED_EXPORT Resultful
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  explicit Resultful();

  /**
   * @brief 最後の結果を取得する。
   * @return 最後の結果
   */
  const ApiResult& lastResult() const;

protected:

  /**
   * @brief 最後の処理結果を保持
   */
  mutable ApiResult lastResult_;
};

inline Resultful::Resultful()
  : lastResult_()
{
}

inline const ApiResult& Resultful::lastResult() const
{
  return lastResult_;
}

NTLX_CORE_END

#endif // NTLX_CORE_RESULTFUL_H
