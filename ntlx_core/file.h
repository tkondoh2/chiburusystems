﻿#ifndef NTLX_CORE_FILE_H
#define NTLX_CORE_FILE_H

#include <ntlx_core/resultful.h>
#include <ntlx_core/filepath.h>

//#include <ntlx_core/fileinfo.h>
//#include <ntlx_core/formula.h>
//#include <ntlx_core/timedate.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class SearchEnumerator;
class SearchIteratorInterface;
class Formula;
class TimeDate;

class NTLX_CORESHARED_EXPORT File
    : public Resultful
{
public:

  explicit File();

  explicit File(const FilePath& path, bool withOpen = true);

  virtual ~File();

  const FilePath& path() const;

  void setPath(const FilePath& path);

  File& open(
      WORD options = 0
      , DHANDLE hNames = NULLHANDLE
      , TIMEDATE* pModifiedTime = nullptr
      , TIMEDATE* pDataModified = nullptr
      , TIMEDATE* pNonDataModified = nullptr
      );

  File& close();

  const File& search(
      SearchIteratorInterface* iterator
      , const Text& formulaText
      , const Text& viewTitle
      , WORD searchFlags
      , WORD noteClassMask
      , TIMEDATE* pSince = nullptr
      , TIMEDATE* pUntil = nullptr
      ) const;

  static STATUS LNPUBLIC searchCallback(
      void* ptr
      , SEARCH_MATCH* pMatch
      , ITEM_TABLE* pTable
      );

  static ApiResult getPath(
      DBHANDLE hDB
      , FilePath* filePath
      , FilePath* fullPath = nullptr
      );

  operator DBHANDLE() const;

protected:
  FilePath path_;
  DBHANDLE handle_;

  friend class Note;
};

inline const FilePath& File::path() const
{
  return path_;
}

inline void File::setPath(const FilePath& path)
{
  close();
  path_ = path;
}

inline File::operator DBHANDLE() const
{
  return handle_;
}

NTLX_CORE_END

#endif // NTLX_CORE_FILE_H
