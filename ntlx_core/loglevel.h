﻿#ifndef NTLX_CORE_LOGLEVEL_H
#define NTLX_CORE_LOGLEVEL_H

#include <ntlx_core/ntlx_core_global.h>

NTLX_CORE_BEGIN

enum class NTLX_CORESHARED_EXPORT LogLevel
{
  None = 0
  , Fatal = 1
  , Error = 2
  , Warn = 3
  , Info = 4
  , Debug = 5
  , Trace = 6
};

class NTLX_CORESHARED_EXPORT LogLevelText
{
public:

  friend NTLX_CORESHARED_EXPORT QString logLevelText(LogLevel level);

  friend NTLX_CORESHARED_EXPORT LogLevel logLevel(const QString& text);

private:
  LogLevelText();
  ~LogLevelText();
  LogLevelText(const LogLevelText &);
  LogLevelText &operator =(const LogLevelText &);

  static const QMap<LogLevel, QString> map_;
};

NTLX_CORE_END

#endif // NTLX_CORE_LOGLEVEL_H
