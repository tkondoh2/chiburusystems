﻿#ifndef NTLX_CORE_CD_CDTEXT_H
#define NTLX_CORE_CD_CDTEXT_H

#include <ntlx_core/cd/cd.h>
#include <ntlx_core/text.h>

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT CdText
    : Cd<CDTEXT, _CDTEXT, SIG_CD_TEXT>
{
public:
  CdText();
  CdText(const CdText& other);
  CdText& operator =(const CdText& other);

  Text value() const;
  BYTE fontSize() const;
  BYTE fontColor() const;
  BYTE fontFaceID() const;
  bool fontIsPlain() const;
  bool fontIsUnderline() const;
  bool fontIsItalic() const;
  bool fontIsBold() const;
  bool fontIsStrikeOut() const;
  bool fontIsSuperScript() const;
  bool fontIsSubScript() const;
  bool fontIsEffect() const;
  bool fontIsShadow() const;
  bool fontIsEmboss() const;
  bool fontIsExtrude() const;
  BYTE fontShadowOffset() const;

  virtual void read(const CdRecord& record) override;

private:
  Text value_;
};

NTLX_CORE_END

#endif // NTLX_CORE_CD_CDTEXT_H
