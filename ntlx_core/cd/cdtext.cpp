﻿#include "cdtext.h"

NTLX_CORE_BEGIN

CdText::CdText()
  : Cd<CDTEXT, _CDTEXT, SIG_CD_TEXT>()
  , value_()
{
}

CdText::CdText(const CdText& other)
  : Cd<CDTEXT, _CDTEXT, SIG_CD_TEXT>(other)
  , value_(other.value_)
{
}

CdText& CdText::operator =(const CdText& other)
{
  if (this != &other)
  {
    Cd<CDTEXT, _CDTEXT, SIG_CD_TEXT>::operator =(other);
    value_ = other.value_;
  }
  return *this;
}

Text CdText::value() const
{
  return value_;
}

BYTE CdText::fontSize() const
{
  return FontGetSize(data_.FontID);
}

BYTE CdText::fontColor() const
{
  return FontGetColor(data_.FontID);
}

BYTE CdText::fontFaceID() const
{
  return FontGetFaceID(data_.FontID);
}

bool CdText::fontIsPlain() const
{
  return FontIsPlain(data_.FontID);
}

bool CdText::fontIsUnderline() const
{
  return FontIsUnderline(data_.FontID);
}

bool CdText::fontIsItalic() const
{
  return FontIsItalic(data_.FontID);
}

bool CdText::fontIsBold() const
{
  return FontIsBold(data_.FontID);
}

bool CdText::fontIsStrikeOut() const
{
  return FontIsStrikeOut(data_.FontID);
}

bool CdText::fontIsSuperScript() const
{
  return FontIsSuperScript(data_.FontID);
}

bool CdText::fontIsSubScript() const
{
  return FontIsSubScript(data_.FontID);
}

bool CdText::fontIsEffect() const
{
  return FontIsEffect(data_.FontID);
}

bool CdText::fontIsShadow() const
{
  return FontIsShadow(data_.FontID);
}

bool CdText::fontIsEmboss() const
{
  return FontIsEmboss(data_.FontID);
}

bool CdText::fontIsExtrude() const
{
  return FontIsExtrude(data_.FontID);
}

BYTE CdText::fontShadowOffset() const
{
  return FontGetShadowOffset(data_.FontID);
}

void CdText::read(const CdRecord& record)
{
  Text text;
  _read(record, [&text](char** pp, DWORD len)
  {
    text = ntlx::Text(*pp, len);
    text.replaceZtoCR();
  });
  value_ = text;
}

NTLX_CORE_END
