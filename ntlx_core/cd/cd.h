﻿#ifndef NTLX_CORE_CD_CD_H
#define NTLX_CORE_CD_CD_H

#include <ntlx_core/cdrecord.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ods.h>
#include <editods.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

template <class CD, WORD _CD, WORD SIG_CD>
class Cd
{
public:
  Cd()
    : data_()
  {
  }

  Cd(const Cd& other)
    : data_(other.data_)
  {
  }

  Cd& operator =(const Cd& other)
  {
    if (this != &other)
      data_ = other.data_;
    return *this;
  }

  virtual WORD signature() const
  {
    return SIG_CD;
  }

  virtual WORD odsLength() const
  {
    return data_.Header.Length + data_.Header.Length % 2;
  }

  virtual void read(const CdRecord& record) = 0;

protected:
  template <class F = ReadMemoryFunctor>
  void _read(
      const CdRecord& record
      , F functor = ReadMemoryFunctor()
      , WORD iterations = 1
      )
  {
    char* ptr = const_cast<char*>(record.constData());
    ODSReadMemory(&ptr, _CD, &data_, iterations);
    functor(&ptr, static_cast<DWORD>(data_.Header.Length - ODSLength(_CD)));
  }

protected:
  CD data_;
};

NTLX_CORE_END

#endif // NTLX_CORE_CD_CD_H
