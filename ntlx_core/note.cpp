﻿#include "note.h"
#include "itemiterator.h"
#include "database.h"
#include "item.h"
#include "iteminfoiterator.h"
#include "textlist.h"
#include "timedate.h"
#include "attachfilesiterator.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <stdnames.h>
#include <nsfnote.h>
#include <nsferr.h>

#if defined(NT)
#pragma pack(pop)
#endif

#include <QtDebug>

NTLX_CORE_BEGIN

Note::Note()
  : Resultful()
  , pDb_(nullptr)
  , noteId_(0)
  , handle_(NULLHANDLE)
{
}

Note::Note(Database& db, NOTEID noteId)
  : Resultful()
  , pDb_(&db)
  , noteId_(noteId)
  , handle_(NULLHANDLE)
{
}

Note::Note(WORD noteClass, Database& db)
  : Resultful()
  , pDb_(&db)
  , noteId_(0)
  , handle_(NULLHANDLE)
{
  lastResult_ = NSFNoteCreate(db, &handle_);
  Q_ASSERT(handle_);
  setNoteClass(noteClass);
}

Note::Note(const Note& other)
  : Resultful()
  , pDb_(other.pDb_)
  , noteId_(other.noteId_)
  , handle_(NULLHANDLE)
{
}

Note& Note::operator =(const Note& other)
{
  if (this != &other)
  {
    pDb_ = other.pDb_;
    noteId_ = other.noteId_;
    handle_ = NULLHANDLE;
  }
  return *this;
}

Note::~Note()
{
  close();
}

const Note& Note::open(WORD openFlags) const
{
  if (!handle_)
  {
    Q_ASSERT(pDb_);
    Q_ASSERT(pDb_->handle_);
    lastResult_ = NSFNoteOpen(pDb_->handle_, noteId_, openFlags, &handle_);
  }
  Q_ASSERT(handle_);
  return *this;
}

const Note& Note::close() const
{
  if (handle_)
  {
    lastResult_ = NSFNoteClose(handle_);
    handle_ = 0;
  }
  return *this;
}

const Note& Note::scanItems(ItemIteratorInterface* iterator) const
{
  if (open().lastResult().hasError())
    return *this;

  Q_ASSERT(handle_);
  lastResult_ = NSFItemScan(handle_, scanItemsCallback, iterator);
  return *this;
}

STATUS Note::scanItemsCallback(
    WORD /*Spare*/
    , WORD ItemFlags
    , char *Name
    , WORD NameLength
    , void *Value
    , DWORD ValueLength
    , void *RoutineParameter
    )
{
  ItemIteratorInterface* iterator
      = reinterpret_cast<ItemIteratorInterface*>(RoutineParameter);
  return (*iterator)(ItemFlags, Name, NameLength, Value, ValueLength);
}

Item Note::getItem(const Text& name, WORD flags) const
{
  Item item(name, flags);
  ItemInfoIterator<Any, Item> iiIt(&item);
  enumItemInfo(name, &iiIt);
  return item;
}

Text Note::getText(const Text& name, WORD flags) const
{
  Item item = getItem(name, flags);
  Any value = item.value();
  return value.getText();
}

TextList Note::getTextList(const Text& name, WORD flags) const
{
  Item item = getItem(name, flags);
  Any value = item.value();
  return value.getTextList();
}

Item Note::getItem(NOTEHANDLE hNote, const Text& name, WORD flags)
{
  Item item(name, flags);
  ItemInfoIterator<Any, Item> iiIt(&item);
  Note::enumItemInfo(hNote, name, &iiIt);
  return item;
}

const Note& Note::enumItemInfo(
    const Text& name
    , ItemInfoIteratorInterface* iterator
    ) const
{
  if (open().lastResult().hasError())
  {
    qDebug() << lastResult().toQString();
    return *this;
  }

  lastResult_ = enumItemInfo(handle_, name, iterator);
  return *this;
}

ApiResult Note::enumItemInfo(
    NOTEHANDLE hNote
    , const Text& name
    , ItemInfoIteratorInterface* iterator
    )
{
  Q_ASSERT(hNote);
  BLOCKID bidItem, bidValue, bidPrev;
  WORD dataType;
  DWORD valueLength;
  ApiResult result = NSFItemInfo(
        hNote
        , name.constData()
        , name.byteSize()
        , &bidItem
        , &dataType
        , &bidValue
        , &valueLength
        );
  while (result.error() != ERR_ITEM_NOT_FOUND)
  {
    if (result.hasError())
      return result;

    (*iterator)(bidItem, dataType, bidValue, valueLength);

    bidPrev = bidItem;
    result = NSFItemInfoNext(
          hNote
          , bidPrev
          , name.constData()
          , name.byteSize()
          , &bidItem
          , &dataType
          , &bidValue
          , &valueLength
          );
  }
  return NOERROR;
}

Text Note::noteIdText(NOTEID noteId)
{
  return formatId<NOTEID, uint>(noteId);
}

Text Note::globalInstanceIdText(GLOBALINSTANCEID id)
{
  return formatId<DWORD, uint>(id.File.Innards[1])
      + formatId<DWORD, uint>(id.File.Innards[0])
      + formatId<DWORD, uint>(id.Note.Innards[1])
      + formatId<DWORD, uint>(id.Note.Innards[0])
      + noteIdText(id.NoteID)
      ;
}

Text Note::originatorIdText(ORIGINATORID oid)
{
  return universalIdText<ORIGINATORID>(oid)
      + formatId<DWORD, uint>(oid.SequenceTime.Innards[1])
      + formatId<DWORD, uint>(oid.SequenceTime.Innards[0])
      + formatId<DWORD, uint>(oid.Sequence)
      ;
}

UNIVERSALNOTEID Note::getUniversalNoteId() const
{
  return getUniversalNoteId(handle_);
}

UNIVERSALNOTEID Note::getUniversalNoteId(NOTEHANDLE hNote)
{
  ORIGINATORID oid = getInfo<ORIGINATORID>(hNote, _NOTE_OID);
  UNIVERSALNOTEID unid;
  unid.File = oid.File;
  unid.Note = oid.Note;
  return unid;
}

AttachFileMap Note::getAllAttachFiles() const
{
  AttachFileMap fileMap;
  AttachFilesIterator attachFilesIt(&fileMap);
  enumItemInfo(ITEM_NAME_ATTACHMENT, &attachFilesIt);
  return fileMap;
}

QByteArray Note::extractAttachFile(const Text& fileName) const
{
  QByteArray file;
  AttachFileMap fileMap = getAllAttachFiles();
  if (lastResult().noError())
  {
    if (fileMap.contains(fileName))
    {
      lastResult_ = NSFNoteExtractWithCallback(
            handle_
            , fileMap.value(fileName)
            , NULL
            , 0
            , extractAttachFileCallback
            , &file
            );
    }
  }
  return file;
}


STATUS LNPUBLIC Note::extractAttachFileCallback(
    const BYTE* bytes
    , DWORD length
    , void* pParam
    )
{
  if (length > 0)
  {
    QByteArray* pBuffer = (QByteArray*)pParam;
    QByteArray data(reinterpret_cast<const char*>(bytes), length);
//    qDebug() << "Extract length" << length;
    *pBuffer += data;
  }
  return NOERROR;
}

void Note::setInfo(WORD _note_XXX, void *pData)
{
  if (open().lastResult().hasError()) return;
  NSFNoteSetInfo(handle_, _note_XXX, pData);
}

void Note::setNoteClass(WORD noteClass)
{
  setInfo(_NOTE_CLASS, &noteClass);
}

void Note::setTextItem(const Text& name, const Text& value, bool isSummary)
{
  if (open().lastResult().hasError()) return;
  ntlx::Text v(value);
  v.replaceCRtoZ();
  lastResult_ = NSFItemSetTextSummary(
        handle_
        , name.constData()
        , v.constData()
        , v.byteSize()
        , isSummary
        );
}

void Note::setTimeItem(const Text& name, const TimeDate& value)
{
  if (open().lastResult().hasError()) return;
  lastResult_ = NSFItemSetTime(
        handle_
        , name.constData()
        , value.constData()
        );
}

Note& Note::update(WORD updateFlags)
{
  if (handle_)
  {
    lastResult_ = NSFNoteUpdate(handle_, updateFlags);
    if (noteId_ == 0)
      noteId_ = getInfo<NOTEID>(handle_, _NOTE_ID);
  }
  return *this;
}

Text Note::noteClassText(WORD noteClass)
{
  QStringList list;
  if (noteClass & NOTE_CLASS_DOCUMENT) list << "Document";
  if (noteClass & NOTE_CLASS_INFO) list << "Help-About";
  if (noteClass & NOTE_CLASS_FORM) list << "Form";
  if (noteClass & NOTE_CLASS_VIEW) list << "View";
  if (noteClass & NOTE_CLASS_ICON) list << "Icon";
  if (noteClass & NOTE_CLASS_DESIGN) list << "Design";
  if (noteClass & NOTE_CLASS_ACL) list << "ACL";
  if (noteClass & NOTE_CLASS_HELP_INDEX) list << "Help Index";
  if (noteClass & NOTE_CLASS_HELP) list << "Help";
  if (noteClass & NOTE_CLASS_FILTER) list << "Filter";
  if (noteClass & NOTE_CLASS_FIELD) list << "Field";
  if (noteClass & NOTE_CLASS_REPLFORMULA) list << "Replication Formula";
  if (noteClass & NOTE_CLASS_PRIVATE) list << "Private";
  if (noteClass & NOTE_CLASS_DEFAULT) list << "Dafault";
  return Text::fromQString(list.join("|"));
}

TextList Note::designFlagsTextList(const Text& flags)
{
  TextList list;
//      if (flags.contains(DESIGN_FLAG_ADD)) list.append(Text("ADD"));
  if (flags.contains(DESIGN_FLAG_ANTIFOLDER)) list.append(Text("ANTIFOLDER"));
  if (flags.contains(DESIGN_FLAG_BACKGROUND_FILTER)) list.append(Text("BACKGROUND_FILTER"));
  if (flags.contains(DESIGN_FLAG_INITBYDESIGNONLY)) list.append(Text("INITBYDESIGNONLY"));
//      if (flags.contains(DESIGN_FLAG_NO_COMPOSE)) list.append(Text("NO_COMPOSE"));
  if (flags.contains(DESIGN_FLAG_CALENDAR_VIEW)) list.append(Text("CALENDAR_VIEW"));
//      if (flags.contains(DESIGN_FLAG_NO_QUERY)) list.append(Text("NO_QUERY"));
//  if (flags.contains(DESIGN_FLAG_DEFAULT_DESIGN)) list.append(Text("DEFAULT_DESIGN"));
  if (flags.contains(DESIGN_FLAG_MAIL_FILTER)) list.append(Text("MAIL_FILTER"));
  if (flags.contains(DESIGN_FLAG_PUBLICANTIFOLDER)) list.append(Text("PUBLICANTIFOLDER"));
  if (flags.contains(DESIGN_FLAG_FOLDER_VIEW)) list.append(Text("FOLDER_VIEW"));
  if (flags.contains(DESIGN_FLAG_V4AGENT)) list.append(Text("V4AGENT"));
  if (flags.contains(DESIGN_FLAG_VIEWMAP)) list.append(Text("VIEWMAP"));
  if (flags.contains(DESIGN_FLAG_FILE)) list.append(Text("FILE"));
  if (flags.contains(DESIGN_FLAG_OTHER_DLG)) list.append(Text("OTHER_DLG"));
  if (flags.contains(DESIGN_FLAG_JAVASCRIPT_LIBRARY)) list.append(Text("JAVASCRIPT_LIBRARY"));
  if (flags.contains(DESIGN_FLAG_V4PASTE_AGENT)) list.append(Text("V4PASTE_AGENT"));
  if (flags.contains(DESIGN_FLAG_IMAGE_RESOURCE)) list.append(Text("IMAGE_RESOURCE"));
  if (flags.contains(DESIGN_FLAG_JAVA_AGENT)) list.append(Text("JAVA_AGENT"));
  if (flags.contains(DESIGN_FLAG_JAVA_AGENT_WITH_SOURCE)) list.append(Text("JAVA_AGENT_WITH_SOURCE"));
  if (flags.contains(DESIGN_FLAG_MOBILE_DIGEST)) list.append(Text("MOBILE_DIGEST"));
  if (flags.contains(DESIGN_FLAG_XSPPAGE)) list.append(Text("XSPPAGE"));
  if (flags.contains(DESIGN_FLAG_CONNECTION_RESOURCE)) list.append(Text("CONNECTION_RESOURCE"));
  if (flags.contains(DESIGN_FLAG_LOTUSSCRIPT_AGENT)) list.append(Text("LOTUSSCRIPT_AGENT"));
  if (flags.contains(DESIGN_FLAG_DELETED_DOCS)) list.append(Text("DELETED_DOCS"));
  if (flags.contains(DESIGN_FLAG_QUERY_MACRO_FILTER)) list.append(Text("QUERY_MACRO_FILTER"));
  if (flags.contains(DESIGN_FLAG_SITEMAP)) list.append(Text("SITEMAP"));
  if (flags.contains(DESIGN_FLAG_NEW)) list.append(Text("NEW"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_NOTES)) list.append(Text("HIDE_FROM_NOTES"));
  if (flags.contains(DESIGN_FLAG_QUERY_V4_OBJECT)) list.append(Text("QUERY_V4_OBJECT"));
  if (flags.contains(DESIGN_FLAG_PRIVATE_STOREDESK)) list.append(Text("PRIVATE_STOREDESK"));
  if (flags.contains(DESIGN_FLAG_PRESERVE)) list.append(Text("PRESERVE"));
  if (flags.contains(DESIGN_FLAG_PRIVATE_1STUSE)) list.append(Text("PRIVATE_1STUSE"));
//  if (flags.contains(DESIGN_FLAG_QUERY_FILTER)) list.append(Text("QUERY_FILTER"));
  if (flags.contains(DESIGN_FLAG_AGENT_SHOWINSEARCH)) list.append(Text("AGENT_SHOWINSEARCH"));
  if (flags.contains(DESIGN_FLAG_REPLACE_SPECIAL)) list.append(Text("REPLACE_SPECIAL"));
  if (flags.contains(DESIGN_FLAG_PROPAGATE_NOCHANGE)) list.append(Text("PROPAGATE_NOCHANGE"));
  if (flags.contains(DESIGN_FLAG_V4BACKGROUND_MACRO)) list.append(Text("V4BACKGROUND_MACRO"));
  if (flags.contains(DESIGN_FLAG_SCRIPTLIB)) list.append(Text("SCRIPTLIB"));
  if (flags.contains(DESIGN_FLAG_VIEW_CATEGORIZED)) list.append(Text("VIEW_CATEGORIZED"));
  if (flags.contains(DESIGN_FLAG_DATABASESCRIPT)) list.append(Text("DATABASESCRIPT"));
  if (flags.contains(DESIGN_FLAG_SUBFORM)) list.append(Text("SUBFORM"));
  if (flags.contains(DESIGN_FLAG_AGENT_RUNASWEBUSER)) list.append(Text("AGENT_RUNASWEBUSER"));
  if (flags.contains(DESIGN_FLAG_AGENT_RUNASINVOKER)) list.append(Text("AGENT_RUNASINVOKER"));
  if (flags.contains(DESIGN_FLAG_PRIVATE_IN_DB)) list.append(Text("PRIVATE_IN_DB"));
  if (flags.contains(DESIGN_FLAG_IMAGE_WELL)) list.append(Text("IMAGE_WELL"));
  if (flags.contains(DESIGN_FLAG_WEBPAGE)) list.append(Text("WEBPAGE"));
//  if (flags.contains(DESIGN_FLAG_HIDE_FROM_WEB)) list.append(Text("HIDE_FROM_WEB"));
  if (flags.contains(DESIGN_FLAG_V4AGENT_DATA)) list.append(Text("V4AGENT_DATA"));
  if (flags.contains(DESIGN_FLAG_SUBFORM_NORENDER)) list.append(Text("SUBFORM_NORENDER"));
//  if (flags.contains(DESIGN_FLAG_NO_MENU)) list.append(Text("NO_MENU"));
  if (flags.contains(DESIGN_FLAG_SACTIONS)) list.append(Text("SACTIONS"));
//      if (flags.contains(DESIGN_FLAG_MULTILINGUAL_PRESERVE_HIDDEN)) list.append(Text("MULTILINGUAL_PRESERVE_HIDDEN"));
  if (flags.contains(DESIGN_FLAG_SERVLET)) list.append(Text("SERVLET"));
  if (flags.contains(DESIGN_FLAG_ACCESSVIEW)) list.append(Text("ACCESSVIEW"));
  if (flags.contains(DESIGN_FLAG_FRAMESET)) list.append(Text("FRAMESET"));
  if (flags.contains(DESIGN_FLAG_MULTILINGUAL_ELEMENT)) list.append(Text("MULTILINGUAL_ELEMENT"));
  if (flags.contains(DESIGN_FLAG_JAVA_RESOURCE)) list.append(Text("JAVA_RESOURCE"));
  if (flags.contains(DESIGN_FLAG_STYLESHEET_RESOURCE)) list.append(Text("STYLESHEET_RESOURCE"));
  if (flags.contains(DESIGN_FLAG_WEBSERVICE)) list.append(Text("WEBSERVICE"));
  if (flags.contains(DESIGN_FLAG_SHARED_COL)) list.append(Text("SHARED_COL"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_MOBILE)) list.append(Text("HIDE_FROM_MOBILE"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_PORTAL)) list.append(Text("HIDE_FROM_PORTAL"));
  if (flags.contains(DESIGN_FLAG_PROPFILE)) list.append(Text("PROPFILE"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_V3)) list.append(Text("HIDE_FROM_V3"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_V4)) list.append(Text("HIDE_FROM_V4"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_V5)) list.append(Text("HIDE_FROM_V5"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_V6)) list.append(Text("HIDE_FROM_V6"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_V7)) list.append(Text("HIDE_FROM_V7"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_V8)) list.append(Text("HIDE_FROM_V8"));
//      if (flags.contains(DESIGN_FLAG_HIDE_FROM_V9)) list.append(Text("HIDE_FROM_V9"));
//      if (flags.contains(DESIGN_FLAG_MUTILINGUAL_HIDE)) list.append(Text("MUTILINGUAL_HIDE"));
  if (flags.contains(DESIGN_FLAG_WEBHYBRIDDB)) list.append(Text("WEBHYBRIDDB"));
  if (flags.contains(DESIGN_FLAG_READONLY)) list.append(Text("READONLY"));
  if (flags.contains(DESIGN_FLAG_NEEDSREFRESH)) list.append(Text("NEEDSREFRESH"));
  if (flags.contains(DESIGN_FLAG_HTMLFILE)) list.append(Text("HTMLFILE"));
  if (flags.contains(DESIGN_FLAG_JSP)) list.append(Text("JSP"));
  if (flags.contains(DESIGN_FLAG_QUERYVIEW)) list.append(Text("QUERYVIEW"));
  if (flags.contains(DESIGN_FLAG_DIRECTORY)) list.append(Text("DIRECTORY"));
  if (flags.contains(DESIGN_FLAG_PRINTFORM)) list.append(Text("PRINTFORM"));
//      if (flags.contains(DESIGN_FLAG_HIDEFROMDESIGNLIST)) list.append(Text("HIDEFROMDESIGNLIST"));
//      if (flags.contains(DESIGN_FLAG_HIDEONLYFROMDESIGNLIST)) list.append(Text("HIDEONLYFROMDESIGNLIST"));
  if (flags.contains(DESIGN_FLAG_COMPOSITE_APP)) list.append(Text("COMPOSITE_APP"));
  if (flags.contains(DESIGN_FLAG_COMPOSITE_DEF)) list.append(Text("COMPOSITE_DEF"));
  if (flags.contains(DESIGN_FLAG_XSP_CC)) list.append(Text("XSP_CC"));
  if (flags.contains(DESIGN_FLAG_JS_SERVER)) list.append(Text("JS_SERVER"));
  if (flags.contains(DESIGN_FLAG_STYLEKIT)) list.append(Text("STYLEKIT"));
  if (flags.contains(DESIGN_FLAG_WIDGET)) list.append(Text("WIDGET"));
  if (flags.contains(DESIGN_FLAG_JAVAFILE)) list.append(Text("JAVAFILE"));
  return list;
}

NTLX_CORE_END
