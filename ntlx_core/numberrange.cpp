﻿#include "numberrange.h"

NTLX_CORE_BEGIN

NumberPair::NumberPair()
  : Pair<Number, NUMBER_PAIR, NFMT>()
{
}

NumberPair::NumberPair(const NUMBER_PAIR* pData, int size)
  : Pair<Number, NUMBER_PAIR, NFMT>(pData, size)
{
}

NumberPair::NumberPair(const NUMBER_PAIR& data)
  : Pair<Number, NUMBER_PAIR, NFMT>(data)
{
}

NumberPair::NumberPair(const NumberPair& other)
  : Pair<Number, NUMBER_PAIR, NFMT>(other)
{
}

NumberPair& NumberPair::operator =(const NumberPair& other)
{
  if (this != &other)
    Pair<Number, NUMBER_PAIR, NFMT>::operator =(other);
  return *this;
}

NumberPair::~NumberPair()
{
}

Text NumberPair::toText(
    const INTLFORMAT* pIntlFormat
    , const NFMT* pTextFormat
    ) const
{
  return Number(value_.Lower).toText(pIntlFormat, pTextFormat)
      + Text(" - ")
      + Number(value_.Upper).toText(pIntlFormat, pTextFormat)
      ;
}

NumberRange::NumberRange()
  : Range<Number, NumberPair, NUMBER, NUMBER_PAIR, NFMT>()
{
}

NumberRange::NumberRange(const char* pBin, int size)
  : Range<Number, NumberPair, NUMBER, NUMBER_PAIR, NFMT>(pBin, size)
{
}

NumberRange::NumberRange(const NumberRange& other)
  : Range<Number, NumberPair, NUMBER, NUMBER_PAIR, NFMT>(other)
{
}

NumberRange& NumberRange::operator =(const NumberRange& other)
{
  if (this != &other)
    Range<Number, NumberPair, NUMBER, NUMBER_PAIR, NFMT>::operator =(other);
  return *this;
}

NumberRange::~NumberRange()
{
}

NTLX_CORE_END
