﻿#ifndef NTLX_CORE_DIR_H
#define NTLX_CORE_DIR_H

#include <ntlx_core/file.h>
#include <ntlx_core/searchiterator.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osfile.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class FileInfo;
#ifndef FileInfoList
using FileInfoList = QList<FileInfo>;
#endif

class NTLX_CORESHARED_EXPORT Dir
    : public File
{
public:

  explicit Dir(const FilePath& path, bool withOpen = true);

  virtual ~Dir();

  virtual Dir& listUp(
      SearchIteratorInterface* iterator
      , WORD searchFlags = SEARCH_SUMMARY | SEARCH_FILETYPE
      , WORD noteClassMask = FILE_DBDESIGN | FILE_DIRS | FILE_NOUPDIRS
      );
};

NTLX_CORE_END

#endif // NTLX_CORE_DIR_H
