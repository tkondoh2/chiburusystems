﻿#ifndef NTLX_CORE_ITEMTABLE_H
#define NTLX_CORE_ITEMTABLE_H

#include <ntlx_core/ntlx_core_global.h>
#include <ntlx_core/text.h>
#include <QVariant>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <stdnames.h>
#include <nsfdata.h>
#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT ItemTable
{
public:
  ItemTable();

  ItemTable(SEARCH_MATCH* pMatch, ITEM_TABLE* pTable);

  ItemTable(const ItemTable& other);

  ItemTable& operator =(const ItemTable& other);

  virtual ~ItemTable();

  const SEARCH_MATCH& match() const;

  const QVariantMap& map() const;

  Text getText(const QString& key) const;

  QVariant operator [](const QString& key) const;

private:
  SEARCH_MATCH match_;
  QVariantMap* pMap_;
};

using ItemTableList = QList<ItemTable>;

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::ItemTable)

#endif // NTLX_CORE_ITEMTABLE_H
