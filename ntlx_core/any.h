﻿#ifndef NTLX_CORE_ANY_H
#define NTLX_CORE_ANY_H

#include <ntlx_core/ntlx_core_global.h>
#include <QByteArray>
#include <QVariant>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <pool.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class Text;
class TextList;
class Number;
class NumberRange;
class TimeDate;
class TimeDateRange;

class NTLX_CORESHARED_EXPORT Any
{
public:
  Any();

  Any(WORD type, const char* value);

  Any(WORD type, const char* value, int size);

  Any(BLOCKID& bidItem
      , WORD dataType
      , BLOCKID& bidValue
      , DWORD valueLength
      );

  Any(const Any& other);

  Any& operator =(const Any& other);

  virtual ~Any();

  WORD type() const { return type_; }

  bool isValid() const;

  int size() const { return value_.size(); }

  const char* constData() const { return value_.constData(); }

  char* data() { return value_.data(); }

  char operator [](int) const;

  Text getText() const;

  TextList getTextList() const;

  QString typeName() const { return typeName(type_); }

  static QString typeName(WORD type);

  template <WORD t, class T>
  T extract() const
  {
    if (type_ != t)
      return T();
    return T(constData(), size());
  }

  Text toText() const;
  Number toNumber() const;
  TimeDate toTimeDate() const;
  TextList toTextList() const;
  NumberRange toNumberRange() const;
  TimeDateRange toTimeDateRange() const;

  template <typename T>
  static Any fromBinary(void* ptr, T length)
  {
    WORD* pType = (WORD*)ptr;
    char* pValue = reinterpret_cast<char*>(pType + 1);
    int len = static_cast<int>(length - sizeof(WORD));
    return Any(*pType, pValue, len);
  }

private:
  WORD type_;
  QByteArray value_;
};

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::Any)

#endif // NTLX_CORE_ANY_H
