﻿#include "thread.h"

NTLX_CORE_BEGIN

Thread::Thread()
  : Resultful()
{
  lastResult_ = NotesInitThread();
}

Thread::~Thread()
{
  NotesTermThread();
}

NTLX_CORE_END
