﻿#ifndef NTLX_CORE_TIMEDATERANGE_H
#define NTLX_CORE_TIMEDATERANGE_H

#include <ntlx_core/timedate.h>
#include <ntlx_core/range.h>

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT TimeDatePair
    : public Pair<TimeDate, TIMEDATE_PAIR, TFMT>
{
public:
  TimeDatePair();

  TimeDatePair(const TIMEDATE_PAIR* pData, int size);

  TimeDatePair(const TIMEDATE_PAIR& data);

  TimeDatePair(const TimeDatePair& other);

  TimeDatePair& operator =(const TimeDatePair& other);

  virtual ~TimeDatePair();

  Text toText(
      const INTLFORMAT* pIntlFormat = nullptr
      , const TFMT* pTextFormat = nullptr
      ) const override;
};

class NTLX_CORESHARED_EXPORT TimeDateRange
    : public Range<TimeDate, TimeDatePair, TIMEDATE, TIMEDATE_PAIR, TFMT>
{
public:
  TimeDateRange();

  TimeDateRange(const char* pBin, int size);

  TimeDateRange(const TimeDateRange& other);

  TimeDateRange& operator =(const TimeDateRange& other);

  virtual ~TimeDateRange();
};

NTLX_CORE_END

Q_DECLARE_METATYPE(ntlx::TimeDatePair)
Q_DECLARE_METATYPE(ntlx::TimeDateRange)

#endif // NTLX_CORE_TIMEDATERANGE_H
