﻿#include "environment.h"
#include "text.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osenv.h>
#include <osfile.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

Environment::Environment()
  : Resultful()
{
}

Text Environment::value(const Text &name)
{
  char buffer[MAXENVVALUE];
  if (!OSGetEnvironmentString(name.constData(), buffer, MAXENVVALUE - 1))
    return Text();

  return Text(buffer);
}

void Environment::setValue(const Text& name, const Text& value)
{
  OSSetEnvironmentVariable(name.constData(), value.constData());
}

Text Environment::dataDirectory()
{
  char buffer[MAXPATH];
  WORD len = OSGetDataDirectory(buffer);
  return Text(buffer, len);
}

NTLX_CORE_END
