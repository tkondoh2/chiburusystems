﻿#ifndef NTLX_CORE_COMPUTEDFORMULA_H
#define NTLX_CORE_COMPUTEDFORMULA_H

#include <ntlx_core/resultful.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfsearc.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class LockedFormula;
class Any;

class NTLX_CORESHARED_EXPORT ComputedFormula
    : public Resultful
{
public:
  explicit ComputedFormula(LockedFormula& lockedFormula);
  virtual ~ComputedFormula();
  operator HCOMPUTE() const { return handle_; }
  Any evaluate(NOTEHANDLE hNote = NULLHANDLE);

private:
  LockedFormula* pLockedFormula_;
  HCOMPUTE handle_;

  ComputedFormula(const ComputedFormula&);
  ComputedFormula& operator =(const ComputedFormula&);
};

NTLX_CORE_END

#endif // NTLX_CORE_COMPUTEDFORMULA_H
