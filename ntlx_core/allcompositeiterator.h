﻿#ifndef NTLX_CORE_ALLCOMPOSITEITERATOR_H
#define NTLX_CORE_ALLCOMPOSITEITERATOR_H

#include <ntlx_core/iteminfoiterator.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfnote.h>
#include <ods.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class CdRecordIteratorInterface;

class AllCompositeIterator
    : public ItemInfoIteratorInterface
{
public:
  AllCompositeIterator(CdRecordIteratorInterface* pCdrIt)
    : pCdrIt_(pCdrIt)
  {
  }

  virtual void operator ()(
      BLOCKID& /*bidItem*/
      , WORD dataType
      , BLOCKID& bidValue
      , DWORD valueLength
      ) override
  {
    if (dataType != TYPE_COMPOSITE)
      return;

    EnumCompositeBuffer(
          bidValue
          , valueLength
          , callback
          , pCdrIt_
          );
  }

  static STATUS LNPUBLIC callback(
      char* RecordPtr
      , WORD RecordType
      , DWORD RecordLength
      , void* vContext
      );

private:
  CdRecordIteratorInterface* pCdrIt_;
};

NTLX_CORE_END

#endif // NTLX_CORE_ALLCOMPOSITEITERATOR_H
