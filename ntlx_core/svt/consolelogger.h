﻿#ifndef NTLX_CORE_SVT_CONSOLELOGGER_H
#define NTLX_CORE_SVT_CONSOLELOGGER_H

#include <ntlx_core/svt/logmessagetext.h>
#include <ntlx_core/text.h>
#include <ntlx_core/loglevel.h>
#include <QString>

NTLX_CORE_BEGIN

namespace svt {

class ConsoleLog;

class NTLX_CORESHARED_EXPORT ConsoleLogger
{
public:

  static ConsoleLogger &instance();

  const QString& logFormat() const;

  const LogLevel& logLevel() const;

  void setLogFormat(const QString& format);

  void setLogLevel(LogLevel level);

  friend ConsoleLog logFatal(STATUS code = NOERROR);

  friend ConsoleLog logError(STATUS code = NOERROR);

  friend ConsoleLog logWarn(STATUS code = NOERROR);

  friend ConsoleLog logInfo(STATUS code = NOERROR);

  friend ConsoleLog logDebug(STATUS code = NOERROR);

  friend ConsoleLog logTrace(STATUS code = NOERROR);

private:
  ConsoleLog logging(LogLevel level, STATUS code) const;

  ConsoleLogger();

  ~ConsoleLogger();

  ConsoleLogger(const ConsoleLogger &);

  ConsoleLogger &operator =(const ConsoleLogger &);

private:
  QString logFormat_;
  LogLevel logLevel_;

};


inline const QString& ConsoleLogger::logFormat() const
{
  return logFormat_;
}

inline const ntlx::LogLevel& ConsoleLogger::logLevel() const
{
  return logLevel_;
}

inline void ConsoleLogger::setLogFormat(const QString& format)
{
  logFormat_ = format;
}

inline void ConsoleLogger::setLogLevel(LogLevel level)
{
  logLevel_ = level;
}



class NTLX_CORESHARED_EXPORT ConsoleLog
{
public:
  ConsoleLog(
      ntlx::LogLevel level = ntlx::LogLevel::None
      , STATUS code = NOERROR
      );

  template <typename ... Args>
  void operator ()(const QString& bodyFormat, Args const & ... args) const
  {
    if (level_ == ntlx::LogLevel::None)
      return;

    QString text(ConsoleLogger::instance().logFormat());
    text.replace("{{level}}", ntlx::logLevelText(level_));
    text.replace("{{body}}", bodyFormat);
    Text lmbcsText = Text::fromQString(text);
    LogMessageText(this->code_)(lmbcsText.constData(), args ...);
  }

private:
  ntlx::LogLevel level_;
  STATUS code_;
};



inline ConsoleLog logFatal(STATUS code)
{
  return ConsoleLogger::instance().logging(LogLevel::Fatal, code);
}

inline ConsoleLog logError(STATUS code)
{
  return ConsoleLogger::instance().logging(LogLevel::Error, code);
}

inline ConsoleLog logWarn(STATUS code)
{
  return ConsoleLogger::instance().logging(LogLevel::Warn, code);
}

inline ConsoleLog logInfo(STATUS code)
{
  return ConsoleLogger::instance().logging(LogLevel::Info, code);
}

inline ConsoleLog logDebug(STATUS code)
{
  return ConsoleLogger::instance().logging(LogLevel::Debug, code);
}

inline ConsoleLog logTrace(STATUS code)
{
  return ConsoleLogger::instance().logging(LogLevel::Trace, code);
}


} // namespace svt
NTLX_CORE_END

#endif // NTLX_CORE_SVT_CONSOLELOGGER_H
