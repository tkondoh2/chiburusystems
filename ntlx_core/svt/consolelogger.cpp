﻿#include "consolelogger.h"
#include <QMap>

NTLX_CORE_BEGIN

namespace svt {

ConsoleLogger &ConsoleLogger::instance()
{
  static ConsoleLogger instance_;
  return instance_;
}

ConsoleLog ConsoleLogger::logging(ntlx::LogLevel level, STATUS code) const
{
  if (level <= logLevel_)
  {
    return ConsoleLog(level, code);
  }
  return ConsoleLog();
}

ConsoleLog::ConsoleLog(ntlx::LogLevel level, STATUS code)
  : level_(level)
  , code_(code)
{
}

ConsoleLogger::ConsoleLogger()
  : logFormat_("[{{level}}] {{body}}")
  , logLevel_(LogLevel::Info)
{
}

ConsoleLogger::~ConsoleLogger()
{
}

} // namespace svt
NTLX_CORE_END
