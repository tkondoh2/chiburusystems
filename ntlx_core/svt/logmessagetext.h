﻿#ifndef NTLX_CORE_SVT_LOGMESSAGETEXT_H
#define NTLX_CORE_SVT_LOGMESSAGETEXT_H

#include <ntlx_core/ntlx_core_global.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <global.h>
#include <addin.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN
namespace svt {

class NTLX_CORESHARED_EXPORT LogMessageText
{
public:
  LogMessageText(STATUS code = NOERROR);

  template <typename ... Args>
  void operator ()(const char* msg, Args const & ... args)
  {
    AddInLogMessageText(const_cast<char*>(msg), code_, args ...);
  }

private:
  STATUS code_;
};

} // namespace svt
NTLX_CORE_END

#endif // NTLX_CORE_SVT_LOGMESSAGETEXT_H
