﻿#include <ntlx_core/fileinfo.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfdb.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

FileInfo::FileInfo()
  : ItemTable()
{
}

FileInfo::FileInfo(SEARCH_MATCH* pMatch, ITEM_TABLE* pTable)
  : ItemTable(pMatch, pTable)
{
}

FileInfo::FileInfo(const FileInfo& other)
  : ItemTable(other)
{
}

FileInfo& FileInfo::operator =(const FileInfo& other)
{
  if (this != &other)
  {
    ItemTable::operator =(other);
  }
  return *this;
}

FileInfo::~FileInfo()
{
}

Text FileInfo::path() const { return getText(DBDIR_PATH_ITEM); }
Text FileInfo::info() const { return getText(DBDIR_INFO_ITEM); }
Text FileInfo::type() const { return getText(DBDIR_TYPE_ITEM); }

//Text FileInfo::title() const { return getInfo(INFOPARSE_TITLE); }
//Text FileInfo::categories() const { return getInfo(INFOPARSE_CATEGORIES); }
//Text FileInfo::templateName() const { return getInfo(INFOPARSE_CLASS); }
//Text FileInfo::inheritedTemplateName() const { return getInfo(INFOPARSE_DESIGN_CLASS); }

Text FileInfo::getInfo(WORD what) const
{
  Text info = getText(DBDIR_INFO_ITEM);
  char part[NSF_INFO_SIZE];
  NSFDbInfoParse(info.data(), what, part, NSF_INFO_SIZE - 1);
  part[NSF_INFO_SIZE - 1] = '\0';
  return Text(part);
}

NTLX_CORE_END
