﻿#ifndef NTLX_CORE_THREAD_H
#define NTLX_CORE_THREAD_H

#include <ntlx_core/ntlx_core_global.h>
#include <ntlx_core/resultful.h>

NTLX_CORE_BEGIN

class NTLX_CORESHARED_EXPORT Thread
    : public Resultful
{
public:
  explicit Thread();

  virtual ~Thread();
};

NTLX_CORE_END

#endif // NTLX_CORE_THREAD_H
