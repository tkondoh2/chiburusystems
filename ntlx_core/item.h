﻿#ifndef NTLX_CORE_ITEM_H
#define NTLX_CORE_ITEM_H

#include <ntlx_core/resultful.h>
#include <ntlx_core/any.h>
#include <ntlx_core/text.h>

NTLX_CORE_BEGIN

class Note;

class CdRecord;

class NTLX_CORESHARED_EXPORT Item
    : public Resultful
{
public:

  Item();
  Item(const Text& name, WORD flags = 0);
  Item(
      WORD ItemFlags
      , char *Name
      , WORD NameLength
      , void *Value
      , DWORD ValueLength
      );
  Item(const Item& other);
  Item& operator =(const Item& other);
  virtual ~Item();

  void append(const Any& value);
//  void addValue(const Any& value);
  WORD type() const;
  QString typeName() const;
  int valueCount() const;
  Any valueAt(int i) const;
  Any value() const;
  Text name() const;
  WORD flags() const;
  TextList flagNames() const;

  const Item& getAllCdRecords(const Note& note, QList<CdRecord>& cdrecords) const;

private:
  Text name_;
  WORD flags_;
  QList<Any> values_;
};

NTLX_CORE_END

#endif // NTLX_CORE_ITEM_H
