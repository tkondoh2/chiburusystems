﻿#include "allcompositeiterator.h"
#include "cdrecorditerator.h"
#include "item.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <nsfnote.h>
#include <ods.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

STATUS LNPUBLIC AllCompositeIterator::callback(
    char* RecordPtr
    , WORD RecordType
    , DWORD RecordLength
    , void* vContext
    )
{
  CdRecordIteratorInterface* iterator
      = reinterpret_cast<CdRecordIteratorInterface*>(vContext);
  (*iterator)(RecordPtr, RecordType, RecordLength);
  return NOERROR;
}

NTLX_CORE_END
