﻿#include "formula.h"
#include "any.h"
#include "lockedformula.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

#include <QtDebug>

NTLX_CORE_BEGIN

Formula::CompileError::CompileError()
  : status_(NOERROR)
  , line_(0)
  , column_(0)
  , offset_(0)
  , length_(0)
{
}

Formula::Formula()
  : Resultful()
  , handle_(NULLHANDLE)
{
}

Formula::Formula(const Text& source)
  : Resultful()
  , handle_(NULLHANDLE)
{
  compile(source);
}

Formula::Formula(const Formula& other)
  : Resultful()
  , handle_(NULLHANDLE)
{
  LockedFormula locked(const_cast<Formula&>(other));
  assign(locked.pointer(), other.odsLength());
}

Formula& Formula::operator =(const Formula& other)
{
  if (this != &other)
  {
    LockedFormula locked(const_cast<Formula&>(other));
    assign(locked.pointer(), other.odsLength());
  }
  return *this;
}

Formula::~Formula()
{
  free();
}

WORD Formula::odsLength() const
{
  WORD len = 0;
  if (handle_ != NULLHANDLE)
    lastResult_ = NSFFormulaGetSize(handle_, &len);
  return len;
}

Formula& Formula::compile(const Text& source, const Text& viewColumnName)
{
  free();
//  qDebug() << "Formula source." << source.toQString();

  lastResult_ = NSFFormulaCompile(
        viewColumnName.isEmpty()
          ? nullptr
          : const_cast<char*>(viewColumnName.constData())
        , viewColumnName.byteSize()
        , source.constData()
        , source.byteSize()
        , &handle_
        , &length_
        , &compileError_.status_
        , &compileError_.line_
        , &compileError_.column_
        , &compileError_.offset_
        , &compileError_.length_
        );
  return *this;
}

void Formula::free()
{
  if (handle_)
  {
    OSMemFree(handle_);
    handle_ = NULLHANDLE;
  }
}

void Formula::assign(const char* binary, WORD len)
{
  free();
  if (len > 0)
  {
    lastResult_ = OSMemAlloc(0, static_cast<DWORD>(len), &handle_);
    LockedFormula locked(*this);
    memcpy(const_cast<char*>(locked.pointer()), binary, len);
  }
}


FormulaStatement::FormulaStatement(const Text &statement)
  : statement_(statement)
  , params_()
{
}

void FormulaStatement::setParam(const Text& placeholder, const Text& replaced)
{
  params_.insert(placeholder, replaced);
}

Text FormulaStatement::replacedText() const
{
  Text s(statement_);
  foreach (Text key, params_.keys())
  {
    s.replace(key, escaped(params_.value(key)));
  }
  return s;
}

Text FormulaStatement::escaped(const Text& param)
{
  Text s(param);
  s.replace("\"", "\\\"");
  return s;
}

NTLX_CORE_END
