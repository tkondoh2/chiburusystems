﻿#include <ntlx_core/distinguishedname.h>
#include <QString>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <names.h>
#include <dname.h>
#include <kfm.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

DName::DistinguishedName()
  : Resultful()
  , value_()
{
}

DName::DistinguishedName(const Text& name)
  : Resultful()
  , value_()
{
  lastResult_ = canonicalize(name, value_);
}

Text DName::canonical() const
{
  return value_;
}

Text DName::abbreviated() const
{
  Text newValue;
  abbreviate(value_, newValue);
  return newValue;
}

QString DName::toQString() const
{
  return value_.toQString();
}

ApiResult DName::canonicalize(const Text& before, Text& after)
{
  char buffer[MAXUSERNAME];
  WORD len;
  ApiResult result = DNCanonicalize(
        (DWORD)0L
        , nullptr
        , before.constData()
        , buffer
        , MAXUSERNAME
        , &len
        );
  if (result.noError())
    after = Text(buffer, len);
  else
    after = before;
  return result;
}

ApiResult DName::abbreviate(const Text& before, Text& after)
{
  char buffer[MAXUSERNAME];
  WORD len;
  ApiResult result = DNAbbreviate(
        (DWORD)0L
        , nullptr
        , before.constData()
        , buffer
        , MAXUSERNAME
        , &len
        );
  if (result.noError())
    after = Text(buffer, len);
  else
    after = before;
  return result;
}

DName DName::currentUserName(ApiResult* result)
{
  char buffer[MAXUSERNAME + 1];
  ApiResult ret = SECKFMGetUserName(buffer);
  assign(result, ret);
  if (ret.noError())
    return DName(buffer);
  return DName();
}

Text DName::orgName() const
{
  DN_COMPONENTS dn;
  lastResult_ = DNParse(0, nullptr, value_.constData(), &dn, sizeof(dn));
  if (lastResult().hasError())
    return Text();

  return Text(dn.O, dn.OLength);
}

NTLX_CORE_END
