﻿#ifndef NTLX_CORE_SERVERITERATOR_H
#define NTLX_CORE_SERVERITERATOR_H

#include <ntlx_core/text.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <ns.h>
#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

template <class ItemClass, class ListClass>
class ServerIterator
    : public Resultful
{
public:
  ServerIterator(ListClass* pList)
    : Resultful()
    , pList_(pList)
  {
  }

  virtual ServerIterator<ItemClass, ListClass>& operator ()(
      const Text& port = Text()
      )
  {
    // サーバリストを取得する。
    DHANDLE handle = NULLHANDLE;

    lastResult_ = NSGetServerList(
          port.isEmpty() ? nullptr : const_cast<char*>(port.constData())
          , &handle
          );
    if (lastResult_.hasError())
      return *this;

    Q_ASSERT(handle);

    WORD* pSize = reinterpret_cast<WORD*>(OSLockObject(handle));
    WORD* pLen = pSize + 1;
    char* pName = reinterpret_cast<char*>(pLen + *pSize);
    for (WORD i = 0; i < *pSize; ++i)
    {
      Text name(pName, *pLen);
      pList_->append(ItemClass(name));
      pName += *pLen++;
    }

    OSUnlockObject(handle);
    OSMemFree(handle);

    return *this;
  }

private:
  ListClass* pList_;
};

NTLX_CORE_END

#endif // NTLX_CORE_SERVERITERATOR_H
