﻿#include <ntlx_core/textlist.h>

NTLX_CORE_BEGIN

TextList::TextList()
  : list_()
{
}

TextList::TextList(const char* pData, int)
  : list_()
{
  LIST* pList = reinterpret_cast<LIST*>(const_cast<char*>(pData));
  USHORT* pCount = reinterpret_cast<USHORT*>(pList + 1);
  char* pStr = reinterpret_cast<char*>(pCount + pList->ListEntries);
  for (USHORT index = 0; index < pList->ListEntries; ++index, ++pCount)
  {
    Text text(pStr, *pCount);
    list_.append(text);
    pStr += *pCount;
  }
}

TextList::TextList(const TextList& other)
  : list_(other.list_)
{
}

TextList::~TextList()
{
}

TextList& TextList::operator =(const TextList& other)
{
  if (this != &other)
    list_ = (other.list_);
  return *this;
}

QStringList TextList::toQStringList() const
{
  QStringList qlist;
  foreach (Text text, list_)
  {
    qlist << text.toQString();
  }
  return qlist;
}

bool TextList::isEmpty() const
{
  return list_.isEmpty();
}

void TextList::append(const Text& item)
{
  list_.append(item);
}

const Text& TextList::at(int index) const
{
  return list_.at(index);
}

NTLX_CORE_END
