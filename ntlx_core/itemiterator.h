﻿#ifndef NTLX_CORE_ITEMITERATOR_H
#define NTLX_CORE_ITEMITERATOR_H

#include <ntlx_core/item.h>

#if defined(NT)
#pragma pack(push, 1)
#endif

//#include <global.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

class Text;
class Any;

class ItemIteratorInterface
{
public:
  virtual STATUS operator ()(
      WORD ItemFlags
      , char *Name
      , WORD NameLength
      , void *Value
      , DWORD ValueLength
      ) = 0;
};

template <class ItemClass, class ListClass>
class ItemIterator
    : public ItemIteratorInterface
{
public:
  ItemIterator(ListClass* pList)
    : pList_(pList)
  {
  }

  virtual STATUS operator ()(
      WORD ItemFlags
      , char *Name
      , WORD NameLength
      , void *Value
      , DWORD ValueLength
      )
  {
    pList_->append(ItemClass(ItemFlags, Name, NameLength, Value, ValueLength));
    return NOERROR;
  }

private:
  ListClass* pList_;
};

NTLX_CORE_END

#endif // NTLX_CORE_ITEMITERATOR_H
