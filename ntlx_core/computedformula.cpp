﻿#include "computedformula.h"
#include "lockedformula.h"
#include "any.h"

#if defined(NT)
#pragma pack(push, 1)
#endif

#include <osmem.h>

#if defined(NT)
#pragma pack(pop)
#endif

NTLX_CORE_BEGIN

ComputedFormula::ComputedFormula(LockedFormula& lockedFormula)
  : Resultful()
  , pLockedFormula_(&lockedFormula)
  , handle_(NULLHANDLE)
{
  if (pLockedFormula_->pointer() == nullptr)
    return;

  lastResult_ = NSFComputeStart(0, const_cast<char*>(pLockedFormula_->pointer()), &handle_);
}

ComputedFormula::~ComputedFormula()
{
  if (handle_ != NULLHANDLE)
    NSFComputeStop(handle_);
}

Any ComputedFormula::evaluate(NOTEHANDLE hNote)
{
  Q_ASSERT(handle_);
  DHANDLE hResult = NULLHANDLE;
  WORD resultLen = 0;
  lastResult_ = NSFComputeEvaluate(handle_, hNote, &hResult, &resultLen, 0, 0, 0);
  if (lastResult_.hasError())
    return Any();

  char* pResult = static_cast<char*>(OSLockObject(hResult));
  WORD* pType = reinterpret_cast<WORD*>(pResult);
  resultLen -= sizeof(WORD);
  Any result(*pType, pResult + sizeof(WORD), resultLen);
  OSUnlockObject(hResult);
  OSMemFree(hResult);
  return result;
}

NTLX_CORE_END
