TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
    qx_core \
    ntlx_core \
    NotesTreeViewer \
    NotesIniEditor \
    DSAPILogger
