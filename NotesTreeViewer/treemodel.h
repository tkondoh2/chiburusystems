﻿#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QStandardItemModel>

/**
 * @brief ツリーモデルクラス
 */
class TreeModel
    : public QStandardItemModel
{
  Q_OBJECT
public:
  /**
   * @brief デフォルトコンストラクタ
   * @param parent 親オブジェクトへのポインタ
   */
  explicit TreeModel(QObject* parent = 0);

  template <class T>
  QStandardItem* addItem(
      const T& data
      , const QString& title
      , QStandardItem* parent = Q_NULLPTR
      )
  {
    QStandardItem* item = new QStandardItem(title);
    item->setData(QVariant::fromValue<T>(data));
    if (parent)
      parent->appendRow(item);
    else
      appendRow(item);
    return item;
  }

  void showProperties(const QModelIndex& index);

signals:
  void writeProperties(const QVariant& properties);

public slots:
};

#endif // TREEMODEL_H
