#-------------------------------------------------
#
# Project created by QtCreator 2018-04-12T17:14:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NotesTreeViewer
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH -= $$PWD
INCLUDEPATH += $$PWD/..

DEPENDPATH -= $$PWD
DEPENDPATH += $$PWD/..

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    treemodel.cpp \
    treeitem.cpp \
    item/cditem.cpp \
    item/dbitem.cpp \
    item/diritem.cpp \
    item/fileitem.cpp \
    item/itemitem.cpp \
    item/noteitem.cpp \
    item/serverlistitem.cpp

HEADERS += \
        mainwindow.h \
    treemodel.h \
    treeitem.h \
    item/cditem.h \
    item/dbitem.h \
    item/diritem.h \
    item/fileitem.h \
    item/itemitem.h \
    item/noteitem.h \
    item/serverlistitem.h

FORMS += \
        mainwindow.ui

LIBS += -lntlx_core
win32:CONFIG(debug,release|debug): LIBS += -L$$OUT_PWD/../ntlx_core/debug
else:win32:CONFIG(release,release|debug): LIBS += -L$$OUT_PWD/../ntlx_core/release
else:macx: LIBS += -L$$OUT_PWD/../ntlx_core

#
# Notes API用マクロ定義
#
win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
}
else:unix {
    DEFINES += UNIX LINUX W32
    QMAKE_CXXFLAGS += -std=c++0x
    target.path = /usr/lib
    INSTALLS += target
}

LIBS += -lnotes

#
# Windows/32
# "INCLUDEPATH+=C:/Users/Public/notesapi/include" "LIBS+=-LC:/Users/Public/notesapi/lib/mswin32"
#

#
# Windows/64
# "INCLUDEPATH+=C:/Users/Public/notesapi/include" "LIBS+=-LC:/Users/Public/notesapi/lib/mswin64"
#

#
# MacOS
# "INCLUDEPATH+=/Users/Shared/notesapi/include" "LIBS+=-L'/Applications/IBM Notes.app/Contents/MacOS'"
#
