﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <ntlx_core/any.h>
#include <ntlx_core/numberrange.h>
#include <ntlx_core/timedaterange.h>

#include <QTimer>
#include "item/diritem.h"
#include "item/serverlistitem.h"

#include <QtDebug>

MainWindow::MainWindow(QWidget* parent)
  : QMainWindow(parent)
  , ui_(new Ui::MainWindow)
  , model_()
  , properties_()
{
  // UIクラスのセットアップ
  ui_->setupUi(this);

  initialize();
}

MainWindow::~MainWindow()
{
  // UIの削除
  delete ui_;
}

void MainWindow::initialize()
{
  // ビューにモデルを関連付ける。
  ui_->treeView->setModel(&model_);

  ui_->treeView->setContextMenuPolicy(Qt::CustomContextMenu);

  properties_.setHorizontalHeaderLabels(
        QStringList()
        << tr("Name")
        << tr("Type")
        << tr("Value")
        );
  ui_->tableView->setModel(&properties_);

  connect(ui_->treeView, &QTreeView::clicked
          , this, &MainWindow::showProperties
          );

  connect(&model_, &TreeModel::writeProperties
          , this, &MainWindow::writeProperties
          );

  QTimer::singleShot(0, this, SLOT(addLocalItem()));
  QTimer::singleShot(0, this, SLOT(addServersItem()));

  // ツリービューのコンテキストメニュースロットをcontextMenuスロットに接続
  connect(ui_->treeView, &QTreeView::customContextMenuRequested
          , this, &MainWindow::contextMenu
          );
}

void MainWindow::showProperties(const QModelIndex &index)
{
  properties_.removeRows(0, properties_.rowCount());
  model_.showProperties(index);
  ui_->tableView->resizeColumnsToContents();
  ui_->tableView->resizeRowsToContents();
}

QList<QStandardItem*>& operator <<(QList<QStandardItem*>& lhs, const QString& rhs)
{
  lhs.append(new QStandardItem(rhs));
  return lhs;
}

template <class T, WORD type>
bool outAnySingle(const ntlx::Any& any, const QString& typeName, QList<QStandardItem*>& row)
{
  if (any.type() == type)
  {
    T val = any.extract<type, T>();
    row << typeName << val.toQString();
    return true;
  }
  return false;
}

template <class T, WORD type>
bool outAnyList(const ntlx::Any& any, const QString& typeName, QList<QStandardItem*>& row)
{
  if (any.type() == type)
  {
    T val = any.extract<type, T>();
    row << typeName << val.toQStringList().join(",");
    return true;
  }
  return false;
}

template <class T>
bool outSingle(const QVariant& value, const QString& typeName, QList<QStandardItem*>& row)
{
  if (value.canConvert<T>())
  {
    T v = value.value<T>();
    row << typeName << v.toQString();
    return true;
  }
  else
    return false;
}

template <class T>
bool outList(const QVariant& value, const QString& typeName, QList<QStandardItem*>& row)
{
  if (value.canConvert<T>())
  {
    T v = value.value<T>();
    row << typeName << v.toQStringList().join(",");
    return true;
  }
  else
    return false;
}

void MainWindow::writeProperties(const QVariant& data)
{
  QVariantMap map;
  if (data.canConvert<QVariantMap>())
    map = data.toMap();

  foreach (QString key, map.keys())
  {
    QList<QStandardItem*> row;
    row << new QStandardItem(key);

    QVariant value = map.value(key);

    if (value.canConvert<ntlx::Any>())
    {
      ntlx::Any any = value.value<ntlx::Any>();

      if (outAnySingle<ntlx::Text, TYPE_TEXT>(any, tr("ntlx::Text"), row)) {}
      else if (outAnySingle<ntlx::Number, TYPE_NUMBER>(any, tr("ntlx::Number"), row)) {}
      else if (outAnySingle<ntlx::TimeDate, TYPE_TIME>(any, tr("ntlx::TimeDate"), row)) {}
      else if (outAnyList<ntlx::TextList, TYPE_TEXT_LIST>(any, tr("ntlx::TextList"), row)) {}
      else if (outAnyList<ntlx::NumberRange, TYPE_NUMBER_RANGE>(any, tr("ntlx::NumberRange"), row)) {}
      else if (outAnyList<ntlx::TimeDateRange, TYPE_TIME_RANGE>(any, tr("ntlx::TimeDateRange"), row)) {}
      else
      {
        QStringList list;
        for (int i = 0; i < 32 && i < any.size(); ++i)
        {
          list << QString("%1").arg(static_cast<uint>(any[i]), 2, 16, QChar('0')).right(2);
        }
        row << tr("ntlx::Any(%1)").arg(any.typeName())
            << tr("size:%1, %2").arg(any.size()).arg(list.join(" "));
      }
    }
    else if (outSingle<ntlx::Text>(value, tr("ntlx::Text"), row)) {}
    else if (outSingle<ntlx::Number>(value, tr("ntlx::Number"), row)) {}
    else if (outSingle<ntlx::TimeDate>(value, tr("ntlx::TimeDate"), row)) {}
    else if (outList<ntlx::TextList>(value, tr("ntlx::TextList"), row)) {}
    else if (outList<ntlx::NumberRange>(value, tr("ntlx::NumberRange"), row)) {}
    else if (outList<ntlx::TimeDateRange>(value, tr("ntlx::TimeDateRange"), row)) {}
    else
    {
      if (value.canConvert<QString>())
      {
        row << new QStandardItem(value.typeName())
            << new QStandardItem(value.toString());
      }
      else
      {
        qDebug() << value.typeName() << value.toString();
      }
    }
    properties_.appendRow(row);
  }
}

void MainWindow::addLocalItem()
{
  model_.appendRow(new DirItem(ntlx::FilePath(), tr("Local")));
}

void MainWindow::addServersItem()
{
  model_.appendRow(new ServerListItem(tr("Servers")));
}

void MainWindow::expand()
{
  QAction* action = qobject_cast<QAction*>(sender());
  QVariant actionData = action->data();
  QModelIndex index = actionData.toModelIndex();
  if (!index.isValid())
    return;

  if (TreeItem* pItem = reinterpret_cast<TreeItem*>(
        model_.itemFromIndex(index)
        ))
  {
    pItem->removeRows(0, pItem->rowCount());
    pItem->expand();
  }
}

void MainWindow::contextMenu(const QPoint& pos)
{
  // マウスポジションからモデルインデックスを取得
  QModelIndex index = ui_->treeView->indexAt(pos);
  if (!index.isValid())
    return;

  if (TreeItem* pItem = reinterpret_cast<TreeItem*>(
        model_.itemFromIndex(index)
        ))
  {
    QMenu menu;
    ContextMenuFlags flags = pItem->contextMenuFlags();
    QMap<QString,QAction*> actions;
    if (flags.isExpandable())
    {
      QAction* action = menu.addAction(tr("Expand"));
      action->setData(index);
      connect(action, &QAction::triggered, this, &MainWindow::expand);
      actions.insert("Expand", action);
    }
    if (!menu.isEmpty())
      menu.exec(QCursor::pos());
    if (flags.isExpandable())
      disconnect(actions["Expand"], &QAction::triggered, this, &MainWindow::expand);
  }
}
