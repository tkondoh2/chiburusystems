﻿#ifndef TREEITEM_H
#define TREEITEM_H

#include <QStandardItem>

class ContextMenuFlags
{
public:
  ContextMenuFlags();
  bool isExpandable() const;
  void setIsExpandable(bool isExpandable);

private:
  bool isExpandable_;
};

/**
 * @brief Platanus用ツリーアイテム基本クラス
 */
class TreeItem
    : public QStandardItem
{
public:
  /**
   * @brief デフォルトコンストラクタ
   */
  TreeItem()
    : QStandardItem()
  {}

  /**
   * @brief コンストラクタ
   * @param text 表示テキスト
   */
  TreeItem(const QString& text)
    : QStandardItem(text)
  {}

  /**
   * @brief コンストラクタ
   * @param icon アイコン
   * @param text 表示テキスト
   */
  TreeItem(const QIcon& icon, const QString& text)
    : QStandardItem(icon, text)
  {}

  /**
   * @brief コンストラクタ
   * @param rows 行番号
   * @param columns 列番号
   */
  TreeItem(int rows, int columns = 1)
    : QStandardItem(rows, columns)
  {}

  /**
   * @brief コピーコンストラクタ
   * @param other コピー元オブジェクト
   */
  TreeItem(const TreeItem& other)
    : QStandardItem(other)
  {}

  /**
   * @brief デストラクタ
   */
  virtual ~TreeItem()
  {}

  virtual ContextMenuFlags contextMenuFlags() const;
  virtual void expand();
  virtual QVariantMap properties() const;

  template <class T>
  T* parentItem() const
  {
    QStandardItem* pParent = parent();
    while (pParent)
    {
      T* pT = dynamic_cast<T*>(pParent);
      if (pT)
        return pT;
      pParent = pParent->parent();
    }
    return nullptr;
  }
};

#endif // TREEITEM_H
