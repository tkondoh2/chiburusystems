﻿#include "treemodel.h"
#include "treeitem.h"

TreeModel::TreeModel(QObject* parent)
  : QStandardItemModel(parent)
{
  // ヘッダーラベルを設定する
  setHorizontalHeaderLabels(QStringList() << tr("Items"));
}

void TreeModel::showProperties(const QModelIndex& index)
{
  if (TreeItem* pItem = reinterpret_cast<TreeItem*>(
        itemFromIndex(index)
        ))
  {
    emit writeProperties(pItem->properties());
  }
}
