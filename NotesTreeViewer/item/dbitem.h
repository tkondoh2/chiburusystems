﻿#ifndef DBITEM_H
#define DBITEM_H

#include "item/fileitem.h"

class DbItem
    : public FileItem
{
public:
  DbItem();
  DbItem(const ntlx::FilePath& filePath, const ntlx::FileInfo& fileInfo, const QString& text);
  DbItem(const DbItem& other);
  DbItem& operator =(const DbItem& other);
  virtual ~DbItem();

  virtual ContextMenuFlags contextMenuFlags() const override;
  virtual void expand() override;
};

#endif // DBITEM_H
