﻿#ifndef FILEITEM_H
#define FILEITEM_H

#include "treeitem.h"
#include <ntlx_core/file.h>
#include <ntlx_core/fileinfo.h>

class FileItem
    : public TreeItem
{
public:
  FileItem();
  FileItem(const ntlx::FilePath& filePath, const QString& text);
  FileItem(const ntlx::FilePath& filePath, const ntlx::FileInfo& fileInfo, const QString& text);
  FileItem(const FileItem& other);
  FileItem& operator =(const FileItem& other);
  virtual ~FileItem();

  const ntlx::FilePath& filePath() const { return filePath_; }

  virtual QVariantMap properties() const override;

protected:
  ntlx::FilePath filePath_;
  ntlx::FileInfo fileInfo_;
};

#endif // FILEITEM_H
