﻿#ifndef NOTEITEM_H
#define NOTEITEM_H

#include "treeitem.h"
#include <ntlx_core/file.h>
#include <ntlx_core/itemtable.h>

class NoteItem
    : public TreeItem
{
public:
  NoteItem();
  NoteItem(const ntlx::ItemTable& itemTable, const QString& text);
  NoteItem(const NoteItem& other);
  NoteItem& operator =(const NoteItem& other);
  virtual ~NoteItem();

  virtual QVariantMap properties() const override;

  virtual ContextMenuFlags contextMenuFlags() const override;
  virtual void expand() override;

  NOTEID noteId() const;

protected:
  ntlx::ItemTable itemTable_;
};

#endif // NOTEITEM_H
