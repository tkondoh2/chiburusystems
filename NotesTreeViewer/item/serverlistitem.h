﻿#ifndef SERVERLISTITEM_H
#define SERVERLISTITEM_H

#include "treeitem.h"

class ServerListItem
    : public TreeItem
{
public:
  ServerListItem();
  ServerListItem(const QString& text);
  ServerListItem(const ServerListItem& other);
  ServerListItem& operator =(const ServerListItem& other);
  virtual ~ServerListItem();

  virtual ContextMenuFlags contextMenuFlags() const override;
  virtual void expand() override;
  virtual QVariantMap properties() const override;
};

#endif // SERVERLISTITEM_H
