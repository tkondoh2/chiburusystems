﻿#ifndef ITEMITEM_H
#define ITEMITEM_H

#include "treeitem.h"
#include <ntlx_core/file.h>
#include <ntlx_core/item.h>

class ItemItem
    : public TreeItem
{
public:
  ItemItem();
  ItemItem(const ntlx::Item& item, const QString& title);
  ItemItem(const ItemItem& other);
  ItemItem& operator =(const ItemItem& other);
  virtual ~ItemItem();

  virtual ContextMenuFlags contextMenuFlags() const override;
  virtual void expand() override;

  virtual QVariantMap properties() const override;

protected:
  ntlx::Item item_;
};

#endif // ITEMITEM_H
