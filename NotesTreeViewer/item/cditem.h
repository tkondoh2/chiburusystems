﻿#ifndef CDITEM_H
#define CDITEM_H

#include "treeitem.h"
#include <ntlx_core/cdrecord.h>

class CdItem
    : public TreeItem
{
public:
  CdItem();
  CdItem(const ntlx::CdRecord& cd, const QString& title);
  CdItem(const CdItem& other);
  CdItem& operator =(const CdItem& other);
  virtual ~CdItem();

  virtual QVariantMap properties() const override;

  static void insertCDPABDEFINITION(const ntlx::CdRecord& cd, QVariantMap& map);
  static void insertCDPABREFERENCE(const ntlx::CdRecord& cd, QVariantMap& map);
  static void insertCDPARAGRAPH(const ntlx::CdRecord& cd, QVariantMap& map);
  static void insertCDTEXT(const ntlx::CdRecord& cd, QVariantMap& map);

private:
  ntlx::CdRecord cd_;
};

#endif // CDITEM_H
