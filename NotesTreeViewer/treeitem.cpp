﻿#include "treeitem.h"

ContextMenuFlags::ContextMenuFlags()
  : isExpandable_(false)
{
}

bool ContextMenuFlags::isExpandable() const
{
  return isExpandable_;
}

void ContextMenuFlags::setIsExpandable(bool isExpandable)
{
  isExpandable_ = isExpandable;
}

ContextMenuFlags TreeItem::contextMenuFlags() const
{
  return ContextMenuFlags();
}

void TreeItem::expand()
{
}

QVariantMap TreeItem::properties() const
{
  QVariant v = data();
  if (v.canConvert<QVariantMap>())
    return v.toMap();
  return QVariantMap();
}
