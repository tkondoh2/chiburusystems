﻿#include "easylogger.h"

#include <QDateTime>

QX_CORE_BEGIN


bool EasyLogger::open(QFile::OpenMode mode)
{
  return file_.open(mode);
}

void EasyLogger::close()
{
  if (file_.isOpen())
    file_.close();
}

QString EasyLogger::now(const QString& format)
{
  QDateTime now = QDateTime::currentDateTime();
  return now.toString(format);
}

QX_CORE_END
