﻿#ifndef CS_QX_CORE_EASYLOGGER_H
#define CS_QX_CORE_EASYLOGGER_H

#include <qx_core/qx_core_global.h>

#include <QFile>
#include <QTextStream>

QX_CORE_BEGIN

class EasyLoggerHeader;

class QX_CORESHARED_EXPORT EasyLogger
{
public:
  template <class MakePathFunc>
  EasyLogger(MakePathFunc f)
    : file_(f())
    , str_(&file_)
  {
  }

  virtual ~EasyLogger()
  {
    close();
  }

  bool isOpen() const { return file_.isOpen(); }

  bool open(
      QFile::OpenMode mode = QFile::WriteOnly | QFile::Text | QFile::Append
      );
  void close();

  template <class HeaderFunc = EasyLoggerHeader>
  void log(const QString& msg, HeaderFunc f = EasyLoggerHeader())
  {
    if (file_.isOpen())
    {
      str_ << QString("%1 %2").arg(f()).arg(msg) << endl;
      str_.flush();
    }
  }

  static QString now(const QString& format = "yyyy/MM/dd HH:mm:ss.zzz");

protected:
  QFile file_;
  QTextStream str_;
};

class QX_CORESHARED_EXPORT EasyLoggerHeader
{
public:
  virtual QString operator ()()
  {
    return QString("[%1]").arg(EasyLogger::now());
  }
};

QX_CORE_END

#endif // EASYLOGGER_H
