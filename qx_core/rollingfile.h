﻿#ifndef CS_QX_CORE_ROLLINGFILE_H
#define CS_QX_CORE_ROLLINGFILE_H

#include <qx_core/qx_core_global.h>

#include <QFile>
#include <QDateTime>

QX_CORE_BEGIN

class RollingFile
{
public:
  virtual const QString& rollingFilePath() const = 0;
  virtual qint64 rollingFileLimitSize() const = 0;
  virtual void renameRollingFile() const = 0;

  virtual void onExistsRollingFile() {}
  virtual void onNotExistsRollingFile() {}
  virtual void beforeRenameRollingFile() {}
  virtual void afterRenameRollingFile() {}

  virtual void roll()
  {
    if (QFile::exists(rollingFilePath()))
    {
      onExistsRollingFile();
      QFile file(rollingFilePath());
      if (file.size() > rollingFileLimitSize())
      {
        beforeRenameRollingFile();
        renameRollingFile();
        afterRenameRollingFile();
      }
    }
    else
      onNotExistsRollingFile();
  }

  virtual QString rollingPathFormat() const
  {
    return "#{dir}\\#{file}_#{time}.#{ext}";
  }
  virtual QString rollingTimeFormat() const
  {
    return "yyyyMMdd_HHmmss_zzz";
  }

  virtual QString makeRenameFilePath(
      const QString& dirPath
      , const QString& baseName
      , const QString& ext
      ) const
  {
    QString path = rollingPathFormat();
    QDateTime now = QDateTime::currentDateTime();
    path.replace("#{dir}", dirPath)
        .replace("#{file}", baseName)
        .replace("#{ext}", ext)
        .replace("#{time}", now.toString(rollingTimeFormat()))
        ;
    return path;
  }
};

QX_CORE_END

#endif // CS_QX_CORE_ROLLINGFILE_H
