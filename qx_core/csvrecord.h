﻿#ifndef CS_QX_CORE_CSVRECORD_H
#define CS_QX_CORE_CSVRECORD_H

#include <qx_core/csvvalue.h>

QX_CORE_BEGIN

class CSVFile;

class QX_CORESHARED_EXPORT CSVRecord
{
public:
  CSVRecord(CSVFile* pFile);
  virtual ~CSVRecord();

  template <class T>
  void addValue(const QString& title, const QVariant& value)
  {
    int index = pFile_->indexFromTitle(title);
    if (index >= 0 && index < values_.count())
    {
      CSVValue* old = values_.at(index);
      values_.replace(index, new T(value));
      Q_ASSERT(old);
      delete old;
    }
  }

  virtual void write();

private:
  CSVFile* pFile_;
  QList<CSVValue*> values_;
};

QX_CORE_END

#endif // CS_QX_CORE_CSVRECORD_H
