﻿#ifndef CS_QX_CORE_CSVHEADER_H
#define CS_QX_CORE_CSVHEADER_H

#include <qx_core/csvcolumn.h>

QX_CORE_BEGIN

class QX_CORESHARED_EXPORT CSVHeader
    : public CSVColumn
{
public:
  CSVHeader(const QString& title);
  virtual ~CSVHeader();

  virtual QString toCSVFormat() const override;

private:
  QString title_;
};

QX_CORE_END

#endif // CS_QX_CORE_CSVHEADER_H
