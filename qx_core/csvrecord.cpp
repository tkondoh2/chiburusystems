﻿#include "csvrecord.h"

#include "csvheader.h"
#include "csvfile.h"

QX_CORE_BEGIN

CSVRecord::CSVRecord(CSVFile* pFile)
  : pFile_(pFile)
  , values_()
{
  foreach (CSVHeader header, pFile_->headers())
  {
    values_.append(new CSVValue());
  }
}

CSVRecord::~CSVRecord()
{
  qDeleteAll(values_);
}

void CSVRecord::write()
{
  pFile_->clearColumns();
  foreach (CSVValue* value, values_)
  {
    pFile_->addToColumn(value);
  }
  pFile_->writeColumns();
}

QX_CORE_END
