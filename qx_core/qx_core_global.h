﻿#ifndef CS_QX_CORE_GLOBAL_H
#define CS_QX_CORE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(QX_CORE_LIBRARY)
#  define QX_CORESHARED_EXPORT Q_DECL_EXPORT
#else
#  define QX_CORESHARED_EXPORT Q_DECL_IMPORT
#endif

#define QX_CORE_BEGIN namespace cs_qx_core {
#define QX_CORE_END }

#endif // CS_QX_CORE_GLOBAL_H
