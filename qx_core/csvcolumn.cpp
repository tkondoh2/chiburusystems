﻿#include "csvcolumn.h"

#include <QDateTime>

QX_CORE_BEGIN

QString CSVColumn::stringFormat(QString value) const
{
  const QString quote("\"");
  value.replace(quote, quote + quote);
  return quote + value + quote;
}

QString CSVColumn::numberFormat(double value) const
{
  return QString::number(value);
}

QString CSVColumn::dateTimeFormat(const QDateTime& value) const
{
  return value.toString("yyyy/MM/dd HH:mm:ss.zzz");
}

namespace deprecated
{

CSVColumn::CSVColumn()
  : header_()
  , value_()
{
}

CSVColumn::CSVColumn(const QString &header)
  : header_(header)
  , value_()
{
}

const QString& CSVColumn::header() const
{
  return header_;
}

const QVariant& CSVColumn::value() const
{
  return value_;
}

void CSVColumn::setValue(const QVariant& value)
{
  value_ = value;
}

void CSVColumn::clear()
{
  value_ = QVariant();
}

} // namespace deprecated

QX_CORE_END
