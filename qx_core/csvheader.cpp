﻿#include "csvheader.h"

QX_CORE_BEGIN

CSVHeader::CSVHeader(const QString& title)
  : CSVColumn()
  , title_(title)
{
}

CSVHeader::~CSVHeader()
{
}

QString CSVHeader::toCSVFormat() const
{
  return stringFormat(title_);
}

QX_CORE_END
