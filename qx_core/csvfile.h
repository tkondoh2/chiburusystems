﻿#ifndef CS_QX_CORE_CSVFILE_H
#define CS_QX_CORE_CSVFILE_H

#include <qx_core/csvheader.h>
#include <qx_core/csvrecord.h>

#include <QFile>

QX_CORE_BEGIN

class QX_CORESHARED_EXPORT CSVFile
{
public:
  CSVFile();
  virtual ~CSVFile();

  const QList<CSVHeader>& headers() const;
  int indexFromTitle(const QString& title) const;

  virtual void addHeader(const QString& title);

  virtual void setFileName(const QString& name);

  virtual bool open(
      QFile::OpenMode mode
            = QFile::Text | QFile::WriteOnly | QFile::Append
      );
  virtual void close();

  virtual void clearColumns();
  virtual void addToColumn(const CSVColumn* pColumn);
  virtual void writeColumns();

private:
  QFile file_;
  QList<CSVHeader> headers_;
  QMap<QString,int> indexMap_;
  QList<CSVRecord> records_;
  QList<QString> columns_;
};

namespace deprecated
{

class CSVColumn;

class QX_CORESHARED_EXPORT CSVFile
{
public:
  CSVFile();
  virtual ~CSVFile();

  virtual void addColumn(int index, const CSVColumn& column);

  virtual void setCSVFilePath(const QString& filePath);
  virtual bool open(
      QFile::OpenMode mode
      = QFile::Text | QFile::WriteOnly | QFile::Append
      );
  virtual void writeRow();
  virtual void close();

  virtual void clearValues();
  virtual void addValue(int index, const QVariant& value);

  virtual void startRow();
  virtual void writeCell(const QVariant& value);
  virtual void endRow();

  virtual QString quote(QString value) const;
  virtual QString numToCell(double value) const;
  virtual QString timeToCell(const QDateTime& value) const;

  virtual void addColumnStr(const QString& str);

private:
  QMap<int,CSVColumn> columns_;
  QFile file_;
  QStringList rowBuffer_;
};

} // namespace deprecated

QX_CORE_END

#endif // CS_QX_CORE_CSVFILE_H
