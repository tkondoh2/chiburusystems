﻿#include "csvfile.h"

#include <QTextStream>
#include <QDateTime>

QX_CORE_BEGIN

CSVFile::CSVFile()
  : file_()
  , headers_()
  , indexMap_()
  , records_()
{
}

CSVFile::~CSVFile()
{
  close();
}

const QList<CSVHeader>& CSVFile::headers() const
{
  return headers_;
}

int CSVFile::indexFromTitle(const QString& title) const
{
  return indexMap_.value(title, -1);
}

void CSVFile::addHeader(const QString& title)
{
  int index = headers_.count();
  headers_.append(CSVHeader(title));
  indexMap_.insert(title, index);
}

void CSVFile::setFileName(const QString& name)
{
  file_.setFileName(name);
}

bool CSVFile::open(QFile::OpenMode mode)
{
  bool result = file_.open(mode);

  // ファイルオープンに成功したら、ファイルサイズを元にヘッダーを書き込む。
  if (result && file_.size() < 1)
  {
    clearColumns();
    foreach (CSVHeader header, headers_)
    {
      addToColumn(&header);
    }
    writeColumns();
  }
  return result;
}

void CSVFile::close()
{
  if (file_.isOpen())
    file_.close();
}

void CSVFile::clearColumns()
{
  columns_.clear();
}

void CSVFile::addToColumn(const CSVColumn *pColumn)
{
  columns_.append(pColumn->toCSVFormat());
}

void CSVFile::writeColumns()
{
  QTextStream str(&file_);
  str << columns_.join(",") << endl;
  str.flush();
  clearColumns();
}

namespace deprecated
{

CSVFile::CSVFile()
  : columns_()
  , file_()
{
}

CSVFile::~CSVFile()
{
  close();
}

void CSVFile::addColumn(int index, const CSVColumn &column)
{
  columns_[index] = column;
}

void CSVFile::setCSVFilePath(const QString& filePath)
{
  file_.setFileName(filePath);
}

bool CSVFile::open(QFile::OpenMode mode)
{
  bool result = file_.open(mode);
  if (result)
  {
    if (file_.size() < 1)
    {
      startRow();
      foreach (int index, columns_.keys())
      {
        writeCell(columns_[index].header());
      }
      endRow();
    }
  }
  return result;
}

void CSVFile::writeRow()
{
  startRow();
  foreach (int index, columns_.keys())
  {
    writeCell(columns_[index].value());
  }
  endRow();
}

void CSVFile::close()
{
  if (file_.isOpen())
    file_.close();
}

void CSVFile::clearValues()
{
  foreach (int index, columns_.keys())
    columns_[index].clear();
}

void CSVFile::addValue(int index, const QVariant& value)
{
  if (columns_.contains(index))
    columns_[index].setValue(value);
}

void CSVFile::startRow()
{
  rowBuffer_.clear();
}

void CSVFile::writeCell(const QVariant& value)
{
  switch (value.type())
  {
  case QMetaType::QDateTime:
    addColumnStr(timeToCell(value.toDateTime()));
    return;

  case QMetaType::QString:
    addColumnStr(quote(value.toString()));
    return;

  default:
    if (value.canConvert(QMetaType::Double))
    {
      addColumnStr(numToCell(value.toDouble()));
      return;
    }
    break;
  }
  addColumnStr(quote(QString()));
}

QString CSVFile::quote(QString value) const
{
  const QString quote("\"");
  value.replace(quote, quote + quote);
  return quote + value + quote;
}

QString CSVFile::numToCell(double value) const
{
  return QString::number(value);
}

QString CSVFile::timeToCell(const QDateTime& value) const
{
  return value.toString("yyyy/MM/dd HH:mm:ss.zzz");
}

void CSVFile::endRow()
{
  QString row = rowBuffer_.join(",");
  QTextStream str(&file_);
  str << row << endl;
}

void CSVFile::addColumnStr(const QString& str)
{
  rowBuffer_.append(str);
}

} // namespace deprecated

QX_CORE_END
