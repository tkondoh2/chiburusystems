﻿#include "csvvalue.h"

#include <QDateTime>

QX_CORE_BEGIN

CSVValue::CSVValue()
  : CSVColumn()
  , value_()
{
}

CSVValue::CSVValue(const QVariant& value)
  : CSVColumn()
  , value_(value)
{
}

CSVValue::~CSVValue()
{
}

QString CSVValue::toCSVFormat() const
{
  if (value_.type() == QMetaType::QString)
    return stringFormat(value_.toString());

  if (value_.canConvert(QMetaType::QDateTime))
    return dateTimeFormat(value_.toDateTime());

  if (value_.canConvert(QMetaType::Double))
    return numberFormat(value_.toDouble());

  if (value_.canConvert(QMetaType::QString))
    return stringFormat(value_.toString());

  return stringFormat(QString());
}

QX_CORE_END
