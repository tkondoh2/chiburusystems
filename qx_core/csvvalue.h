﻿#ifndef CS_QX_CORE_CSVVALUE_H
#define CS_QX_CORE_CSVVALUE_H

#include <qx_core/csvcolumn.h>

QX_CORE_BEGIN

class QX_CORESHARED_EXPORT CSVValue
    : public CSVColumn
{
public:
  CSVValue();
  CSVValue(const QVariant& value);
  virtual ~CSVValue();

  virtual QString toCSVFormat() const override;

  const QVariant& value() const
  {
    return value_;
  }

private:
  QVariant value_;
};

QX_CORE_END

#endif // CS_QX_CORE_CSVVALUE_H
