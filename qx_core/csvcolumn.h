﻿#ifndef CS_QX_CORE_CSVCOLUMN_H
#define CS_QX_CORE_CSVCOLUMN_H

#include <qx_core/qx_core_global.h>

#include <QVariant>

QX_CORE_BEGIN

class QX_CORESHARED_EXPORT CSVColumn
{
public:
  virtual QString toCSVFormat() const = 0;

  virtual QString stringFormat(QString value) const;
  virtual QString numberFormat(double value) const;
  virtual QString dateTimeFormat(const QDateTime& value) const;
};

namespace deprecated
{

class QX_CORESHARED_EXPORT CSVColumn
{
public:
  CSVColumn();
  CSVColumn(const QString& header);

  const QString& header() const;
  const QVariant& value() const;
  void setValue(const QVariant& value);
  void clear();

private:
  QString header_;
  QVariant value_;
};

} // namespace deprecated

QX_CORE_END

#endif // CS_QX_CORE_CSVCOLUMN_H
